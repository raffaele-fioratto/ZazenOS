# this one is important
SET(CMAKE_SYSTEM_NAME i686-elf)

# specify the cross compiler
SET(CMAKE_C_COMPILER ${COMPILER_ROOT}/bin/i686-elf-gcc)
SET(CMAKE_CXX_COMPILER ${COMPILER_ROOT}/bin/i686-elf-g++)
SET(CMAKE_ASM_COMPILER ${COMPILER_ROOT}/bin/i686-elf-gcc)
SET(OBJCOPY ${COMPILER_ROOT}/bin/i686-elf-objcopy)

# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH  ${COMPILER_ROOT} )

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

SET(CMAKE_C_FLAGS "${CFLAGS} -ffreestanding -fno-builtin -nostdlib -std=c11")
SET(CMAKE_CXX_FLAGS "${CXXFLAGS} -fno-exceptions -fno-rtti -ffreestanding -fno-builtin -nostdlib -std=c++11")
SET(CMAKE_EXE_LINKER_FLAGS "-ffreestanding -fno-builtin -nostdlib")

SET(COMPILER_NAME gcc)
UNSET(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS)
UNSET(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS)

execute_process(COMMAND ${CMAKE_C_COMPILER} --print-file-name=crtbegin.o OUTPUT_VARIABLE CRTBEGIN OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND ${CMAKE_C_COMPILER} --print-file-name=crtend.o OUTPUT_VARIABLE CRTEND OUTPUT_STRIP_TRAILING_WHITESPACE)