# ZazenOS
Yet Another Hobbyist Operating System.

## Description
ZazenOS is (yet) a simple OS consisting of a kernel (and hopefully some user space programs in the future).

The kernel is written mainly in C++.

It should provide some sort of portability across different architectures.

## Build
ZazenOS uses CMake as its build system.

The set of files that guides the compiling has been designed with modularity and  portability accross various targets and cross compilers.
As of now only the x86-32 target is supported which expect a working i686-elf cross compiler.


A dedicated cmake file is in charge of setting up correctly the compiler, this is controlled by another file in `conf/` directory that contains various options that in principle should be different for every target and compiler.


In order to obtain a x86-32 kernel, all you have to do is:
```shell
mkdir build && cd build
cmake -C ../conf/x86.cmake -DCOMPILER_ROOT=<path to your i686-elf x-compiler> [-DCMAKE_BUILD_TYPE=Debug|Release]  ..
make
```
With the Debug build an additional file is generated containing debug symbols that can be used with gdb (see Debugging section).

## Run
As of now only QEMU is supported, and at this stage of development, you can simply run the kernel.

To do this, issue the command (assuming inside the `build` directory):
```shell
qemu-system-i386 -kernel kernel/ZazenOS
```

## Debugging
You can attach a debugger to the QEMU session.

The command line for the emulator becomes:
```shell
qemu-system-i386 -kernel kernel/ZazenOS -s -S
```
With these switches QEMU starts but doesn't fire up the emulation layer and waits for a debugger to attach and explicity resume execution.

Open a new terminal window and move to the `build` directory.

The debug build creates a specific file that contains all the debug informations. You may wish to pass them to GDB in order to make the debug session more friendly :)
```shell
i686-elf-gdb kernel/ZazenOS.kdbg
```
By default QEMU creates a GDB server at port `1234`, so inside the debugger you can attach to it by simply issuing the command
```shell
(gdb) target remote :1234
```
Now you can run a standard GDB session using the commands you are already familiar with, notice that the execution is already started so you should issue `continue` (or `c`) command to start debugging rather than `run` (or `r`)

## Cleaning
You can clean the working directory used for building by issuing the command at top level (e.g. `build` if you followed precisely the build procedure):
```shell
make clean
```
