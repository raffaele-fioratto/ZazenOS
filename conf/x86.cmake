OPTION(ARCH_X86 "Configure kernel for x86-32 build" ON)

SET(XCOMPILER_CONFIG "i686-elf" CACHE STRING "Compiler Config File")
SET(LINKER_SCRIPT "src/arch/x86/x86.ld" CACHE STRING "Linker Script File")

SET(CFLAGS "-Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations -Wredundant-decls -Wnested-externs -Winline -Wno-long-long -Wuninitialized -Wstrict-prototypes"
        CACHE STRING "C Compiler Flags")
SET(CXXFLAGS "-Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align -Wwrite-strings -Wmissing-declarations -Wredundant-decls -Winline -Wno-long-long -Wuninitialized"
        CACHE STRING "C++ Compiler Flags")
