#! /usr/bin/bash

sed -i -E 's/\{ (0x[A-Fa-f0-9]+), ([0-9]+), (0x[A-Fa-f0-9]+), ([0-9]+), ([0-9]+), ([0-9]+), ([0-9]+) \}/\{ \{ \1, \2 }, \3, \4, \5, \6, \7 \}/' insts.c
sed -i -E 's/\"([A-Z0-9\\0_]+)\"/\L\"\1\"/g' mnemonics.c
sed -i -E 's/\"(DB |LOCK |REP[N]?[Z]? |)\"/\"\L\1\"/' distorm.c