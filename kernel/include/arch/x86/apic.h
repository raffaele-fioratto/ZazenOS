// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __APIC_H__
#define __APIC_H__

#include <global.h>
#include <arch/x86/irq.h>
#include <map.h>

namespace X86 {

class IOAPIC : public AbstractInterruptController {
public:
    enum IntDeliveryMode {
        Fixed           = 0,
        LowestPriority  = 1,
        SMI             = 2,
        Reserved1       = 3,
        NMI             = 4,
        INIT            = 5,
        Reserved2       = 6,
        ExtINT          = 7
    };

    enum IntDestinationMode {
        Physical    = 0,
        Logical     = 1
    };

    enum IntPinPolarity {
        HighActive  = 0,
        LowActive   = 1
    };

    enum IntTriggerMode {
        EdgeTriggered   = 0,
        LevelTriggered    = 1
    };

    enum IntMask {
        Unmasked = 0,
        Masked   = 1
    };

    IOAPIC(uint32_t ioApicBaseAddr);
    virtual ~IOAPIC();
    virtual void init();
    virtual void disable();
    virtual void ackIRQ(uint32_t number);
    virtual void maskIRQ(uint32_t number);
    virtual void unmaskIRQ(uint32_t number);
    virtual void enable();
private:
    static const uint32_t IOAPIC_MAX_REDIR_ENTRIES  = 0x18;

    static const uint32_t IOAPICID                  = 0x00;
    static const uint32_t IOAPICVER                 = 0x01;
    static const uint32_t IOAPICARB                 = 0x02;
    static const uint32_t IOREDTBL                  = 0x10;

    void IOAPICWrite32(uint32_t offset, uint32_t value);
    void IOAPICWrite64(uint32_t offset, uint64_t value);

    uint64_t IOAPICRead64(uint32_t offset);
    uint32_t IOAPICRead32(uint32_t offset);

    union io_apic_id {
        struct {
            uint32_t reserved1 : 23; // 0  .. 23
            uint32_t id        : 4;  // 24 .. 27
            uint32_t reserved2 : 4;  // 28 .. 31
        } bits;
        uint32_t dword;
        io_apic_id() { dword = 0; }
        io_apic_id(uint32_t v) { dword = v; }
        operator uint32_t() { return dword; }
    };

    union io_apic_ver {
        struct {
            uint32_t version         : 8; // 0  .. 7
            uint32_t reserved1       : 8; // 8  .. 15
            uint32_t max_redir_entry : 8; // 16 .. 23
            uint32_t reserved2       : 8; // 24 .. 31
        } bits;
        uint32_t dword;
        io_apic_ver() { dword = 0; }
        io_apic_ver(uint32_t v) { dword = v; }
    };

    union io_apic_arb {
        struct {
            uint32_t reserved1 : 23; // 0  .. 23
            uint32_t arb_id    : 4;  // 24 .. 27
            uint32_t reserved2 : 4;  // 28 .. 31
        } bits;
        uint32_t dword;
        io_apic_arb() { dword = 0; }
        io_apic_arb(uint32_t v) { dword = v; }
        operator uint32_t() { return dword; }
    };

    union io_red_tbl {
        struct {
            uint64_t vector          : 8;   // 0  .. 7
            uint64_t delivery_mode   : 3;   // 8  .. 10
            uint64_t dest_mode       : 1;   // 11
            uint64_t delivery_status : 1;   // 12
            uint64_t polarity        : 1;   // 13
            uint64_t remote_irr      : 1;   // 14
            uint64_t trigger_mode    : 1;   // 15
            uint64_t mask            : 1;   // 16
            uint64_t reserved        : 39;  // 17 .. 55
            uint64_t destination     : 8;   // 56 .. 63
        } bits;
        uint64_t qword;
        io_red_tbl() { qword = 0; }
        io_red_tbl(uint64_t v) { qword = v; }
        operator uint64_t() { return qword; }
    };


    uint32_t version;
    uint32_t ioRegSel;
    uint32_t ioWin;
    uint32_t id;
    uint32_t IRQMaxEntry;
    io_red_tbl redirTable[IOAPIC_MAX_REDIR_ENTRIES];
    KLibcpp::Map<int, int> irqToIntinMappings;
};

class APIC : public AbstractInterruptController {
public:
    APIC();
    virtual ~APIC();
    virtual void init();
    virtual void disable();
    virtual void ackIRQ(uint32_t number);
    virtual void maskIRQ(uint32_t number);
    virtual void unmaskIRQ(uint32_t number);
    virtual void enable();
private:
    KLibcpp::Map<int, IOAPIC*> ioApicList;
    KLibcpp::Map<int, int> irqMappings;
};

}

#endif
