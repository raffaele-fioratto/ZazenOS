// Copyright (C) 2016 Fioratto Raffaele
// Useful assembly macros and constants

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __X86_ASSEMBLY_H__
#define __X86_ASSEMBLY_H__

#define TMP_PAGE_0		0x80000 // 4KB for identity mapping memory range [0-4]MB shared with mapping Phys [0-4]MB -> [3072-3076]MB
#define TMP_PAGE_1022 	0x81000 // stack page
#define TMP_DIR		  	0x82000 // initial kernel directory address

#define STACKSIZE		0x4000

#define BASE_IRQ_VECTOR	0x20

#define DEFINE_FUNCTION(name) \
	.global name; \
	.type name,@function; \
	name:
#define END_FUNCTION(name) \
	.size name, . - name;

#define GLUE2(a, b) a##b
#define GLUE(a, b) GLUE2(a, b)

#endif /* __X86_ASSEMBLY_H__ */
