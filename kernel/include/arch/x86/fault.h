#ifndef __FAULT_H__
#define __FAULT_H__

#include <arch/x86/isr.h>

namespace X86 {

enum X86Exceptions {
    DivByZero           = 0,
    Debug               = 1,
    NMI                 = 2,
    Breakpoint          = 3,
    Overflow            = 4,
    BoundRangeExceeded  = 5,
    UndefOpcode         = 6,
    DevNotAvail         = 7,
    DoubleFault         = 8,
    KernelPanic         = 9,
    InvalidTSS          = 10,
    SegmNotPresent      = 11,
    SSFault             = 12,
    GPF                 = 13,
    PageFault           = 14,
    FPUFault            = 16,
    AlignCheck          = 17,
    MachineCheck        = 18,
    XMMFault            = 19,
    VirtFault           = 20,
    SecurityFault       = 30,
    EXC_NUM             = 32
};

class DivisionByZeroFault : public AbstractISRHandler {
public:
    DivisionByZeroFault();
    ~DivisionByZeroFault() {}
    int handle(ISRHandler::Registers& regs);

};

class KernelPanicFault : public AbstractISRHandler {
public:
    KernelPanicFault();
    ~KernelPanicFault() {}
    int handle(ISRHandler::Registers& regs);

private:
    static const char* EXC_MNEMONICS[EXC_NUM];
};

}

#endif
