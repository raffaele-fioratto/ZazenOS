// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __I8254_H__
#define __I8254_H__

#include <arch/x86/irq.h>

namespace X86 {

class I8254 : public AbstractIRQHandler {
public:
    enum Encoding {
        BCD = 0,
        Binary = 1
    };

    enum OperatingMode {
        InterruptOnTerminalCount = 0,
        HWRetriggerOneShot       = 1,
        RateGenerator            = 2,
        SQWaveGenerator          = 3,
        SWTriggerStrobe          = 4,
        HWTriggerStrobe          = 5,
        RateGenerator2           = 6,
        SQWaveGenerator2         = 7
    };

    enum AccessMode {
        LatchCountValueCmd       = 0,
        LowByteOnly              = 1,
        HighByteOnly             = 2,
        Word                     = LowByteOnly | HighByteOnly
    };

    enum Channel {
        Channel0                 = 0,
        Channel1                 = 1,
        Channel2                 = 2,
    };

    I8254();
    ~I8254();

    void init();

    void setOperatingMode(OperatingMode mode, Channel channel = Channel0);
    void setOperatingMode(OperatingMode mode, uint16_t reloadValue, Channel channel = Channel0);
    OperatingMode getOperatingMode(Channel channel = Channel0);

    void setReloadValue(uint16_t reloadValue, Channel channel = Channel0);
    uint8_t getGate();
    void    setGate(uint8_t value);

    virtual int handle(ISRHandler::Registers& regs);

private:
    static const uint32_t PIT_NUM_CHANNELS = 3;
    static const uint8_t PIT_CHANNEL0_DATA = 0x40;
    static const uint8_t PIT_CHANNEL1_DATA = 0x41;
    static const uint8_t PIT_CHANNEL2_DATA = 0x42;
    static const uint8_t PIT_MCR           = 0x43;
    static const uint8_t PIT_GATE          = 0x61;

    static const uint8_t PIT_CHANNELS_DATA[PIT_NUM_CHANNELS];

    union MCR {
        struct {
            uint8_t BCDBinaryMode : 1; // 0
            uint8_t operatingMode : 3; // 1..3
            uint8_t accessMode    : 2; // 4..5
            uint8_t channelSelect : 2; // 6..7
        } Bits;
        uint8_t b;
        MCR() { b = 0; }
        MCR(uint8_t v) { b = v; }
        operator uint8_t() { return b; }
    };

    union ReadBackStatus {
        struct {
            uint8_t BCDBinaryMode  : 1; // 0
            uint8_t operatingMode  : 3; // 1..3
            uint8_t accessMode     : 2; // 4..5
            uint8_t nullCountFlag  : 1; // 6
            uint8_t outputPinState : 1; // 7
        } Bits;
        uint8_t b;
        ReadBackStatus() { b = 0; }
        ReadBackStatus(uint8_t v) { b = v; }
        operator uint8_t() { return b; }
    };

    union MCRReadBack {
         struct {
            uint8_t reserved              : 1; // 0
            uint8_t readBackTimerChannel0 : 1; // 1
            uint8_t readBackTimerChannel1 : 1; // 2
            uint8_t readBackTimerChannel2 : 1; // 3
            uint8_t latchStatusFlag       : 1; // 4
            uint8_t latchCountFlag        : 1; // 5
            uint8_t readBackAccess        : 2; // 6..7
        } Bits;
        uint8_t b;
        MCRReadBack() { b = 0; }
        MCRReadBack(uint8_t v) { b = v; }
        operator uint8_t() { return b; }
    };

    ReadBackStatus cachedStatuses[PIT_NUM_CHANNELS];

    enum {
        ReadBack = 3
    };

    ReadBackStatus readChannelStatus(Channel channel);
};

}

#endif
