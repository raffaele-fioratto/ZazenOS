// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __I8259_H__
#define __I8259_H__

#include <stdint.h>
#include <arch/x86/irq.h>

namespace X86 {

class I8259 : public AbstractInterruptController {
public:
    I8259();
    ~I8259();
    int remap(uint8_t masterIRQOffset, uint8_t slaveIRQOffset);
    virtual void init() { remap(BASE_IRQ_VECTOR, BASE_IRQ_VECTOR + 8); }
    virtual void disable();
    virtual void ackIRQ(uint32_t number);
    virtual void maskIRQ(uint32_t number);
    virtual void unmaskIRQ(uint32_t number);
    virtual void enable();
    uint16_t getIRR();
    uint16_t getISR();
    uint8_t getMasterIRQOffset(void);
    uint8_t getSlaveIRQOffset(void);

private:
    uint16_t getIRQReg(uint8_t ocw3);

    static const uint8_t  PIC1            = 0x20;        /* IO base address for master PIC */
    static const uint8_t  PIC2            = 0xA0;        /* IO base address for slave PIC */
    static const uint8_t  PIC1_COMMAND    = PIC1;
    static const uint8_t  PIC1_DATA       = (PIC1+1);
    static const uint8_t  PIC2_COMMAND    = PIC2;
    static const uint8_t  PIC2_DATA       = (PIC2+1);

    static const uint8_t  PIC_EOI         = 0x20;        /* End-of-interrupt command code */

    static const uint8_t  ICW1_ICW4       = 0x01;        /* ICW4 (not) needed */
    static const uint8_t  ICW1_SINGLE     = 0x02;        /* Single (cascade) mode */
    static const uint8_t  ICW1_INTERVAL4  = 0x04;        /* Call address interval 4 (8) */
    static const uint8_t  ICW1_LEVEL      = 0x08;        /* Level triggered (edge) mode */
    static const uint8_t  ICW1_INIT       = 0x10;        /* Initialization - required! */

    static const uint8_t  OCW3_READ_IRR   = 0x0A;
    static const uint8_t  OCW3_READ_ISR   = 0x0B;

    static const uint8_t  ICW4_8086       = 0x01;        /* 8086/88 (MCS-80/85) mode */
    static const uint8_t  ICW4_AUTO       = 0x02;        /* Auto (normal) EOI */
    static const uint8_t  ICW4_BUF_SLAVE  = 0x08;        /* Buffered mode/slave */
    static const uint8_t  ICW4_BUF_MASTER = 0x0C;        /* Buffered mode/master */
    static const uint8_t  ICW4_SFNM       = 0x10;        /* Special fully nested (not) */

    uint8_t masterIRQOffset, slaveIRQOffset;
    uint16_t cachedIRQMask;

};

}



#endif
