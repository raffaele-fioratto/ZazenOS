#ifndef __ASM_INLINE_H__
#define __ASM_INLINE_H__

#include <global.h>

namespace X86 {

static inline void outb(uint16_t port, uint8_t val) {
    __asm__ volatile("outb %0, %1"
                    :
                    :"a"(val), "Nd"(port));
}

static inline void outw(uint16_t port, uint16_t val) {
    __asm__ volatile("outw %0, %1"
                    :
                    :"a"(val), "Nd"(port));
}

static inline void outl(uint16_t port, uint32_t val) {
    __asm__ volatile("outl %0, %1"
                    :
                    :"a"(val), "Nd"(port));
}


static inline uint8_t inb(uint16_t port) {
    uint8_t ret;
    __asm__ volatile("inb %1, %0"
                    :"=a"( ret )
                    :"Nd"( port ) );
    return ret;
}

static inline uint16_t inw(uint16_t port) {
    uint16_t ret;
    __asm__ volatile("inw %1, %0"
                    :"=a"(ret)
                    :"Nd"(port));
    return ret;
}

static inline unsigned long inl( uint16_t port )
{
	uint32_t ret;
	__asm__ volatile( "inl %1, %0"
		          	: "=a"( ret )
		          	: "Nd"( port ) );
	return ret;
}

static inline void io_wait( void )
{
    __asm__ volatile( "outb %%al, $0x80"
                    :
                    : "a"( 0 ) );
}

static inline void cpuid( uint32_t code, uint32_t info[4] )
{
	__asm__ volatile( "cpuid"
			        : "=a"( *info ), "=b"( *( info + 1 ) ), "=c"( *( info + 2 ) ), "=d"( *( info + 3 ) )
			        : "a"( code ) );
}

struct CpuidRegs {
    uint32_t eax;
    uint32_t ebx;
    uint32_t ecx;
    uint32_t edx;
};

static inline void cpuid(uint32_t code, CpuidRegs* regs) {
    __asm__ volatile( "cpuid"
                    : "=a"(regs->eax), "=b"(regs->ebx), "=c"(regs->ecx), "=d"(regs->edx)
                    : "a"(code));
}

static inline void sti( void )
{
	__asm__ volatile( "sti" );
	//outb( 0x70, inb( 0x70 ) & 0x7F );
}

static inline void cli( void )
{
	__asm__ volatile( "cli" );
	//outb( 0x70, inb( 0x70 ) | 0x80 );
}

static inline void fpu_set_control_word( uint16_t ctrl )
{
	__asm__ volatile( "fldcw %0"
			        :
			        : "m" ( ctrl )
			        );
}

/*
static inline void lidt( struct idt_table* table )
{
	__asm__ volatile( "lidt (%0)"
			:
			: "a" ( table )
			);
}
*/

static inline void bts( uint32_t *value, uint32_t pos )
{
	__asm__ volatile( "bts %%edx, %0\n\t"
					 :
					 : "m"( *value ), "d"( pos )
					 );
}

static inline void btr( uint32_t *value, uint32_t pos )
{
	__asm__ volatile( "btr %%edx, %0\n\t"
					 :
					 : "m"( *value ), "d"( pos )
					);
}

static inline int bt( uint32_t value, uint32_t pos )
{
	int res = 0;
	__asm__ volatile( "bt %%edx, %%ecx\n\t"
					  "setc (%%eax)\n\t"
					 :
					 : "a" (&res), "c" (value), "d" (pos)
					 );
	return res;
}

static inline int32_t bsr( uint32_t value )
{
	int32_t pos;
	__asm__ volatile( "mov $-1, %%ecx\n\t"
                    "cmp $0, %%edx\n\t"
				    "je 1f\n\t"
                    "bsr %%edx, %%ecx\n\t"
                    "1:\n\t"
                    : "=c" ( pos )
                    : "d" ( value )
                    );
	return pos;
}

static inline int32_t bsf( uint32_t value )
{
  int32_t pos;
  __asm__ volatile( "mov $-1, %%ecx\n\t"
                    "cmp $0, %%edx\n\t"
                    "je 1f\n\t"
                    "bsf %%edx, %%ecx\n\t"
                    "1:\n\t"
                    : "=c" ( pos )
                    : "d" ( value )
                    );
    return pos;
}

static inline void bit_set_multi( uint32_t* value, uint32_t pos, uint32_t n )
{
    __asm__ volatile( "1:\n\t"
                      "bts %%eax, (%%edx)\n\t"
                      "inc %%eax\n\t"
                      "test $0x1F, %%eax\n\t"
                      "jne 2f\n\t"
                      "xor %%eax, %%eax\n\t"
                      "addl $4, %%edx\n\t"
                      "2:\n\t"
                      "loop 1b\n\t"
                    :
                    : "a" ( pos ), "d" ( value ), "c" ( n )
                    );
}

static inline void bit_unset_multi( uint32_t* value, uint32_t pos, uint32_t n )
{
    __asm__ volatile( "1:\n\t"
                      "btr %%eax, (%%edx)\n\t"
                      "inc %%eax\n\t"
                      "test $0x1F, %%eax\n\t"
                      "jne 2f\n\t"
                      "xor %%eax, %%eax\n\t"
                      "addl $4, %%edx\n\t"
                      "2:\n\t"
                      "loop 1b\n\t"
                    :
                    : "a" ( pos ), "d" ( value ), "c" ( n )
                    );
}

static inline void set_cr3( unsigned long value )
{
	__asm__ volatile( "mov %0, %%cr3\n\t"
			        :
			        : "a"( value )
			        );
}

static inline unsigned long get_cr3 () {
    unsigned long value;
    __asm__ volatile( "mov %%cr3, %0\n\t"
    : "=a" ( value )
    :
    );
    return value;
}

static inline void flush_tlb_single( unsigned long addr )
{
	__asm__ volatile("invlpg (%0)" :: "r" (addr) : "memory" );
}

static inline void halt( void )
{
	__asm__ volatile( "1:\n\t"
			          "hlt\n\t"
			          "jmp 1b"
			        :
			        :
			        );
}

static inline uint64_t rdtsc( void )
{
	uint32_t eax, edx;
	__asm__ volatile( "rdtsc"
			        : "=a"( eax ), "=d"( edx )
			        :
			        );
	return ( uint64_t ) edx << 32 | eax;
}

static inline void lock_inc( int32_t* ptr )
{
    __asm__ volatile( "lock incl %0"
                    :
                    : "m" ( *ptr )
                    : "memory"
                    );
}

static inline void lock_dec( int32_t* ptr )
{
    __asm__ volatile( "lock decl %0"
                    :
                    : "m" ( *ptr )
                    : "memory"
                    );
}

static inline void set_wp_bit( void )
{
    __asm__ volatile( "mov %%cr0, %%eax\n\t"
                      "or $0x00010000, %%eax\n\t"
                      "mov %%eax, %%cr0\n\t"
                    ::
                    );
}

static inline void clear_wp_bit( void )
{
    __asm__ volatile( "mov %%cr0, %%eax\n\t"
                      "and $0xFFFEFFFF, %%eax\n\t"
                      "mov %%eax, %%cr0\n\t"
                    ::
                    );
}

static inline uint32_t save_eflags( void )
{
    uint32_t ret;
    __asm__ volatile( "pushf\n\t"
                      "pop %%eax\n\t"
                    : "=a" ( ret )
                    :
                    );
    return ret;
}

static inline void restore_eflags( uint32_t flags )
{
    __asm__ volatile( "push %%eax\n\t"
                      "popf\n\t"
                    :
                    : "a" ( flags )
                    );
}

static inline void rep_insw( uint16_t port, void* buffer, uint32_t words )
{
	__asm__ volatile( "rep insw\n\t"
					:
					: "Nd" ( port ), "D" ( buffer ), "c" ( words )
					);
}

static inline void enable_sse() {
  __asm__ volatile( "mov %cr0, %eax\n\t"
                    "andl $0xFFFFFFFB, %eax\n\t"
                    "orl $0x2, %eax\n\t"
                    "mov %eax, %cr0\n\t"
                    "mov %cr4, %eax\n\t"
                    "orl $0x600, %eax\n\t"
                    "mov %eax, %cr4\n\t"
                  );
}

static inline void read_MSR(uint32_t msr, uint32_t *lo, uint32_t *hi) {
   __asm__ volatile("rdmsr" : "=a"(*lo), "=d"(*hi) : "c"(msr));
}

static inline void write_MSR(uint32_t msr, uint32_t lo, uint32_t hi) {
   __asm__ volatile("wrmsr" :: "a"(lo), "d"(hi), "c"(msr));
}

}

#endif /* __ASM_INLINE_H__ */
