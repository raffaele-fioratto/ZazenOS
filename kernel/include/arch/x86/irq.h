// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __IRQ_H__
#define __IRQ_H__

#include <global.h>
#include <arch/x86/assembly.h>
#include <arch/x86/isr.h>
#include <list.h>

namespace X86 {

class AbstractInterruptController {
public:
    virtual ~AbstractInterruptController() { }
    virtual void init() = 0;
    virtual void disable() = 0;
    virtual void ackIRQ(uint32_t number) = 0;
    virtual void maskIRQ(uint32_t number) = 0;
    virtual void unmaskIRQ(uint32_t number) = 0;
    virtual void enable() = 0;
};

class AbstractIRQHandler : public AbstractISRHandler {
public:
    AbstractIRQHandler(uint32_t number) : AbstractISRHandler(number + BASE_IRQ_VECTOR) { }
    ~AbstractIRQHandler() { }
};

class IRQBroker : public AbstractIRQHandler {
public:
    enum IRQReturn {
        Invalid = 0,
        Pass = 1,
        Handled = 2
    };
    IRQBroker(uint32_t irqNumber);
    ~IRQBroker();

    void registerIRQHandler(AbstractIRQHandler* handler);
    void deregisterIRQHandler(AbstractIRQHandler* handler);

protected:
    int handle(ISRHandler::Registers& regs);

private:
    KLibcpp::List<AbstractIRQHandler*> sharedHandlers;
};

}

#endif /* __IRQ_H__ */
