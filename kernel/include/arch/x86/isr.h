// Copyright (C) 2016 Raffaele Fioratto

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ISR_H__
#define __ISR_H__

#include <stdint.h>

namespace X86 {

static const uint32_t ISR_NUM_HANDLERS = 0xFF;

class AbstractISRHandler;

class ISRHandler {
public:
    struct Registers {
        uint32_t fxsave_buffer[128]; // +0
        uint32_t fill1;     // +512
        uint32_t ctx_switch;// +516
        uint32_t ds;        // +520
        uint32_t edi;       // +524
        uint32_t esi;       // +528
        uint32_t ebp;       // +532
        uint32_t esp;       // +536
        uint32_t ebx;       // +540
        uint32_t edx;       // +544
        uint32_t ecx;       // +548
        uint32_t eax;       // +552
        uint32_t int_no;    // +556
        uint32_t err_code;  // +560
        uint32_t eip;       // +564
        uint32_t cs;        // +568
        uint32_t eflags;    // +572
    };

    static void init();
    static void handle(Registers& regs);
    static void handleIRQ(Registers& regs);
    static int registerHandler(AbstractISRHandler* handler);
    static int registerIRQHandler(AbstractISRHandler* handler);
    static int deregisterHandler(AbstractISRHandler* handler);
    static int deregisterIRQHandler(AbstractISRHandler* handler);

private:
    static AbstractISRHandler* handlers[ISR_NUM_HANDLERS];
};

class AbstractISRHandler {
public:
    AbstractISRHandler(uint8_t number) { this->isrNumber = number; }
    uint8_t number() { return this->isrNumber; }

protected:
    virtual int handle(ISRHandler::Registers& regs) = 0;
    virtual ~AbstractISRHandler() { ISRHandler::deregisterHandler(this); }
    uint8_t isrNumber;

    friend class IRQBroker;
    friend class ISRHandler;
};

}
#endif /* __ISR_H__ */
