// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LAPIC_H__
#define __LAPIC_H__

#include <stdint.h>
#include <arch/x86/irq.h>

namespace X86 {

class LAPIC {
public:
    LAPIC();
    ~LAPIC();
    void init();
    void disable() const;
    void enable() const;
    void sendEOI() const;

private:
    void enableAPICMSR(uintptr_t baseAddr);
    void APICWrite(uint32_t offset, uint32_t value) const;
    uint32_t APICRead(uint32_t offset) const;

    static const uint32_t APIC_TASKPRIOR        = 0x080;
    static const uint32_t APIC_LDR              = 0x0D0;
    static const uint32_t APIC_DFR              = 0x0E0;
    static const uint32_t APIC_EOI              = 0x0B0;
    static const uint32_t APIC_SPURIOUS         = 0x0F0;
    static const uint32_t APIC_LVT_TMR          = 0x320;
    static const uint32_t APIC_LVT_PERF         = 0x340;
    static const uint32_t APIC_LVT_LINT0        = 0x350;
    static const uint32_t APIC_LVT_LINT1        = 0x360;
    static const uint32_t APIC_TMRINITCNT       = 0x380;
    static const uint32_t APIC_TMRCURRCNT       = 0x390;
    static const uint32_t APIC_TMRDIV           = 0x3E0;

    static const uint32_t APIC_SW_ENABLE        = 0x100;
    static const uint32_t APIC_DISABLE          = 0x10000;
    static const uint32_t APIC_NMI              = 0x400;

    static const uint32_t APIC_BASE_MSR_ENABLE  = 0x800;
    static const uint32_t APIC_BASE_MSR         = 0x01B;

    uintptr_t lapicBaseAddr;
};

}

#endif
