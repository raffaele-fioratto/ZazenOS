// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LOCKS_H__
#define __LOCKS_H__

#include <arch/x86/inline.h>

namespace X86 {

extern "C" void spinlockAcquire(long* lock) __fastcall;
extern "C" void spinlockRelease(long* lock) __fastcall;

class Spinlock {
public:
    void lock() { uint32_t flags = save_eflags(); X86::cli(); spinlockAcquire(&value); restore_eflags(flags); }
    void unlock() { uint32_t flags = save_eflags(); X86::cli(); spinlockRelease(&value); restore_eflags(flags); }
    bool isLocked() { return value == 1; }

private:
    long value = 0;
};

template<class T>
class ScopedLock {
public:
    ScopedLock(T l) { thisLock = l; thisLock.lock(); }
    ScopedLock& operator=(T l) { thisLock = l; thisLock.lock(); return *this; }
    ~ScopedLock() { thisLock.unlock(); }
    operator bool() { return thisLock.isLocked(); }

private:
    T thisLock;
};

}

#define lock(x) \
    X86::ScopedLock<__typeof__(x)> __l = x; if(0); else

#endif
