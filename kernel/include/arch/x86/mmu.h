// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MMU_H__
#define __MMU_H__

#include <global.h>

namespace X86 { namespace MMU {

typedef enum PdeFlags {
    PDE_PRESENT    = 1,
    PDE_READ_WRITE = 2,
    PDE_USER       = 4,
    PDE_WT         = 8,  /* Write-Through */
    PDE_CD         = 16, /* Cache Disable */
    PDE_ACCESSED   = 32,
    PDE_DEMAND     = 64, /* Table allocated on-demand by the client */
    PDE_4MB        = 128,
    PDE_MASK       = 0xFFF
} PdeFlags_t;

typedef enum PteFlags {
    PTE_PRESENT    = 1,
    PTE_READ_WRITE = 2,
    PTE_USER       = 4,
    PTE_WT         = 8,
    PTE_CD         = 16,
    PTE_ACCESSED   = 32,
    PTE_DIRTY      = 64,
    PTE_GLOBAL     = 256,
    PTE_MASK       = 0x3FF
} PteFlags_t;

static const uintptr_t PAGE_SIZE    = 0x1000;
static const uintptr_t TABLE_SIZE   = PAGE_SIZE * 1024;
static const uintptr_t PAGE_MASK    = 0xFFFFF000;
static const uintptr_t TABLE_MASK   = 0xFFFFF000;

static inline uintptr_t dirIndex(uintptr_t addr) { return addr >> 22; }
static inline uintptr_t tblIndex(uintptr_t addr) { return (addr >> 12) & PTE_MASK; }

static inline uintptr_t pageAlign(uintptr_t addr) { return ((addr & PAGE_MASK) == addr ? addr : (addr + PAGE_SIZE) & PAGE_MASK); }


typedef uintptr_t mmuAddr;
typedef struct {
    mmuAddr mmuTables[1024];
} mmuDir;

typedef struct {
    mmuAddr mmuPages[1024];
} mmuTable;

static inline uintptr_t getFaultAddress(void) {
    uintptr_t addr;
    __asm__ volatile ("mov %%cr2, %0" : "=a" (addr)::);
    return addr;
}

int mapPageFast(void* physAddr, void* virtAddr, uint32_t flags);
int mapPageRangeFast(void* startPhysAddr, void* endPhysAddr, void* startVirtAddr, void* endVirtAddr, uint32_t flags);
int mapPageRangeFast(void* physAddr, void* virtAddr, uint32_t length, uint32_t flags);
int mapPage    (void* physAddr, void* virtAddr, uint32_t flags);
int mapPageRange(void* physAddr, void* virtAddr, uint32_t length, uint32_t flags);
int mapTable   (void* physAddr, void* virtAddr, uint32_t flags);

int unmapPage (void* virtAddr);
int unmapPageRange(void* startVirtAddr, void* endVirtAddr);
int unmapPageRange(void* virtAddr, uint32_t length);

mmuAddr getPhysicalAddress(mmuAddr virtAddr);
void* getPhysicalAddress(void* virtAddr);

}

}

#endif
