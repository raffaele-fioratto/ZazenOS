// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MP_H__
#define __MP_H__

#include <global.h>
#include <mm/mm.h>
#include <iterator.h>

struct mp_float_ptr_struct {
    uint8_t signature[4];       // 0x0
    uint32_t phys_addr_ptr;     // 0x4
    uint8_t length;             // 0x8
    uint8_t spec_rev;           // 0x9
    uint8_t checksum;           // 0xA
    uint8_t features_bytes[5];  // 0xB
} __packed;

struct mp_config_table_header {
    uint8_t signature[4];       // +0
    uint16_t base_table_len;    // +4
    uint8_t spec_rev;           // +6
    uint8_t checkum;            // +7
    uint8_t oem_id[8];          // +8
    uint8_t product_id[12];     // +16
    uint32_t oem_table_ptr;     // +28
    uint16_t oem_table_len;     // +32
    uint16_t entry_count;       // +34
    uint32_t lapic_phys_addr;   // +36
    uint16_t extd_table_len;    // +40
    uint8_t extd_checksum;      // +42
    uint8_t reserved;          // +43
} __packed;

struct mp_conf_table_entry {
    uint8_t type;
} __packed;

struct mp_processor_entry {
    mp_conf_table_entry header;
    uint8_t lapic_id;
    uint8_t lapic_version_num;
    union {
        struct {
            uint8_t enabled   : 1;
            uint8_t bootstrap : 1;
            uint8_t reserved  : 6;
        } bits;
        uint8_t byte;
    } cpu_flags;
    union {
        struct {
            uint32_t stepping : 4;
            uint32_t model    : 4;
            uint32_t family   : 4;
            uint32_t reserved : 20;
        } bits;
        uint32_t word;
    } cpu_signature;
    uint32_t features_flags;
    uint32_t reserved0;
    uint32_t reserved1;
} __packed;

struct mp_bus_entry {
    mp_conf_table_entry header;
    uint8_t bus_id;
    uint8_t bus_type_string[6];
} __packed;

struct mp_ioapic_entry {
    mp_conf_table_entry header;
    uint8_t ioapic_id;
    uint8_t ioapic_version_num;
    union {
        struct {
            uint8_t enabled  : 1;
            uint8_t reserved : 7;
        } bits;
        uint8_t byte;
    } ioapic_flags;
    uint32_t ioapic_phys_addr;
} __packed;

struct mp_io_int_assign_entry {
    mp_conf_table_entry header;
    uint8_t int_type;
    union {
        struct {
            uint16_t polarity      : 2;
            uint16_t trigger_mode  : 2;
            uint16_t reserved      : 12;
        } bits;
        uint16_t byte;
    } io_int_flags;
    uint8_t src_bus_id;
    uint8_t src_bus_irq;
    uint8_t dest_ioapic_id;
    uint8_t dest_ioapic_intin_num;
};

struct mp_local_int_assign_entry {
    mp_conf_table_entry header;
    uint8_t int_type;
    union {
        struct {
            uint16_t polarity      : 2;
            uint16_t trigger_mode  : 2;
            uint16_t reserved      : 12;
        } bits;
        uint16_t byte;
    } io_int_flags;
    uint8_t src_bus_id;
    uint8_t src_bus_irq;
    uint8_t dest_lapic_id;
    uint8_t dest_lapic_lintin_num;
};

namespace X86 { namespace SMP {

class MP {
public:
    static const uint32_t MP_CONF_TABLE_ENTRIES_NUM = 5;
    static const size_t MP_CONF_TABLE_ENTRY_SIZE[MP_CONF_TABLE_ENTRIES_NUM];

    constexpr static const char*    MP_SIGNATURE_STRING = "_MP_";
    static const int                MP_SIGNATURE_STRING_LEN = 4;
    static const int                MP_SIGNATURE_STRING_ALIGN = 16;

    constexpr static const char*    MP_CONF_HDR_SIGNATURE_STRING = "PCMP";
    static const int                MP_CONF_HDR_SIGNATURE_STRING_LEN = 4;

public:
    class TablesIterator : public KLibcpp::iterator<KLibcpp::input_iterator_tag, mp_conf_table_entry> {

    public:
        TablesIterator(uintptr_t startAddr, uint32_t tablesCount);
        TablesIterator(const TablesIterator& mit);
        TablesIterator() : TablesIterator(0, 0) { }

        TablesIterator& operator++();
        TablesIterator operator++(int);
        bool operator==(const TablesIterator& rhs);
        bool operator!=(const TablesIterator& rhs);

        mp_conf_table_entry* operator*();

    private:
        uintptr_t addr;
        uint32_t  count;
    };

    MP();
    ~MP();

    template <class T>
    T* getTable(uint8_t type, TablesIterator& resumeIt) {
        TablesIterator it = resumeIt != TablesIterator(0, 0) ? resumeIt : getTablesIterator();
        TablesIterator end = getLastTable();

        for (; it != end; it++) {
            if ((*it)->type == type) {
                resumeIt = it;
                resumeIt++;
                return reinterpret_cast<T*> (*it);
            }
        }

        return NULL;
    }

    int findFloatPtrStruct();
    int parseMPConfigTable();

    TablesIterator getTablesIterator();
    TablesIterator getLastTable();

private:
    bool isFloatPtrStructChecksumValid(uint8_t* addr);
    bool isMPConfTableHdrChecksumValid();
    static MM::AddressInterval EBDA;
    static MM::AddressInterval BIOS_ROM;
    mp_float_ptr_struct *mpFloatPtrStruct = NULL;
    mp_config_table_header *mpConfigTableHeader = NULL;
    bool isValid = false;
};

extern MP mp;

} }

#endif
