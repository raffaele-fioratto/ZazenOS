// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PROCESS_H__
#define __PROCESS_H__

#include <stdint.h>
#include <arch/x86/isr.h>
#include <mm/virtual.h>
#include <mm/memrbtree.h>
#include <arch/x86/mmu.h>

namespace X86 {

struct Process {
    enum State {
        Ready = 0,
        Exec,
        Wait
    };

    ISRHandler::Registers generalRegs;
    uint32_t pid;
    State state;
    MMU::mmuAddr cr3;

    X86::Process* duplicate();

    // MM::MemRBTree<MM::AddressInterval, MM::MappedRegion*, MM::AddressIntervalEqual, MM::AddressIntervalLess> maps;
};

}

#endif /* __PROCESS_H__ */
