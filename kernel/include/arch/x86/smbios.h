// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SMBIOS_H__
#define __SMBIOS_H__

#include <global.h>
#include <iterator.h>
#include <string.h>

namespace X86 {

struct smbios_entry_point {
    uint8_t     anchor_sting[4];
    uint8_t     checksum;
    uint8_t     length;
    uint8_t     major;
    uint8_t     minor;
    uint16_t    max_size;
    uint8_t     revision;
    uint8_t     formatted_area[5];
    uint8_t     inter_anchor_string[5];
    uint8_t     inter_checksum;
    uint16_t    table_length;
    uint32_t    table_address;
    uint16_t    num_structs;
    uint8_t     bcd_revision;
} __packed;

struct smbios_hdr {
    uint8_t     type;
    uint8_t     length;
    uint16_t    handle;
} __packed;

struct smbios_cpu_info {
    smbios_hdr  header;
    uint8_t     socket_desig;
    uint8_t     cpu_type;
    uint8_t     family;
    uint8_t     manufacturer;
    uint64_t    id;
    uint8_t     version;
    uint8_t     voltage;
    uint16_t    ext_clock;
    uint16_t    max_speed;
    uint16_t    curr_speed;
    uint8_t     status;
    uint8_t     upgrade;
    uint16_t    l1_cache_handle;
    uint16_t    l2_cache_handle;
    uint16_t    l3_cache_handle;
    uint8_t     serial_num;
    uint8_t     asset_tag;
    uint8_t     part_num;
    uint8_t     core_count;
    uint8_t     core_enabled;
    uint8_t     thread_count;
    uint16_t    characteristics;
    uint16_t    family_2;
} __packed;

struct smbios_bios_info {
    smbios_hdr  header;
    uint8_t     vendor_str_idx;
    uint8_t     version_str_idx;
    uint16_t    start_addr_segm;
    uint8_t     rel_date_str_idx;
    uint8_t     rom_size;
    uint64_t    characteristics;
    uint8_t     ext_characteristics[2];
    uint8_t     sys_bios_major_release;
    uint8_t     sys_bios_minor_release;
    uint8_t     embd_cont_major_release;
    uint8_t     embd_cont_minor_release;
} __packed;

class SMBIOS {
public:
    constexpr static const char*    SMBIOS_ANCHOR_STRING = "_SM_";
    static const int                SMBIOS_ANCHOR_STRING_LEN = 4;
    static const int                SMBIOS_ANCHOR_STRING_ALIGN = 16;

    static const uintptr_t SMBIOS_START_ADDR = KERNEL_BASE_VIRT_ADDR + 0x000F0000;
    static const uintptr_t SMBIOS_END_ADDR = KERNEL_BASE_VIRT_ADDR + 0x00100000;

public:
    class TablesIterator : public KLibcpp::iterator<KLibcpp::input_iterator_tag, smbios_hdr> {

    public:
        TablesIterator(uintptr_t startAddr, uint32_t tablesCount);
        TablesIterator(const TablesIterator& mit);
        TablesIterator() : TablesIterator(0, 0) { }

        TablesIterator operator++();
        TablesIterator operator++(int);
        bool operator==(const TablesIterator& rhs);
        bool operator!=(const TablesIterator& rhs);

        smbios_hdr* operator*();

    private:
        uintptr_t addr;
        uint32_t  count;
    };

public:
    SMBIOS();
    ~SMBIOS();

    int findEntryPoint();

    template <class T>
    T* getTable(uint8_t type, TablesIterator& resumeIt) {
        TablesIterator it = resumeIt != TablesIterator(0, 0) ? resumeIt : getTablesIterator();
        TablesIterator end = getLastTable();

        for (; it != end; it++) {
            if ((*it)->type == type) {
                resumeIt = it;
                resumeIt++;
                return reinterpret_cast<T*> (*it);
            }
        }

        return NULL;
    }

    template <class T>
    const char* readStringFromTable(T* table, int idx) {
        const char* tbl = reinterpret_cast<const char*>((uintptr_t) table + table->header.length);

        int i = 1;
        while (i < idx) {
            tbl += ::strlen(tbl) + 1;
            i++;
        }

        return tbl;
    }

    TablesIterator getTablesIterator();
    TablesIterator getLastTable();

private:
    bool isChecksumValid(uint8_t* addr);
    bool isValid;
    smbios_entry_point* smbiosEPS;
};

extern X86::SMBIOS smbios;

}

#endif
