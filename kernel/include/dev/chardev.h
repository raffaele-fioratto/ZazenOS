// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CHARDEV_H__
#define __CHARDEV_H__

#include <dev/device.h>
#include <sys/types.h>
#include <ostream.h>

namespace Dev {

class CharDevice : public Device, public KLibcpp::OStreamBuffer {
protected:
    CharDevice(const char* name);
    ~CharDevice();

public:
    virtual int write(void* buffer, size_t bytes, off_t *offset);
    virtual int read(void* buffer, size_t bytes, off_t *offset);
    virtual int flush();
    int putn(void* buffer, size_t size) { return write(buffer, size, NULL); }
    int sync() { return flush(); }
};

}

#endif /* __CHARDEV_H__ */
