// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TEXTVGA_H__
#define __TEXTVGA_H__

#include <dev/chardev.h>
#include <stdint.h>

namespace Dev { namespace Video {

enum CGAColorPalette {
    Black         = 0x0,
    Blue          = 0x1,
    Green         = 0x2,
    Cyan          = 0x3,
    Red           = 0x4,
    Magenta       = 0x5,
    Brown         = 0x6,
    Gray          = 0x7,
    DarkGray      = 0x8,
    BrightBlue    = 0x9,
    BrightGreen   = 0xA,
    BrightCyan    = 0xB,
    BrightRed     = 0xC,
    BrightMagenta = 0xD,
    Yellow        = 0xE,
    White         = 0xF
};

class TextVGA : public CharDevice {
public:
    TextVGA();
    virtual ~TextVGA();

    int write(void* buffer, size_t bytes, off_t *offset);
    int flush();

private:
    int puts(char* str, size_t bytes);
    int putch(int ch);

private:
    struct Framebuffer {
    public:
        uint16_t* fbBuffer;
        uint8_t fbX, fbY;
        uint16_t readIndex, writeIndex;
        uint8_t fbRows, fbColumns;
        uint8_t fbAttribute;
        size_t fbBufSize;

        void clear();
        void backspace() { fbX--; }
        void tab() { fbX = (fbX + 8) & ~(8 - 1); }
        void carriageReturn() { fbX = 0; }
        void newline() { carriageReturn(); fbY++; }
        void write(char ch) { fbBuffer[fbColumns * fbY + fbX] = ch | (fbAttribute << 8); fbX++; }
        void scroll();
        bool isAtEol() { return fbX >= fbColumns; }
        bool isAtEof() { return fbY >= fbRows; }
        void updateCursor();
    };

    static const uintptr_t TEXT_VGA_BASE_ADDR = KERNEL_BASE_VIRT_ADDR + 0xB8000;
    static const uint8_t TEXT_VGA_MAX_FRAMEBUFFERS = 8;
    static const uint8_t TEXT_VGA_EARLY_ROWS = 25;
    static const uint8_t TEXT_VGA_EARLY_COLS = 80;

    Framebuffer framebuffers[TEXT_VGA_MAX_FRAMEBUFFERS];
    uint8_t activeFb;
    static uint16_t earlyBuffer[TEXT_VGA_EARLY_ROWS * TEXT_VGA_EARLY_COLS];
};

}}

#endif /* __TEXTVGA_H__ */
