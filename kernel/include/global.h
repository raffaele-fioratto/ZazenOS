// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include <stdint.h>
#include <stddef.h>
#include <foreach.h>

#ifndef __packed
#define __packed __attribute__((packed))
#endif /* __packed */

#ifndef __fastcall
#define __fastcall __attribute__((fastcall))
#endif /* __fastcall */

#ifndef __forceinline
#define __forceinline __attribute__((always_inline))
#endif /* __forceinline */


#define container_of(ptr, type, member) \
    ((type*) ((char*)((const __typeof__(((type*) 0)->member)*) ptr) - offsetof(type, member)))

#ifndef K_UNUSED
#define K_UNUSED(x) \
    (void) x
#endif /* K_UNUSED */

#define panic( msg ) \
do { \
KLibcpp::kout << "Panic!!! " << msg << KLibcpp::endl; \
__asm__ volatile( "int $9" ); \
} while (0);


#define ROUND_UP(x, y) ( ( x ) / ( y ) + ( ( ( x ) % ( y ) ) ? 1 : 0 ) )
#define ALIGN(x, y) (((x) & ((y) - 1) ? ((x) + (y) - ((x) & ((y) - 1))) : (x)))

#define KERNEL_BASE_VIRT_ADDR 0xC0000000

#define SOURCE_POS __FILE__ << ":" << __LINE__ << " "

#define BYTE0(x) ((x) & 0xFF)
#define BYTE1(x) (((x) >> 8) & 0xFF)
#define BYTE2(x) (((x) >> 16) & 0xFF)
#define BYTE3(x) (((x) >> 24) & 0xFF)

#define WORD0(x) ((x) & 0xFFFF)
#define WORD1(x) (((x) >> 16) & 0xFFFF)

#define DWORD0(x) ((x) & 0xFFFFFFFF)
#define DWORD1(x) (((x) >> 32) & 0xFFFFFFFF)

#endif /* __GLOBAL_H__ */
