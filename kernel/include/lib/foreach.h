// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __FOREACH_H__
#define __FOREACH_H__

// Implement foreach construct à la Qt
template<class T>
struct ForeachContainer {
    ForeachContainer(T cont) : it(cont.begin()), end(cont.end()), brk(0) { }
    typename T::Iterator it;
    typename T::Iterator end;
    int brk;
};

#define foreach(x, cont) \
    for(ForeachContainer<__typeof__(cont)> c(cont); \
        !c.brk && (c.it != c.end); \
        __extension__({ ++c.brk; ++c.it; })) \
        for(x = *(c.it);; __extension__ ({ --c.brk; break; }) )


#endif
