// Copyright (C) 2016 Fioratto Raffaele
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __FUNCTIONAL_H__
#define __FUNCTIONAL_H__

#include <global.h>
#include <stdint.h>

template<class Arg1, class Arg2, class Res> struct binary_function {
    virtual ~binary_function() { }
    virtual Res operator()(const Arg1& a1, const Arg2& a2) = 0;
};

template<class T> struct equal_to : public binary_function<T, T, bool> {
    bool operator()(const T& a1, const T& a2) { return a1 == a2; }
};

template<class T> struct not_equal_to : public binary_function<T, T, bool> {
    bool operator()(const T& a1, const T& a2) { return a1 != a2; }
};

template<class T> struct greater : public binary_function<T, T, bool> {
    bool operator()(const T& a1, const T& a2) { return a1 > a2; }
};

template<class T> struct less : public binary_function<T, T, bool> {
    bool operator()(const T& a1, const T& a2) { return a1 < a2; }
};

template<class T> struct greater_equal : public binary_function<T, T, bool> {
    bool operator()(const T& a1, const T& a2) { return a1 >= a2; }
};

template<class T> struct less_equal : public binary_function<T, T, bool> {
    bool operator()(const T& a1, const T& a2) { return a1 <= a2; }
};


#endif /* __FUNCTIONAL_H__ */
