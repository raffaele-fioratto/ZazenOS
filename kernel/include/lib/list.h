// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LIST_H__
#define __LIST_H__

#include <global.h>
#include <iterator.h>
#include <mm/heap/slab.h>

namespace KLibcpp {

template <class T>
class List {
public:
    typedef T value_type;
    typedef T& reference;
    typedef const T& const_reference;


private:
    struct ListNode {
        ListNode(const value_type& v) { value = v; next = prev = NULL; }
        template <class... Args>
        ListNode(Args&&... args) : value(args...) { }
        value_type value;
        ListNode* next;
        ListNode* prev;
    };

public:

    class Iterator : public KLibcpp::iterator<KLibcpp::input_iterator_tag, ListNode*> {
    friend class List<T>;
    public:
        Iterator(ListNode* n) { node = n; }
        Iterator(const Iterator& other) { node = other.node; }

        Iterator operator++() { node = node->next; return *this; }
        Iterator operator++(int) { Iterator temp = *this; operator++(); return temp; }

        bool operator==(const Iterator& rhs) { return node == rhs.node; }
        bool operator!=(const Iterator& rhs) { return node != rhs.node; }

        reference operator*() { return node->value; }

    private:
        ListNode* node;
    };

    class ConstIterator : public KLibcpp::iterator<KLibcpp::input_iterator_tag, ListNode*> {
        friend class List<T>;
    public:
        ConstIterator(ListNode* n) { node = n; }
        ConstIterator(const ConstIterator& other) { node = other.node; }

        ConstIterator operator++() { node = node->next; return *this; }
        ConstIterator operator++(int) { ConstIterator temp = *this; operator++(); return temp; }

        bool operator==(const ConstIterator& rhs) { return node == rhs.node; }
        bool operator!=(const ConstIterator& rhs) { return node != rhs.node; }

        const_reference operator*() const { return node->value; }

    private:
        ListNode* node;
    };

public:


    List() { headNode = NULL; endNode = NULL; pastEndNode.value = T(); pastEndNode.next = pastEndNode.prev = NULL; }
    ~List() { }

    void push_back(const value_type& value) {
        ListNode* node = allocNode(value);

        if (empty()) {
            headNode = node;
        } else {
            endNode->next = node;
            node->prev = endNode;
        }

        endNode = node;
        endNode->next = &pastEndNode;
        pastEndNode.prev = endNode;
    }

    void push_front(const value_type& value) {
        ListNode* node = allocNode(value);

        if (empty()) {
            endNode = node;
            endNode->next = &pastEndNode;
            pastEndNode.prev = endNode;
        } else {
            headNode->prev = node;
            node->next = headNode;
        }

        headNode = node;
    }

    template <class... Args>
    reference emplace_front(Args&&... args) {
        ListNode* node = allocNode(args...);

        if (empty()) {
            endNode = node;
            endNode->next = &pastEndNode;
            pastEndNode.prev = endNode;
        } else {
            headNode->prev = node;
            node->next = headNode;
        }

        headNode = node;

        return node->value;
    }

    template <class... Args>
    reference emplace_back(Args&&... args) {
        ListNode* node = allocNode(args...);

        if (empty()) {
            headNode = node;
        } else {
            endNode->next = node;
            node->prev = endNode;
        }

        endNode = node;
        endNode->next = &pastEndNode;
        pastEndNode.prev = endNode;

        return node->value;
    }

    reference back() {
        ListNode *n = endNode;
        return n->value;
    }

    const_reference back() const {
        ListNode *n = endNode;
        return n->value;
    }

    void pop_back() {
        ListNode *n = endNode;
        endNode = n->prev;
        if (endNode != &pastEndNode) {
            endNode->next = &pastEndNode;
            pastEndNode.prev = endNode;
        } else {
            endNode = NULL;
            headNode = NULL;
        }

        deallocNode(n);
    }

    reference front() {
        ListNode *n = headNode;
        return n->value;
    }

    const_reference front() const {
        ListNode *n = headNode;
        return n->value;
    }

    void pop_front() {
        ListNode *n = headNode;
        headNode = n->next;
        if (headNode != &pastEndNode) {
            headNode->prev = NULL;
        } else {
            headNode = NULL;
            endNode = NULL;
            pastEndNode.prev = NULL;
        }

        deallocNode(n);
    }

    bool empty() const{
        return headNode == NULL && endNode == NULL;
    }

    List<T>::Iterator erase(Iterator it) {
        it.node->prev->next = it.node->next;
        it.node->next->prev = it.node->prev;
        if (it.node == headNode) {
            headNode = it.node->next;
        } else if (it.node == endNode) {
            endNode = it.node->next;
        }

        return Iterator(it.node->next);
    }

    List<T>::Iterator erase(Iterator start, Iterator end) {
        while((start = erase(start)) != end);
        return start;
    }

    void remove(const value_type& value) {
        Iterator it = begin();
        Iterator itEnd = end();

        for (; it != itEnd; it++) {
            if (*it == value) {
                erase(it);
            }
        }
    }

    void clear() {
        ListNode* n = headNode;
        ListNode* d = n;
        while(n != &pastEndNode) {
            n = n->next;

            deallocNode(d);
            d = n;
        }

        headNode = NULL;
        endNode = NULL;
        pastEndNode.value = T();
        pastEndNode.next = pastEndNode.prev = NULL;
    }

    List<T>::Iterator begin() { return Iterator(headNode); }
    List<T>::Iterator end() { return Iterator(&pastEndNode); }

private:
    ListNode* headNode;
    ListNode* endNode;
    ListNode pastEndNode;


    ListNode* allocNode(const T& v) {
        return new ListNode(v);
    }

    template <class... Args>
    ListNode* allocNode(Args&&... args) {
        return new ListNode(args...);
    }

    void deallocNode(ListNode* n) {
        MM::kfree(n, sizeof(ListNode));
    }
};

}

#endif /* __LIST_H__ */
