// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __OSTREAM_H__
#define __OSTREAM_H__

#include <global.h>

namespace KLibcpp {

namespace FmtFlags {

static const uint32_t zero        = 0x0;
static const uint32_t boolalpha   = 0x1;
static const uint32_t showbase    = 0x2;
static const uint32_t showpoint   = 0x4;
static const uint32_t showpos     = 0x8;
static const uint32_t skipws      = 0x10;
static const uint32_t unitbuf     = 0x20;
static const uint32_t uppercase   = 0x40;
static const uint32_t dec         = 0x80;
static const uint32_t hex         = 0x100;
static const uint32_t oct         = 0x200;
static const uint32_t fixed       = 0x400;
static const uint32_t scientific  = 0x800;
static const uint32_t internal    = 0x1000;
static const uint32_t left        = 0x2000;
static const uint32_t right       = 0x4000;

}

class OStreamBuffer {

public:
    OStreamBuffer() { }
    virtual ~OStreamBuffer() { }

    virtual int putn(void* buffer, size_t size) = 0;
    virtual int sync() = 0;

};

class StringBuffer : public OStreamBuffer {
public:
    StringBuffer(size_t size);
    ~StringBuffer();
    int putn(void* buffer, size_t size);
    int sync() { return 0; };
    const char* toString() const;

private:
    char* stringBuffer;
    uint32_t maxSize;
    uint32_t index;
};

class OStream {

public:
    OStream();
    ~OStream();

    OStream& operator <<(bool b);
    OStream& operator <<(const char* str);
    OStream& operator <<(char* buf);
    OStream& operator <<(unsigned short val);
    OStream& operator <<(short val);
    OStream& operator <<(unsigned long long val);
    OStream& operator <<(long long val);
    OStream& operator <<(unsigned long val);
    OStream& operator <<(long val);
    OStream& operator <<(int val);
    OStream& operator <<(unsigned int val);
    OStream& operator <<(void* ptr);
    OStream& operator <<(OStream& (*pf)(OStream&));

    OStream& setFmtFlags(unsigned flag);
    OStream& unsetFmtFlags(unsigned int flag);

    int saveOptions();
    int restoreOptions();

    char        getFill();
    OStream&    setFill(char fillch);

    size_t      getWidth();
    OStream&    setWidth(size_t width);

    OStream& put(char ch);
    OStream& write(const char *str);
    OStream& flush();

    int registerOutputBuffer(OStreamBuffer* buf);
    int deregisterOutputBuffer(OStreamBuffer* buf);

private:
    static const uint32_t OSTREAM_MAX_OUT_BUFS = 10;
    static const uint32_t OSTREAM_MAX_SAVED_OPTIONS = 10;
    OStreamBuffer* outputBuffers[OSTREAM_MAX_OUT_BUFS];
    uint32_t numRegisteredOutputBuffers;

    struct Options {
        size_t fillWidth;
        char fillChar;
        int numBase;
        unsigned int fmtFlags;
    };

    Options curOpts;

    uint32_t optStackTop;
    Options optsStack[OSTREAM_MAX_SAVED_OPTIONS];

    template <class T>
    size_t itoa(T value, char* result, int base);

    template <class T>
    size_t uitoa(T value, char* result, int base);

    void fill(size_t num, char fillch);
};

OStream& endl(OStream& os);
OStream& hex(OStream& os);
OStream& dec(OStream& os);
OStream& oct(OStream& os);
OStream& boolalpha(OStream& os);
OStream& noboolalpha(OStream& os);
OStream& uppercase(OStream& os);
OStream& nouppercase(OStream& os);

extern OStream kout;
}

#endif /* __OSTREAM_H__ */
