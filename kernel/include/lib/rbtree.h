// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RBTREE_H__
#define __RBTREE_H__

#include <functional.h>
#include <mm/heap/slab.h>

template <class K, class V, class Equal = equal_to<K>, class Compare = less<K>>
class RBTree {
private:
    struct __Node {
        enum Color { BLACK, RED };
        __Node(K k, V v, Color c, __Node* l, __Node* r, __Node* p) {
            key = k;
            value = v;
            color = c;
            left = l;
            right = r;
            parent = p;
        }

        bool isRed() { return color == RED; }
        bool isBlack() { return color == BLACK; }
        void setRed() { color = RED; }
        void setBlack() { color = BLACK; }

        K key;
        V value;

        Color color;
        __Node* left;
        __Node* right;
        __Node* parent;
    };

private:
    __Node* rootNode;
    Equal equalPred;
    Compare lessThanPred;
    __Node sentinel;
    __Node* NIL;

public:
    typedef K key_type;
    typedef V value_type;

    class Iterator : KLibcpp::iterator<KLibcpp::input_iterator_tag, __Node*> {

    public:
        Iterator(__Node* x, __Node* nil) { node = x; NIL = nil; }
        Iterator(const Iterator& other) { node = other.node; }

        Iterator operator++() { increment(); return *this; }
        Iterator operator++(int) { Iterator temp = *this; increment(); return temp; }
        bool operator==(const Iterator& rhs) { return node == rhs.node; }
        bool operator!=(const Iterator& rhs) { return node != rhs.node; }

        value_type& operator*() { return node->value; }

        key_type& key() { return node->key; }
        value_type& value() { return node->value; }

    private:
        void increment() {
            if (node->right != NIL) {
                node = node->right;
                while (node->left != NIL)
                    node = node->left;
            } else {
                __Node* y = node->parent;
                while (node == y->right) {
                    node = y;
                    y = y->parent;
                }

                if (node->right != y) {
                    node = y;
                } else {
                    node = NIL;
                }
            }
        }

        __Node* NIL;
        __Node* node;
    };

    class ConstIterator : KLibcpp::iterator<KLibcpp::input_iterator_tag, __Node*> {

    public:
        ConstIterator(__Node* x, __Node* nil) { node = x; NIL = nil; }
        ConstIterator(const ConstIterator& other) { node = other.node; }

        ConstIterator operator++() { increment(); return *this; }
        ConstIterator operator++(int) { ConstIterator temp = *this; increment(); return temp; }
        bool operator==(const ConstIterator& rhs) { return node == rhs.node; }
        bool operator!=(const ConstIterator& rhs) { return node != rhs.node; }

        const value_type& operator*() { return node->value; }

        const key_type& key() const { return node->key; }
        const value_type& value() const { return node->value; }

    private:
        void increment() {
            if (node->right != NIL) {
                node = node->right;
                while (node->left != NIL)
                    node = node->left;
            } else {
                __Node* y = node->parent;
                while (node == y->right) {
                    node = y;
                    y = y->parent;
                }

                if (node->right != y) {
                    node = y;
                } else {
                    node = NIL;
                }
            }
        }

        __Node* NIL;
        __Node* node;
    };

public:
    RBTree() : sentinel(K(), V(), __Node::BLACK, &sentinel, &sentinel, &sentinel) {
        NIL = &sentinel;
        rootNode = NIL;
    }

    ~RBTree() { }

    void insert(const key_type& key, const value_type& value) {
        __Node *n = allocNode(key, value);
        _insert(n);
    }

    void erase(const key_type& key) {
        __Node* n = _find(key);
        if (n != NULL) {
            _remove(n);
            deallocNode(n);
        }
    }

    Iterator find(const key_type& key) {
        __Node* n = _find(key);
        if (n != NULL) {
            return Iterator(n, NIL);
        }
        return Iterator(NIL, NIL);
    }


    value_type& findMinimumGreater(const key_type& key) {
        __Node* n = _findMinimumGreater(key);
        if (n != NULL) {
            return n->value;
        }
        return NIL->value;
    }

    bool empty() const {
        return rootNode == NIL;
    }

    value_type& operator[] (const key_type& key) {
        __Node* n =  _find(key);
        if (n != NULL) {
            return n->value;
        }
        return NIL->value;
    }

    Iterator begin() { return Iterator(treeMinimum(rootNode), NIL); }
    Iterator end() { return Iterator(NIL, NIL); }

private:
    void _insert(__Node* z) {
        __Node* y = NIL;
        __Node* x = rootNode;
        while (x != NIL) {
            y = x;
            if (lessThanPred(z->key, x->key)) {
                x = x->left;
            } else {
                x = x->right;
            }
        }

        z->parent = y;
        if (y == NIL) {
            rootNode = z;
        } else if (lessThanPred(z->key, y->key)) {
            y->left = z;
        } else {
            y->right = z;
        }

        z->left = NIL;
        z->right = NIL;
        z->setRed();

        insertFixup(z);
    }

    void _remove(__Node* z) {
        __Node* y;
        __Node* x;
        if (z->left == NIL || z->right == NIL) {
            y = z;
        } else {
            y = treeSuccessor(z);
        }

        if (y->left != NIL) {
            x = y->left;
        } else {
            x = y->right;
        }

        x->parent = y->parent;

        if (y->parent == NIL) {
            rootNode = x;
        } else if(y == y->parent->left) {
            y->parent->left = x;
        } else {
            y->parent->right = x;
        }

        if (y != z) {
            z->key = y->key;
        }

        if (y->isBlack()) {
            deleteFixup(x);
        }
    }
    __Node* _find(K key) {
        __Node* x = rootNode;
        while (x != NIL) {
            if (equalPred(key, x->key)) {
                return x;
            } else if (lessThanPred(key, x->key)) {
                x = x->left;
            } else {
                x = x->right;
            }
        }
        return NULL;
    }

    __Node* _findMinimumGreater(K key) {
        __Node* x = rootNode;
        __Node* maxNode = treeMaximum(x);

        if (lessThanPred(maxNode->key, key)) {
            return NULL;
        } else if (equalPred(key, maxNode->key)) {
            return maxNode;
        }

        while (x != NIL) {
            if (equalPred(key, x->key)) {
                return x;
            } else if (lessThanPred(key, x->key)) {
                maxNode = treeMaximum(x->left);
                if (lessThanPred(maxNode->key, key)) {
                    return x;
                } else if (equalPred(key, maxNode->key)) {
                    return maxNode;
                }
                x = x->left;
            } else {
                x = x->right;
            }
        }

        return maxNode;
    }

    void rotateLeft(__Node* x) {
        __Node* y = x->right;

        x->right = y->left;
        if (y->left != NIL) {
            y->left->parent = x;
        }

        y->parent = x->parent;
        if (x->parent == NIL) {
            rootNode = y;
        } else if (x == x->parent->left) {
            x->parent->left = y;
        } else {
            x->parent->right = y;
        }

        y->left = x;
        x->parent = y;
    }

    void rotateRight(__Node* x) {
        __Node* y = x->left;

        x->left = y->right;

        if (y->right != NIL) {
            y->right->parent= x;
        }

        y->parent = x->parent;
        if (x->parent == NIL) {
            rootNode = y;
        } else if (x == x->parent->right) {
            x->parent->right = y;
        } else {
            x->parent->left = y;
        }

        y->right = x;
        x->parent = y;
    }

    void insertFixup(__Node* z) {
        if (z == rootNode) {
            rootNode->setBlack();
        } else {
            __Node* x = z->parent;
            if (x->isRed()) {
                if (x == x->parent->left) {
                    __Node* y = x->parent->right;
                    if (y->isRed()) {
                        x->setBlack();
                        y->setBlack();
                        x->parent->setRed();
                        insertFixup(x->parent);
                    } else {
                        if (z == x->right) {
                            z = x;
                            rotateLeft(z);
                            x = z->parent;
                        }
                        x->setBlack();
                        x->parent->setRed();
                        rotateRight(x->parent);
                    }
                } else {
                    __Node* y = x->parent->left;
                    if (y->isRed()) {
                        x->setBlack();
                        y->setBlack();
                        x->parent->setRed();
                        insertFixup(x->parent);
                    } else {
                        if (z == x->left) {
                            z = x;
                            rotateRight(z);
                            x = z->parent;
                        }
                        x->setBlack();
                        x->parent->setRed();
                        rotateLeft(x->parent);
                    }
                }
            }
        }
    }

    void deleteFixup(__Node* x) {
        __Node* w;
        if (x->isRed() || x->parent == NIL) {
            x->setBlack();
        } else if (x == x->parent->left) {
            w = x->parent->right;
            if (w->isRed()) {
                w->setBlack();
                x->parent->setRed();
                rotateLeft(x->parent);
                w = x->parent->right;
            }

            if (w->left->isBlack() && w->right->isBlack()) {
                w->setRed();
                deleteFixup(x->parent);
            } else {
                if (w->right->isBlack()) {
                    w->left->setBlack();
                    w->setRed();
                    rotateRight(w);
                    w = x->parent->right;
                }

                w->color = x->parent->color;
                x->parent->setBlack();
                w->right->setBlack();
                rotateLeft(x->parent);
            }
        } else {
            w = x->parent->left;
            if (w->isRed()) {
                w->setBlack();
                x->parent->setRed();
                rotateRight(x->parent);
                w = x->parent->left;
            }

            if (w->right->isBlack() && w->left->isBlack()) {
                w->setRed();
                deleteFixup(x->parent);
            } else {
                if (w->left->isBlack()) {
                    w->right->setBlack();
                    w->setRed();
                    rotateLeft(w);
                    w = x->parent->left;
                }

                w->color = x->parent->color;
                x->parent->setBlack();
                w->left->setBlack();
                rotateRight(x->parent);
            }
        }
    }

    __Node* treeMinimum(__Node* x) {
        while (x->left != NIL) {
            x = x->left;
        }

        return x;
    }

    __Node* treeMaximum(__Node* x) {
        while (x->right != NIL) {
            x = x->right;
        }

        return x;
    }

    __Node* treeSuccessor(__Node* x) {
        __Node* y;
        if (x->right != NIL) {
            return treeMinimum(x->right);
        }

        y = x->parent;
        while (y != NIL && x == y->right) {
            x = y;
            y = y->parent;
        }
        return y;
    }

    __Node* allocNode(K k, V v) {
        __Node* node = new __Node(k, v, __Node::BLACK, NIL, NIL, NIL);
        return node;
    }

    void deallocNode(__Node* n) {
        MM::kfree(n, sizeof(__Node));
    }


};

#endif /* __RBTREE_H__ */
