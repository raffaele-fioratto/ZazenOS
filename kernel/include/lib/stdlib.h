#ifndef __STDLIB_H__
#define __STDLIB_H__

#ifdef __cplusplus

extern "C" {

#endif

typedef __SIZE_TYPE__ size_t;

void reverse (char* str);
char *itoa (int n, char *str, int base);
int sgn(int x);
int abs (int x);

char *_tolower (char *str);
int _isdigit (char ch);
char *itoa_fill (int n, char *str, int base, int fill_chars, char filler);
char *uitoa_fill (unsigned int n, char *str, int base, int fill_chars, char filler);
char *itoa64_fill (long long n, char *str, long long base, int fill_chars, char filler);
char *uitoa64_fill (unsigned long long n, char *str, long long base, int fill_chars, char filler);
int atoi (char *str);

static inline int max(int a, int b) {
	return a > b ? a : b;
}

static inline int min(int a, int b) {
	return a > b ? b : a;
}

#ifdef __cplusplus

}

#endif

#endif /* __STDLIB_H__ */
