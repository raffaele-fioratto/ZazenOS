#ifndef __STRING_H__
#define __STRING_H__

#include <stddef.h>
#include <stdarg.h>

#ifdef __cplusplus

extern "C" {

#endif

size_t strlen( const char *str );
char *strcpy( char *to, const char *from );
char* strncpy(char *to, const char *from, size_t n);
int strcmp( const char *str1, const char *str2 );
int strncmp( const char* str1, const char* str, size_t n );
int snprintf( char* s, size_t size, const char* fmt, ... );

void *memcpy( void *to, const void *from, size_t n );
void *memset( void *to, int value, size_t n );
int memcmp( const void *m1, const void *m2, size_t n );
void* memmove( void* to, const void* from, size_t n );

int vsnprintf (char *str, size_t size, const char *fmt, va_list va);

/* not part of the standard (added by me) */
char* strtrim( char* str );
void* memset_32 ( void* to, unsigned int value, size_t dwords);

#ifdef __cplusplus

}

#endif

#endif /* __STRING_H__ */
