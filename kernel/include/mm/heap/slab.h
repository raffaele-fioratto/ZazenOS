// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SLAB_H__
#define __SLAB_H__

#include <mm/mm.h>
#include <mm/physical.h>
#include <mm/memrbtree.h>
#include <mm/memlist.h>
#include <new.h>

#include <arch/x86/locks.h>

namespace MM {

const uint32_t SLAB_CACHE_NAMELEN = 20;
const uint32_t KMALLOC_NUM_CACHES = 13;

struct Slab {
    uint32_t inuse;
    uintptr_t* freeObjsPtr;
    uint32_t freeIndex;
    AddressInterval slabAddr;
};

typedef MemListNode<Slab> SlabNode;
typedef MemList<Slab>     SlabList;

class SlabCache {
    friend class SlabManager;

public:
    enum Flags {
        None = 0,
        SlabOff = 1,
        PhysImplMultiplePages = 2
    };

    SlabCache();
    virtual ~SlabCache();
    template <class T, typename... Arguments> T* newObject(Arguments... parameters) { return new(allocObject()) T(parameters...); }
    template <class T> void deleteObject(T* obj) { obj->~T(); freeObject(obj); }
    void* allocObject();
    int freeObject(void* addr);
    int shrink();
    bool isSlabOff() { return flags & SlabOff; }

protected:
    int allocAndMapVirtSpace(AddressInterval& out);
    int deallocAndUnmapVirtSpace(AddressInterval in);
    virtual int allocSlab(AddressInterval objsAddr);

    SlabList full;
    SlabList partial;
    SlabList free;
    uint32_t objSize;
    uint32_t objsPerSlab;
    uint32_t flags;
    uint32_t pages;
    Physical::Zone physZone;
    SlabCache* objsArrayCache;
    char name[SLAB_CACHE_NAMELEN];
    X86::Spinlock spinlock;
};

class SlabManager {
    friend class SlabCache;
public:
    SlabManager() { }
    SlabCache* cacheCreate(const char* name, uint32_t objectSize,
            Physical::Zone zone = Physical::Zone::Normal, uint32_t pagesPerSlab = 1, uint32_t flags = SlabCache::Flags::None);
    void cacheDestroy(SlabCache* cache);

    static void init();

private:
    void* allocVirtSpace(uint32_t size);
    int   freeVirtSpace(AddressInterval interval);
    typedef MemRBNode<uint32_t, AddressInterval> FreeVirtSpaceNode;
    MemRBTree<uint32_t, AddressInterval> freeVirtualSpace;
};

extern MM::SlabManager slab;

void* kmalloc(size_t size);
int kfree(void* addr, size_t size);
int kfree(void* addr);
}

#endif
