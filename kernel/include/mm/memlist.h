// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MEMLIST_H__
#define __MEMLIST_H__

#include <iterator.h>

namespace MM {

template <class V> class MemList;

template <class V> class MemListNode {
    friend class MemList<V>;

public:
    MemListNode() { prev = next = this; }
    MemListNode(V v) : value(v) { prev = next = this; }

    void setValue(V v) { value = v; }
    V& getValue() { return value; }

private:
    V value;
    MemListNode<V> *prev;
    MemListNode<V> *next;
};

template <class V> class MemList {
    typedef MemListNode<V> __Node;
public:
    class Iterator : public KLibcpp::iterator<KLibcpp::input_iterator_tag, __Node*> {
    public:
        Iterator(__Node* n, __Node* h, bool wrappedAround = false) { node = n; handle = h; hasWrappedAround = wrappedAround; }
        Iterator(const Iterator& other) { node = other.node; }

        Iterator operator++() { node = node->next; if (handle == node) { hasWrappedAround = !hasWrappedAround; } return *this; }
        Iterator operator++(int) { Iterator temp = *this; operator++(); return temp; }

        bool operator==(const Iterator& rhs) { return node == rhs.node && handle == rhs.handle && hasWrappedAround == rhs.hasWrappedAround; }
        bool operator!=(const Iterator& rhs) { return node != rhs.node || handle != rhs.handle || hasWrappedAround != rhs.hasWrappedAround; }

        __Node* operator*() { return node; }

    private:
        __Node* node;
        __Node* handle;
        bool hasWrappedAround;
    };

    MemList() { handle = NULL; }

    bool isEmpty() { return (handle == NULL); }

    void push_front(MemListNode<V>* node) {
        if (!isEmpty()) {
            node->next = handle;
            node->prev = handle->prev;

            handle->prev->next = node;
            handle->prev = node;
        }

        handle = node;
    }

    MemListNode<V>* pop_front() {
        if (isEmpty()) {
            return NULL;
        }

        MemListNode<V>* ret = handle;
        handle->prev->next = handle->next;
        handle->next->prev = handle->prev;

        handle = handle->next;

        if (ret == handle) {
            handle = NULL;
        }

        return ret;
    }

    void push_back(MemListNode<V>* node) {
        if (!isEmpty()) {
            node->prev = handle->prev;
            node->next = handle;

            handle->prev->next = node;
            handle->prev = node;
        } else {
            handle = node;
        }

    }

    MemListNode<V>* pop_back() {
        if (isEmpty()) {
            return NULL;
        }

        MemListNode<V>* ret = handle->prev;
        ret->prev->next = handle;
        handle->prev = ret->prev;

        if (ret == handle) {
            handle = NULL;
        }

        return ret;
    }

    MemListNode<V>* front() {
        return handle;
    }

    void remove(MemListNode<V>* n) {
        if (n == n->next && n == n->prev) {
            handle = NULL;
            return;
        }

        n->prev->next = n->next;
        n->next->prev = n->prev;

        if (n == handle) {
            handle = n->next;
        }
    }

    MemList::Iterator begin() { return Iterator(handle, handle); }
    MemList::Iterator end() { return Iterator(handle, handle, true); }

private:
    MemListNode<V> *handle;
};


}

#endif
