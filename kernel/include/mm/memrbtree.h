// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MEMRBTREE_H__
#define __MEMRBTREE_H__

#include <functional.h>
#include <iterator.h>

namespace MM {

template <class K, class V, class Equal = equal_to<K>, class Compare = less<K>> class MemRBTree;

template <class K, class V, class Equal = equal_to<K>, class Compare = less<K>>
class MemRBNode {
    friend class MemRBTree<K, V, Equal, Compare>;

public:
    MemRBNode() : key(), value() { }
    MemRBNode(V v) : key(), value(v) { }

    void setValue(V v) { value = v; }

    K& getKey() { return key; }
    V& getValue() { return value; }

private:
    typedef MemRBNode<K, V, Equal, Compare> __Node;
    enum Color { BLACK, RED };

    MemRBNode(K k, V v, Color c, __Node* l, __Node* r, __Node* p) {
        key = k;
        value = v;
        color = c;
        left = l;
        right = r;
        parent = p;
    }

    bool isRed() { return color == RED; }
    bool isBlack() { return color == BLACK; }
    void setRed() { color = RED; }
    void setBlack() { color = BLACK; }

    K key;
    V value;

    Color color;
    __Node* left;
    __Node* right;
    __Node* parent;

};

template <class K, class V, class Equal, class Compare>
class MemRBTree {
private:
    typedef MemRBNode<K, V, Equal, Compare> __Node;
    __Node* rootNode;
    Equal equalPred;
    Compare lessThanPred;
    __Node sentinel;
    __Node* NIL;

public:
    class MemRBTreeIterator : KLibcpp::iterator<KLibcpp::input_iterator_tag, __Node*> {

    public:
        MemRBTreeIterator(__Node* x, __Node* nil) { node = x; NIL = nil; }
        MemRBTreeIterator(const MemRBTreeIterator& other) { node = other.node; }

        MemRBTreeIterator operator++() { increment(); return *this; }
        MemRBTreeIterator operator++(int) { MemRBTreeIterator temp = *this; increment(); return temp; }
        bool operator==(const MemRBTreeIterator& rhs) { return node == rhs.node; }
        bool operator!=(const MemRBTreeIterator& rhs) { return node != rhs.node; }

        __Node* operator*() { return node; }

    private:
        void increment() {
            if (node->right != NIL) {
                node = node->right;
                while (node->left != NIL)
                    node = node->left;
            } else {
                __Node* y = node->parent;
                while (node == y->right) {
                    node = y;
                    y = y->parent;
                }

                if (node->right != y) {
                    node = y;
                } else {
                    node = NIL;
                }
            }
        }

        __Node* NIL;
        __Node* node;
    };

    MemRBTree() : sentinel(K(), V(), __Node::BLACK, &sentinel, &sentinel, &sentinel) {

        NIL = &sentinel;
        rootNode = NIL;
    }
    ~MemRBTree() { }

    void insert(K key, __Node* z) {
        __Node* y = NIL;
        __Node* x = rootNode;
        while (x != NIL) {
            y = x;
            if (lessThanPred(key, x->key)) {
                x = x->left;
            } else {
                x = x->right;
            }
        }

        z->parent = y;
        if (y == NIL) {
            rootNode = z;
        } else if (lessThanPred(key, y->key)) {
            y->left = z;
        } else {
            y->right = z;
        }

        z->key = key;
        z->left = NIL;
        z->right = NIL;
        z->setRed();

        insertFixup(z);
    }

    void remove(__Node* z) {
        __Node* y;
        __Node* x;
        if (z->left == NIL || z->right == NIL) {
            y = z;
        } else {
            y = treeSuccessor(z);
        }

        if (y->left != NIL) {
            x = y->left;
        } else {
            x = y->right;
        }

        x->parent = y->parent;

        if (y->parent == NIL) {
            rootNode = x;
        } else if(y == y->parent->left) {
            y->parent->left = x;
        } else {
            y->parent->right = x;
        }

        if (y != z) {
            z->key = y->key;
        }

        if (y->isBlack()) {
            deleteFixup(x);
        }
    }
    __Node* find(K key) {
        __Node* x = rootNode;
        while (x != NIL) {
            if (equalPred(key, x->key)) {
                return x;
            } else if (lessThanPred(key, x->key)) {
                x = x->left;
            } else {
                x = x->right;
            }
        }
        return NULL;
    }

    __Node* findMinimumGreater(K key) {
        __Node* x = rootNode;
        __Node* maxNode = treeMaximum(x);

        if (lessThanPred(maxNode->key, key)) {
            return NULL;
        } else if (equalPred(key, maxNode->key)) {
            return maxNode;
        }

        while (x != NIL) {
            if (equalPred(key, x->key)) {
                return x;
            } else if (lessThanPred(key, x->key)) {
                maxNode = treeMaximum(x->left);
                if (lessThanPred(maxNode->key, key)) {
                    return x;
                } else if (equalPred(key, maxNode->key)) {
                    return maxNode;
                }
                x = x->left;
            } else {
                x = x->right;
            }
        }

        return maxNode;
    }

    __Node* leftmostNode() {
        return treeMinimum(rootNode);
    }

    __Node* rightmostNode() {
        return treeMaximum(rootNode);
    }

    bool isEmpty() {
        return rootNode == NIL;
    }

    MemRBTreeIterator begin() { return MemRBTreeIterator(treeMinimum(rootNode), NIL); }
    MemRBTreeIterator end() { return MemRBTreeIterator(NIL, NIL); }

private:
    void rotateLeft(__Node* x) {
        __Node* y = x->right;

        x->right = y->left;
        if (y->left != NIL) {
            y->left->parent = x;
        }

        y->parent = x->parent;
        if (x->parent == NIL) {
            rootNode = y;
        } else if (x == x->parent->left) {
            x->parent->left = y;
        } else {
            x->parent->right = y;
        }

        y->left = x;
        x->parent = y;
    }

    void rotateRight(__Node* x) {
        __Node* y = x->left;

        x->left = y->right;

        if (y->right != NIL) {
            y->right->parent= x;
        }

        y->parent = x->parent;
        if (x->parent == NIL) {
            rootNode = y;
        } else if (x == x->parent->right) {
            x->parent->right = y;
        } else {
            x->parent->left = y;
        }

        y->right = x;
        x->parent = y;
    }

    void insertFixup(__Node* z) {
        if (z == rootNode) {
            rootNode->setBlack();
        } else {
            __Node* x = z->parent;
            if (x->isRed()) {
                if (x == x->parent->left) {
                    __Node* y = x->parent->right;
                    if (y->isRed()) {
                        x->setBlack();
                        y->setBlack();
                        x->parent->setRed();
                        insertFixup(x->parent);
                    } else {
                        if (z == x->right) {
                            z = x;
                            rotateLeft(z);
                            x = z->parent;
                        }
                        x->setBlack();
                        x->parent->setRed();
                        rotateRight(x->parent);
                    }
                } else {
                    __Node* y = x->parent->left;
                    if (y->isRed()) {
                        x->setBlack();
                        y->setBlack();
                        x->parent->setRed();
                        insertFixup(x->parent);
                    } else {
                        if (z == x->left) {
                            z = x;
                            rotateRight(z);
                            x = z->parent;
                        }
                        x->setBlack();
                        x->parent->setRed();
                        rotateLeft(x->parent);
                    }
                }
            }
        }
    }

    void deleteFixup(__Node* x) {
        __Node* w;
        if (x->isRed() || x->parent == NIL) {
            x->setBlack();
        } else if (x == x->parent->left) {
            w = x->parent->right;
            if (w->isRed()) {
                w->setBlack();
                x->parent->setRed();
                rotateLeft(x->parent);
                w = x->parent->right;
            }

            if (w->left->isBlack() && w->right->isBlack()) {
                w->setRed();
                deleteFixup(x->parent);
            } else {
                if (w->right->isBlack()) {
                    w->left->setBlack();
                    w->setRed();
                    rotateRight(w);
                    w = x->parent->right;
                }

                w->color = x->parent->color;
                x->parent->setBlack();
                w->right->setBlack();
                rotateLeft(x->parent);
            }
        } else {
            w = x->parent->left;
            if (w->isRed()) {
                w->setBlack();
                x->parent->setRed();
                rotateRight(x->parent);
                w = x->parent->left;
            }

            if (w->right->isBlack() && w->left->isBlack()) {
                w->setRed();
                deleteFixup(x->parent);
            } else {
                if (w->left->isBlack()) {
                    w->right->setBlack();
                    w->setRed();
                    rotateLeft(w);
                    w = x->parent->left;
                }

                w->color = x->parent->color;
                x->parent->setBlack();
                w->left->setBlack();
                rotateRight(x->parent);
            }
        }
    }

    __Node* treeMinimum(__Node* x) {
        while (x->left != NIL) {
            x = x->left;
        }

        return x;
    }

    __Node* treeMaximum(__Node* x) {
        while (x->right != NIL) {
            x = x->right;
        }

        return x;
    }

    __Node* treeSuccessor(__Node* x) {
        __Node* y;
        if (x->right != NIL) {
            return treeMinimum(x->right);
        }

        y = x->parent;
        while (y != NIL && x == y->right) {
            x = y;
            y = y->parent;
        }
        return y;
    }
};

}

#endif /* __MEMRBTREE_H__ */
