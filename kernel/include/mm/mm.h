// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MM_H__
#define __MM_H__

#include <stdint.h>
#include <stdlib.h>
#include <functional.h>

namespace MM {

struct AddressInterval {
    template<class T>
    class Iterator {
    public:
        Iterator(uintptr_t addr) { ptr = reinterpret_cast<T*>(addr); }
        Iterator(const Iterator& other) { ptr = other.ptr; }

        Iterator& operator++() { ptr++; return *this;}
        Iterator operator++(int) { Iterator it(this); operator++(); return it; }
        Iterator& operator--() { ptr--; return *this; }
        Iterator operator--(int) { Iterator it(this); operator--(); return it; }
        Iterator& operator+=(int v) { ptr += v; return *this; }
        Iterator& operator-=(int v) { ptr -= v; return *this; }

        bool operator==(const Iterator& rhs) { return ptr == rhs.ptr; }
        bool operator!=(const Iterator& rhs) { return ptr != rhs.ptr; }
        T* operator&() { return ptr; }
        T operator*() { return *ptr; }
    private:
        T* ptr;
    };

    AddressInterval() { startAddr = 0; length = 0; }
    AddressInterval(uintptr_t start, uintptr_t len) { startAddr = start; length = len; }
    AddressInterval(void* start, uintptr_t len) { startAddr = reinterpret_cast<uintptr_t>(start); length = len; }
    template <class T>
    AddressInterval::Iterator<T> begin() { return AddressInterval::Iterator<T>(startAddr); }

    template <class T>
    AddressInterval::Iterator<T> end() { return AddressInterval::Iterator<T>(startAddr + length); }

    uintptr_t startAddr;
    uintptr_t length;

    bool intersect(const AddressInterval& other, AddressInterval& out) const {
        if (other.length == 0) {
            return false;
        }

        uintptr_t endAddrInt2 = other.startAddr + other.length;

        if (this->length == 0) {
            if (other.startAddr >= this->startAddr) {
                out.startAddr = other.startAddr;
                out.length = other.length;
                return true;
            } else if (endAddrInt2 > this->startAddr) {
                out.startAddr = this->startAddr;
                out.length = endAddrInt2 - this->startAddr;
                return true;
            }
        } else {
            uintptr_t endAddrInt1 = this->startAddr + this->length;
            if (other.startAddr >= this->startAddr && other.startAddr < endAddrInt1) {
                out.startAddr = other.startAddr;
                out.length = ::min(endAddrInt1, endAddrInt2) - other.startAddr;
                return true;
            } else if (endAddrInt2 >= this->startAddr && endAddrInt2 < endAddrInt1) {
                out.startAddr = this->startAddr;
                out.length = endAddrInt2 - this->startAddr;
                return true;
            } else if (other.startAddr < this->startAddr && endAddrInt2 > endAddrInt1) {
                out.startAddr = this->startAddr;
                out.length = this->length;
                return true;
            }
        }

        return false;
    }

    bool contains(const AddressInterval& other) {
        return startAddr <= other.startAddr && length >= other.length;
    }
};

struct AddressIntervalEqual : public binary_function<AddressInterval, AddressInterval, bool> {
    bool operator()(const AddressInterval& a1, const AddressInterval& a2) { return a1.startAddr == a2.startAddr && a1.length == a2.length; }
};

struct AddressIntervalLess : public binary_function<AddressInterval, AddressInterval, bool> {
    bool operator()(const AddressInterval& a1, const AddressInterval& a2) {
        AddressInterval out;
        return !(a1.intersect(a2, out)) && a1.startAddr < a2.startAddr; }
};

}

#endif /* __MM_H__ */
