// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __BUDDY_H__
#define __BUDDY_H__

#include <mm/memrbtree.h>
#include <mm/physical.h>

namespace MM  {

const int32_t BUDDY_NUM = 9;
const int32_t BUDDY_MAX_ORDER = BUDDY_NUM - 1;

typedef MemRBNode<int, int> BuddyZoneNode;
typedef MemRBTree<int, int> BuddyZone;

class PhysicalBuddy : public PhysicalImplementer {
public:
    PhysicalBuddy();
    ~PhysicalBuddy();

    void* allocatePage();
    void* allocatePages(uint32_t num);

    void deallocatePage(void* addr);
    void deallocatePages(void* addr, uint32_t num);

    void printStatistics();

    bool supportsMultiplePages() { return true; }

    int init(uintptr_t startAddr, uintptr_t regionLength);

private:
    BuddyZone zones[BUDDY_NUM];
    BuddyZoneNode* freeNodes;
    BuddyZoneNode** freeNodesStack;
    int32_t pageSizeOrder;
    size_t minBlockSize;
    size_t maxBlockSize;
    size_t freeNodesIndex;
    size_t freeNodesMaxIndex;

private:
    void freeMemoryRegion(AddressInterval interval);
    int split(int fromIndex);

    BuddyZoneNode* allocNode();
    void           deallocNode(BuddyZoneNode* node);
};

}

#endif
