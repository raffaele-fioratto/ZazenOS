// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __STACK_H__
#define __STACK_H__

#include <mm/physical.h>

namespace MM {

class PhysicalStack : public PhysicalImplementer {
public:
    PhysicalStack();
    ~PhysicalStack();

    void* allocatePage();
    void* allocatePages(uint32_t num);

    void deallocatePage(void* addr);
    void deallocatePages(void* addr, uint32_t num);

    void printStatistics();

    bool supportsMultiplePages() { return false; }

    int init(uintptr_t startAddr, uintptr_t regionLength = 0);

private:
    uintptr_t firstFreePageAddr;
    uintptr_t virtMapAddr;
    uintptr_t physStartAddr;
    uintptr_t physRegionLen;

    static int numInitializedInstances;
};

}

#endif
