// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PHYSICAL_H__
#define __PHYSICAL_H__

#include <global.h>
#include <mm/mm.h>

namespace MM {

enum PhysicalPageFlags {
    Free = 0,
    InUse = 1

    // ... (add other useful flags here if necessary)
};

struct PhysicalPage {
    uint32_t    flags;
    uintptr_t   virtAddr;
    void*       reserved;
};

class PhysicalImplementer {
public:
    PhysicalImplementer() { };
    virtual ~PhysicalImplementer() { };

    virtual void* allocatePage() = 0;
    virtual void* allocatePages(uint32_t num) = 0;

    virtual void deallocatePage(void* addr) = 0;
    virtual void deallocatePages(void* addr, uint32_t num) = 0;

    virtual void printStatistics() = 0;

    virtual int init(uintptr_t startAddr, uintptr_t regionLength) = 0;

    virtual bool supportsMultiplePages() = 0;
};

class Physical {
public:
    enum Zone {
        DMA = 0,
        Normal = 1,
        NumZones
    };

    static const uintptr_t PHYSICAL_BORDER_ADDR = 0x01000000;
    static const uintptr_t PHYSPAGES_VIRT_START_ADDR = 0xF0000000;

public:
    Physical();
    ~Physical();

    int initPageStructs(uintptr_t physFreeEndAddr);

    void* allocatePage(Zone zone);
    void* allocatePages(Zone zone, uint32_t num);

    void deallocatePage(Zone zone, void* addr);
    void deallocatePages(Zone zone, void* addr, uint32_t num);

    int  registerImplementer(Zone zone, PhysicalImplementer* impl);
    int  deregisterImplementer(Zone zone, PhysicalImplementer* impl);

    void printStatistics(Zone zone);

    bool supportsMultiplePages(Zone zone);

    void setPhysicalPageFlags(uintptr_t addr, uint32_t flags);
    void setPhysicalPageFlags(void* addr, uint32_t flags);

    void unsetPhysicalPageFlags(uintptr_t addr, uint32_t flags);
    void unsetPhysicalPageFlags(void* addr, uint32_t flags);

    void setPhysicalPageRangeFlags(uintptr_t startAddr, uintptr_t endAddr, uint32_t flags);
    void setPhysicalPageRangeFlags(void* startAddr, void* endAddr, uint32_t flags);

    void unsetPhysicalPageRangeFlags(uintptr_t startAddr, uintptr_t endAddr, uint32_t flags);
    void unsetPhysicalPageRangeFlags(void* startAddr, void* endAddr, uint32_t flags);

    uint32_t getPhysicalPageFlags(uintptr_t addr);
    uint32_t getPhysicalPageFlags(void* addr);

    bool isPhysicalPageFree(uintptr_t addr);
    bool isPhysicalPageFree(void* addr);

    static int init(uintptr_t physFreeEndAddr);

private:
    PhysicalImplementer* physImpls[NumZones];
    PhysicalPage*  physPages;
    uintptr_t physLastFreeAddr;
};

extern MM::Physical phys;

}

#endif
