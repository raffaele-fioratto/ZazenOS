// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ALLOCATOR_H__
#define __ALLOCATOR_H__

#include <map.h>
#include <list.h>

#include <stdint.h>
#include <stdlib.h>


class VirtChunk;
class VirtAllocator {
public:
    VirtAllocator(uintptr_t baseAddr, size_t regionSize);

    void* alloc(size_t size);
    void dealloc(void* addr);

private:
    uintptr_t baseAddr_;
    size_t regionSize_;

    KLibcpp::Map<size_t, KLibcpp::List<VirtChunk*>> freeBins_;
    typedef typename KLibcpp::Map<size_t, KLibcpp::List<VirtChunk*>>::Iterator freeBinsIterator;

    KLibcpp::Map<uintptr_t, VirtChunk*> allocatedChunks_;
    typedef typename KLibcpp::Map<size_t, VirtChunk*>::Iterator allocatedChunksIterator;

    size_t round2(size_t size, bool off);
};


#endif //__ALLOCATOR_H__
