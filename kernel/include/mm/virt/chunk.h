// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CHUNK_H__
#define __CHUNK_H__

#include <stdlib.h>
#include <stdint.h>
#include <list.h>


class VirtChunk {
public:
    enum State {
        Free = 0,
        InUse
    };

    VirtChunk(State state, uintptr_t address, size_t size);
    ~VirtChunk();
    const State& state() const;
    const uintptr_t& address() const;
    const size_t& size() const;

    void setState(const State& state);

    VirtChunk* split();
    void merge(VirtChunk* other);

    bool isBuddyFree() const;
    VirtChunk* buddy() const;

private:
    State state_;
    uintptr_t address_;
    size_t size_;

    KLibcpp::List<VirtChunk*> buddies_;
};

#endif //__CHUNK_H__
