// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __VIRTUAL_H__
#define __VIRTUAL_H__

#include <mm/mm.h>
#include <mm/memrbtree.h>

namespace MM {

struct MappedRegion {
    enum Permissions {
        None    = 0x00,
        Read    = 0x01,
        Write   = 0x02,
        Exec    = 0x04,
        Shared  = 0x08,
        Private = 0x10
    };

    AddressInterval addressInterval;
    uint32_t perms;
    uint32_t offset;
    uint32_t dev;
    uint32_t inode;
    const char* pathName;
};

typedef MemRBNode<AddressInterval, MappedRegion*, AddressIntervalEqual, AddressIntervalLess> MappedRegionNode;

class Virtual {

public:
    Virtual();
    ~Virtual();

    static int init();
};

extern MM::Virtual virt;

}

#endif /* __VIRTUAL_H__ */
