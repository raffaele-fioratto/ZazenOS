// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __KSYMTAB_H__
#define __KSYMTAB_H__

#include <global.h>
#include <sys/elf.h>
#include <sys/multiboot.h>
#include <iterator.h>

class KSymbolTable {
public:
    class SectionHeader {
    public:
        SectionHeader(Elf32_Shdr* header, const char* stringTbl);
        ~SectionHeader();

        Elf32_Word  getSize();
        Elf32_Word  getAddress();
        Elf32_Word  getType();
        const char* getName();

    private:
        Elf32_Shdr* shdr;
        const char* shdrStrTbl;
    };

    class SectionHeaderIterator : KLibcpp::iterator<KLibcpp::input_iterator_tag, SectionHeader> {

    public:
        SectionHeaderIterator(uintptr_t addr, uint32_t count, const char* stringTbl);
        SectionHeaderIterator(const SectionHeaderIterator& other);

        SectionHeaderIterator operator++();
        SectionHeaderIterator operator++(int);
        bool operator==(const SectionHeaderIterator& rhs);
        bool operator!=(const SectionHeaderIterator& rhs);

        SectionHeader operator*();

    private:
        uintptr_t shdrAddr;
        uint32_t  shdrsCount;
        const char* shdrStrTbl;
    };

    class KSymbol {
    public:
        KSymbol() : KSymbol(NULL, NULL) {}
        KSymbol(Elf32_Sym* symbol, const char* stringTbl);
        ~KSymbol();

        const char* getName();
        Elf32_Word  getValue();
        Elf32_Word  getSize();
        uint32_t    getType();
        uint32_t    getBind();
        bool        isValid() { return sym != NULL && symsStrTbl != NULL; }
        uint32_t    getRelPos(uint32_t addr);

    private:
        Elf32_Sym* sym;
        const char* symsStrTbl;
    };

    class KSymbolIterator : KLibcpp::iterator<KLibcpp::input_iterator_tag, KSymbol> {

    public:
        KSymbolIterator(uintptr_t addr, uint32_t count, const char* stringTbl);
        KSymbolIterator(const KSymbolIterator& other);

        KSymbolIterator operator++();
        KSymbolIterator operator++(int);
        bool operator==(const KSymbolIterator& rhs);
        bool operator!=(const KSymbolIterator& rhs);

        KSymbol operator*();

    private:
        uintptr_t symAddr;
        uint32_t  symsCount;
        const char* symsStrTbl;
    };

    KSymbolTable();
    ~KSymbolTable();

    void setInfo(elfSyms* info);
    void markPhysicalPages();

    SectionHeaderIterator getSectionHeaderIterator();
    SectionHeaderIterator getSectionHeaderIteratorEnd();

    KSymbolIterator getSymbolsIterator();
    KSymbolIterator getSymbolsIteratorEnd();

    KSymbol resolve(const char* name);
    KSymbol resolve(uintptr_t addr);

private:
    elfSyms* symsInfo;
    bool isInfoValid;
    size_t tableSize;
    const char* shdrsStrTab;
    const char* symsStrTable;
    Elf32_Sym*  kernelSymsArray;
    uint32_t    kernelNumSymbols;
};

extern KSymbolTable ksymtab;

#endif /* __KSYMTAB_H__ */
