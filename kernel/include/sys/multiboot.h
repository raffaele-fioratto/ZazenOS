// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __MULTIBOOT_H__
#define __MULTIBOOT_H__

#include <global.h>
#include <iterator.h>
#include <ostream.h>

struct elfSyms {
    uint32_t num;
    uint32_t size;
    uint32_t addr;
    uint32_t shndx;
} __packed;

struct aoutSyms {
    uint32_t tabsize;
    uint32_t strsize;
    uint32_t addr;
    uint32_t res;
} __packed;

struct mbootInfo {
    uint32_t    flags;

    uint32_t    mem_lower;
    uint32_t    mem_upper;

    uint32_t    boot_device;

    char*       cmdline;

    uint32_t    mods_count;
    uint32_t    mods_addr;

    union {
        struct aoutSyms aout;
        struct elfSyms elf;
    } __packed syms;

    uint32_t    mmap_length;
    uint32_t    mmap_addr;

    uint32_t    drives_length;
    uint32_t    drives_addr;

    uint32_t    config_table;

    char*       boot_loader_name;

    uint32_t    apm_table;

    uint32_t    vbe_control_info;
    uint32_t    vbe_mode_info;
    uint16_t    vbe_mode;
    uint16_t    vbe_interface_seg;
    uint16_t    vbe_interface_off;
    uint16_t    vbe_interface_len;
} __packed;

struct mbootMod {
            uint32_t    mod_start;
            uint32_t    mod_end;

            char*       cmdline;
            uint32_t    pad;
} __packed;

struct mbootMmapEntry {
    uint32_t size;
    uint64_t addr;
    uint64_t len;
    uint32_t type;
} __packed;

class Multiboot {
public:
    static const uint32_t MAGIC = 0x2BADB002;

    enum InfoFlags {
        Memory      = 0x00000001,
        BootDev     = 0x00000002,
        CmdLine     = 0x00000004,
        Mods        = 0x00000008,
        Aout        = 0x00000010,
        Elf         = 0x00000020,
        MemMap      = 0x00000040,
        DriveInfo   = 0x00000080,
        ConfigTable = 0x00000100,
        BootLdrName = 0x00000200,
        APMTable    = 0x00000400,
        VideoInfo   = 0x00000800
    };

    enum MMapEntryType {
        Available   = 1,
        Reserved    = 2
    };

public:
    class Module {
    public:
        Module(uintptr_t addr);

        uint32_t getStartAddr();

    private:
        mbootMod* module;
    };

    class ModuleIterator : public KLibcpp::iterator<KLibcpp::input_iterator_tag, Module> {

    public:
        ModuleIterator(uintptr_t startAddr, uint32_t modCount);
        ModuleIterator(const ModuleIterator& mit);

        ModuleIterator operator++();
        ModuleIterator operator++(int);
        bool operator==(const ModuleIterator& rhs);
        bool operator!=(const ModuleIterator& rhs);

        Module operator*();

    private:
        uintptr_t addr;
        uint32_t  count;
    };

    class MMapEntry {
    friend class MMapIterator;
    public:
        MMapEntry() { setAddress(0); }
        void setAddress(uintptr_t addr);

        uint32_t getEntrySize() const;
        uint64_t getStartAddr() const;
        uint64_t getLength() const;
        uint64_t getEndAddr() const;
        uint32_t getType() const;
        bool     isAvailable() const;

    private:
        mbootMmapEntry* entry;
    };

    class MMapIterator : public KLibcpp::iterator<KLibcpp::input_iterator_tag, MMapEntry> {

    public:
        MMapIterator(uintptr_t startAddr, uint32_t mapLength);
        MMapIterator(const MMapIterator& mit);

        MMapIterator operator++();
        MMapIterator operator++(int);
        bool operator==(const MMapIterator& rhs);
        bool operator!=(const MMapIterator& rhs);

        const MMapEntry& operator*();

    private:
        uintptr_t addr;
        uint32_t  length;
        MMapEntry currentEntry;
    };

    Multiboot();
    ~Multiboot() {};

    void setInfo(void* mbd);
    elfSyms* getElfSymbols();

    bool hasFlags(uint32_t flags);

    uint32_t getLowerMemory();
    uint32_t getUpperMemory();
    uint32_t getTotalMemory();

    char*    getCommandLine();

    uint32_t getModulesCount();
    ModuleIterator getModulesIterator();
    ModuleIterator getLastModule();

    MMapIterator getMMapIterator();
    MMapIterator getLastMMapEntry();

    void markPhysicalPages();

private:
    bool isInfoValid;
    mbootInfo* info;
};

extern Multiboot mboot;

KLibcpp::OStream& operator <<(KLibcpp::OStream& os, const Multiboot::MMapEntry& ent);

#endif

