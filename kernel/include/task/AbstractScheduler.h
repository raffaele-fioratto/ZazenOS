// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ABSTRACTSCHEDULER_H__
#define __ABSTRACTSCHEDULER_H__

#include <arch/x86/irq.h>
#include <arch/x86/process.h>

namespace Task {

class AbstractScheduler : public X86::AbstractIRQHandler {
public:
    AbstractScheduler(X86::Process* currentProcess);
    virtual ~AbstractScheduler();

    static AbstractScheduler* instance() { return instance_; }

    virtual void addProcess(X86::Process* process) = 0;

    uint32_t getNextPid();

protected:
    virtual X86::Process* next() = 0;

    int handle(X86::ISRHandler::Registers& regs);

    X86::Process* currentProcess_;
    static AbstractScheduler* instance_;

    uint32_t lastPid_;

};

}

#endif //__ABSTRACTSCHEDULER_H__
