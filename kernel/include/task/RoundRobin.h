// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ROUNDROBIN_H__
#define __ROUNDROBIN_H__

#include <task/AbstractScheduler.h>
#include <list.h>

namespace Task {

class RoundRobin : public AbstractScheduler {
public:
    RoundRobin(X86::Process* currentProcess);

    void addProcess(X86::Process* process);

protected:
    X86::Process* next();

private:
    KLibcpp::List<X86::Process*> readyProcesses_;
    KLibcpp::List<X86::Process*> waitProcesses_;
};

}

#endif //__ROUNDROBIN__H__
