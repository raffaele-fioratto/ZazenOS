// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/apic.h>
#include <arch/x86/mp.h>
#include <arch/x86/mmu.h>
#include <arch/x86/assembly.h>
#include <arch/x86/cpu.h>
#include <ostream.h>

namespace X86 {

extern CPU bsp;

const uint32_t MPIOAssignToIOAPICIntDelivery[4] {
    IOAPIC::IntDeliveryMode::Fixed,
    IOAPIC::IntDeliveryMode::NMI,
    IOAPIC::IntDeliveryMode::SMI,
    IOAPIC::IntDeliveryMode::ExtINT
};

IOAPIC::IOAPIC(uint32_t ioApicBaseAddr) {
    if (!MMU::getPhysicalAddress(ioApicBaseAddr)) {
        MMU::mapPage((void*) ioApicBaseAddr, (void*) ioApicBaseAddr, MMU::PteFlags::PTE_READ_WRITE);
    }
    ioRegSel = ioApicBaseAddr;
    ioWin = ioRegSel + 0x10;
}

IOAPIC::~IOAPIC() {

}

void IOAPIC::init() {
    io_apic_id memId = IOAPICRead32(IOAPICID);
    id = memId.bits.id;

    io_apic_ver memVer = IOAPICRead32(IOAPICVER);
    version = memVer.bits.version;
    IRQMaxEntry = memVer.bits.max_redir_entry;

    KLibcpp::kout << "IOAPIC: apic_id " << id << ", version " << version << ", address " << (void*) ioRegSel << ", GSI 0-" << IRQMaxEntry << KLibcpp::endl;

    SMP::MP::TablesIterator resumeIt;
    mp_io_int_assign_entry* io_irq_assign;
    while ((io_irq_assign = SMP::mp.getTable<mp_io_int_assign_entry>(3, resumeIt))) {
        if (io_irq_assign->dest_ioapic_id == id) {
            irqToIntinMappings.insert(io_irq_assign->src_bus_irq, io_irq_assign->dest_ioapic_intin_num);

            // Configure IRQ
            io_red_tbl& irq = redirTable[io_irq_assign->dest_ioapic_intin_num];
            irq.bits.vector = BASE_IRQ_VECTOR + io_irq_assign->src_bus_irq;
            irq.bits.delivery_mode = MPIOAssignToIOAPICIntDelivery[io_irq_assign->int_type];
            irq.bits.dest_mode = IntDestinationMode::Physical;
            irq.bits.polarity = IntPinPolarity::HighActive;
            irq.bits.trigger_mode = IntTriggerMode::EdgeTriggered;
            irq.bits.mask = IntMask::Masked;

            IOAPICWrite64(IOREDTBL + (io_irq_assign->dest_ioapic_intin_num << 1), irq);
        }
    }
}

void IOAPIC::disable() {
    foreach(int intinPin, irqToIntinMappings) {
        io_red_tbl& irq = redirTable[intinPin];
        irq.bits.mask = IntMask::Masked;
        IOAPICWrite64(IOREDTBL + (intinPin << 1), irq);
    }
}

void IOAPIC::ackIRQ(uint32_t number) {
    unmaskIRQ(number);
    bsp.localAPIC().sendEOI();
}

void IOAPIC::maskIRQ(uint32_t number) {
    int& intinPin = *(irqToIntinMappings.find(number));
    io_red_tbl& irq = redirTable[intinPin];
    irq.bits.mask = IntMask::Masked;
    IOAPICWrite64(IOREDTBL + (intinPin << 1), irq);
}

void IOAPIC::unmaskIRQ(uint32_t number) {
    int& intinPin = *(irqToIntinMappings.find(number));
    io_red_tbl& irq = redirTable[intinPin];
    irq.bits.mask = IntMask::Unmasked;
    IOAPICWrite64(IOREDTBL + (intinPin << 1), irq);
}

void IOAPIC::enable() {
    foreach(int intinPin, irqToIntinMappings) {
        io_red_tbl& irq = redirTable[intinPin];
        irq.bits.mask = IntMask::Unmasked;
        IOAPICWrite64(IOREDTBL + (intinPin << 1), irq);
    }
}

void IOAPIC::IOAPICWrite32(uint32_t offset, uint32_t value) {
    *(uint32_t*) (ioRegSel) = offset;
    *(uint32_t*) (ioWin) = value;
}

void IOAPIC::IOAPICWrite64(uint32_t offset, uint64_t value) {
    *((uint32_t*) (ioRegSel)) = offset;
    *((uint32_t*) (ioWin)) = DWORD0(value);
    *((uint32_t*) (ioRegSel)) = offset + 1;
    *((uint32_t*) (ioWin)) = DWORD1(value);
}

uint64_t IOAPIC::IOAPICRead64(uint32_t offset) {
    *(uint32_t*) (ioRegSel) = offset;
    uint32_t low = *((uint32_t *) (ioWin));
    *(uint32_t*) (ioRegSel) = offset + 1;
    uint32_t high = *((uint32_t *) (ioWin));
    return (uint64_t) low | (uint64_t) high << 32;
}

uint32_t IOAPIC::IOAPICRead32(uint32_t offset) {
    *(uint32_t*) (ioRegSel) = offset;
    return *((uint32_t *) (ioWin));
}

APIC::APIC() {

}

APIC::~APIC() {

}

void APIC::init() {
    SMP::MP::TablesIterator resumeIt;

    mp_ioapic_entry* ioapic_entry;
    while ((ioapic_entry = SMP::mp.getTable<mp_ioapic_entry>(2, resumeIt))) {
        if (!ioapic_entry->ioapic_flags.bits.enabled) {
            continue;
        }
        IOAPIC* ioapic = new IOAPIC(ioapic_entry->ioapic_phys_addr);
        ioapic->init();
        ioApicList.insert(ioapic_entry->ioapic_id, ioapic);
    }

    resumeIt = SMP::MP::TablesIterator(0, 0);
    mp_io_int_assign_entry* io_irq_assign;
    while ((io_irq_assign = SMP::mp.getTable<mp_io_int_assign_entry>(3, resumeIt))) {
        irqMappings.insert(io_irq_assign->src_bus_irq, io_irq_assign->dest_ioapic_id);
    }
}

void APIC::disable() {
    foreach(IOAPIC* ioapic, ioApicList) {
        ioapic->disable();
    }
}

void APIC::ackIRQ(uint32_t number) {
    int& apicId = *(irqMappings.find(number));
    IOAPIC* ioApic = *(ioApicList.find(apicId));

    if (ioApic != NULL) {
        ioApic->ackIRQ(number);
    }
}

void APIC::maskIRQ(uint32_t number) {
    int& apicId = *(irqMappings.find(number));
    IOAPIC* ioApic = *(ioApicList.find(apicId));

    if (ioApic != NULL) {
        ioApic->maskIRQ(number);
    }
}

void APIC::unmaskIRQ(uint32_t number) {
    int& apicId = *(irqMappings.find(number));
    IOAPIC* ioApic = *(ioApicList.find(apicId));

    if (ioApic != NULL) {
        ioApic->unmaskIRQ(number);
    }
}

void APIC::enable() {
    foreach(IOAPIC* ioapic, ioApicList) {
        ioapic->enable();
    }
}

}
