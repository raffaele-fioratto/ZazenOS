// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/x86.h>
#include <arch/x86/cpu.h>
#include <arch/x86/smbios.h>
#include <arch/x86/i8254.h>
#include <string.h>
#include <ostream.h>


namespace X86 {

extern I8254 pit;

union EAX1b {
    struct {                       // low order
        uint32_t SteppingID:4;         // 3..0
        uint32_t ModelID:4;            // 7..4
        uint32_t FamilyID:4;           // 11..8
        uint32_t ProcessorType:2;      // 13..12
        uint32_t Reserved2:2;          // 15..14
        uint32_t ExtendedModel:4;      // 19..16
        uint32_t ExtendedFamily:8;     // 27..20
        uint32_t Reserved:4;           // 31..28
    } Intel;                       // high order

    struct {                       // low order
        uint32_t SteppingID:4;         // 3..0
        uint32_t ModelID:4;            // 7..4
        uint32_t FamilyID:4;           // 11..8
        uint32_t Reserved2:4;          // 15..12
        uint32_t ExtendedModel:4;      // 19..16
        uint32_t ExtendedFamily:8;     // 27..20
        uint32_t Reserved:4;           // 31..28
    } AMD;                         // high order
    uint32_t w;
};

union EBX1b {
    struct { // low order
        uint32_t BrandIndex:8;
        uint32_t CLFLUSHLineSize:8;
        uint32_t LogicalProcessors:8;
        uint32_t InitialAPICID:8;
    } bits;  // high order
    uint32_t w;
};

union ECX8e {
    struct {
        uint32_t CoresPerDie:8;         // 7..0
        uint32_t Reserved1:4;           // 11..8
        uint32_t ApicIdCoreIdSize:4;    // 15..12
        uint32_t TSCSize:2;             // 17..16
        uint32_t Reserved2:14;          // 31..18
    } bits;
    uint32_t w;
};

CPU bsp;

CPU::CPU() {

}

CPU::~CPU() {

}

int CPU::detectMyself() {
    CpuidRegs regs;

    X86::cpuid(0x0, &regs);

    cpuidLevel = regs.eax;

    ::memcpy(vendor, &(regs.ebx), 4);
    ::memcpy(vendor + 4, &(regs.edx), 4);
    ::memcpy(vendor + 8, &(regs.ecx), 4);

    if (0 == ::strcmp(vendor, CPUID_VENDOR_INTEL)) {
        type = CPU_INTEL;
    } else if (0 == ::strcmp(vendor, CPUID_VENDOR_AMD)) {
        type = CPU_AMD;
    } else {
        /* CPU not supported here */
        return -1;
    }

    if (cpuidLevel >=  1) {
        X86::cpuid(0x1, &regs);

        EAX1b EAX;
        EBX1b EBX;

        EAX.w = regs.eax;
        EBX.w = regs.ebx;

        stepping = EAX.Intel.SteppingID;
        family = EAX.Intel.FamilyID;
        model = EAX.Intel.ModelID;

        if (regs.edx & CPUID_FEAT_EDX_TSC) {
            startTSC = rdtsc();
        }

        hasFPU = regs.edx & CPUID_FEAT_EDX_FPU;

        if (family == 15) {
            family += EAX.Intel.ExtendedFamily;
        }

        if (family == 6 || family == 15) {
            model += (uint32_t) EAX.Intel.ExtendedModel << 4;
        }

        initialApicId = EBX.bits.InitialAPICID;

        ecxBaseFlags = regs.ecx;
        edxBaseFlags = regs.edx;
    }

#if 0
    if (cpuidLevel >= 2) {
        X86::cpuid(0x2, &regs);

        uint8_t al = regs.eax & 0xFF;
    }
#endif

    cpuid (0x80000000, &regs);

    extCpuidLevel = regs.eax;

    if (extCpuidLevel > 0x80000000) {
        cpuid (0x80000001, &regs);
        ecxExtFlags = regs.ecx;
        edxExtFlags = regs.edx;
    }

    if (extCpuidLevel >= 0x80000004) {
        cpuid (0x80000002, &regs);
        ::memcpy (modelName, &regs, 16);

        cpuid (0x80000003, &regs);
        ::memcpy (modelName + 16, &regs, 16);

        cpuid (0x80000004, &regs);
        ::memcpy (modelName + 32, &regs, 16);
    }

    if (extCpuidLevel >= 0x80000008) {
        cpuid (0x80000008, &regs);
        ECX8e ECX;
        ECX.w = regs.ecx;

        cpuCores = ECX.bits.CoresPerDie + 1;

        uint32_t mask = 1;
        uint32_t numbits = 1;
        if (ECX.bits.ApicIdCoreIdSize) {
            numbits = ECX.bits.ApicIdCoreIdSize;
            mask <<= (numbits - 1);
            mask++;
        }
        processor = (initialApicId & ~mask) >> (numbits);
        coreId = initialApicId & mask;
    } else {
        processor = 1;
        cpuCores = 1;
    }
    speedMHz = 0;

#if 0
    X86::SMBIOS::TablesIterator resumeIt;

    X86::smbios_cpu_info* cpuInfo = X86::smbios.getTable<X86::smbios_cpu_info>(4, resumeIt);

    while (cpuInfo != NULL) {
        EAX1b eax;
        eax.w = cpuInfo->id & 0xFFFFFFFF;
        int smbiosFamily = eax.Intel.FamilyID;
        int smbiosModel = eax.Intel.ModelID;
        int smbiosStepping = eax.Intel.SteppingID;

        if (smbiosFamily == 15) {
            smbiosFamily += eax.Intel.ExtendedFamily;
        }

        if (smbiosFamily == 6 || smbiosFamily == 15) {
            smbiosModel += (uint32_t) eax.Intel.ExtendedModel << 4;
        }

        if (smbiosFamily == family && smbiosModel == model && smbiosStepping == stepping) {
            speedMHz = cpuInfo->curr_speed;
            break;
        }

        cpuInfo = X86::smbios.getTable<X86::smbios_cpu_info>(4, resumeIt);
    }

    if (speedMHz == 0) {
        KLibcpp::kout << "Warning: cannot detect CPU speed through SMBIOS!" << KLibcpp::endl;
    }
#endif

    if (hasEDXFlags(CPUID_FEAT_EDX_SSE, false)) {
        enable_sse();
    }

    if (hasEDXFlags(CPUID_FEAT_EDX_APIC, false)) {
        lapic.init();
        lapic.disable();
    }

    return 0;
}

bool CPU::isFPUPresent() {
    return hasFPU;
}

bool CPU::hasECXFlags(uint32_t flags, bool extended) {
    return (extended ? ecxExtFlags & flags : ecxBaseFlags & flags);
}

bool CPU::hasEDXFlags(uint32_t flags, bool extended) {
    return (extended ? edxExtFlags & flags : edxBaseFlags & flags);
}

int CPU::calibrateTSC() {
    int pitGate = pit.getGate();
    pitGate &= 0xFD;
    pitGate |= 1;

    pit.setGate(pitGate);
    pit.setOperatingMode(I8254::OperatingMode::HWRetriggerOneShot, 1193, I8254::Channel::Channel2);

    pitGate &= 0xFE;
    pit.setGate(pitGate);

    pitGate |= 1;
    pit.setGate(pitGate);
    __asm__ volatile("cpuid" :
                             : "eax" (0)
                             : "eax", "ebx", "ecx", "edx");
    uint64_t tscBefore = rdtsc();

    while (pit.getGate() & 0x20);

    __asm__ volatile("cpuid" :
                             : "eax" (0)
                             : "eax", "ebx", "ecx", "edx");
    uint64_t tscAfter = rdtsc();

    uint32_t ticks = tscAfter - tscBefore;
    speedMHz = ticks / 1000;

    KLibcpp::kout << "CPU speed: " << speedMHz << "MHz" << KLibcpp::endl;

    return 0;
}

}
