#include <arch/x86/fault.h>
#include <global.h>

namespace X86 {

DivisionByZeroFault de;

DivisionByZeroFault::DivisionByZeroFault()
    : AbstractISRHandler(X86Exceptions::DivByZero) {

}

int DivisionByZeroFault::handle(ISRHandler::Registers& regs) {
    K_UNUSED(regs);
    return 0;
}

}
