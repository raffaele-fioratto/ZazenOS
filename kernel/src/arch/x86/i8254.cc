// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <global.h>
#include <arch/x86/i8254.h>
#include <ostream.h>

namespace X86 {

const uint8_t X86::I8254::PIT_CHANNELS_DATA[PIT_NUM_CHANNELS] = { PIT_CHANNEL0_DATA, PIT_CHANNEL1_DATA, PIT_CHANNEL2_DATA };

I8254::I8254() : AbstractIRQHandler(0) {
}

I8254::~I8254() {

}

void I8254::init() {
    cachedStatuses[Channel::Channel0] = readChannelStatus(Channel::Channel0);
    cachedStatuses[Channel::Channel1] = readChannelStatus(Channel::Channel1);
    cachedStatuses[Channel::Channel2] = readChannelStatus(Channel::Channel2);

    ISRHandler::registerIRQHandler(this);
}

void I8254::setOperatingMode(OperatingMode mode, Channel channel) {
    MCR mcr;
    mcr.Bits.operatingMode = mode;
    mcr.Bits.accessMode = Word;
    mcr.Bits.channelSelect = channel;

    outb(PIT_MCR, mcr);

    cachedStatuses[channel].Bits.operatingMode = mode;
    cachedStatuses[channel].Bits.accessMode = Word;
}

void I8254::setOperatingMode(OperatingMode mode, uint16_t reloadValue, Channel channel) {
    setOperatingMode(mode, channel);
    setReloadValue(reloadValue, channel);
}

void I8254::setReloadValue(uint16_t reloadValue, Channel channel) {
    outb(PIT_CHANNELS_DATA[channel], BYTE0(reloadValue));
    outb(PIT_CHANNELS_DATA[channel], BYTE1(reloadValue));
}

I8254::OperatingMode I8254::getOperatingMode(Channel channel) {
    return static_cast<OperatingMode>(cachedStatuses[channel].Bits.operatingMode);
}

int I8254::handle(ISRHandler::Registers& regs) {
    K_UNUSED(regs);
#if 0
    KLibcpp::kout << "Timer tick ";
    KLibcpp::kout.flush();
#endif
    return 0;
}

I8254::ReadBackStatus I8254::readChannelStatus(Channel channel) {
    MCRReadBack mcr;
    ReadBackStatus rbs;
    switch (channel) {
        case Channel0:
            mcr.Bits.readBackTimerChannel0 = 1;
            break;
        case Channel1:
            mcr.Bits.readBackTimerChannel1 = 1;
            break;
        case Channel2:
            mcr.Bits.readBackTimerChannel2 = 1;
            break;
        default:
            return rbs;
    }

    mcr.Bits.latchCountFlag = 1;
    mcr.Bits.readBackAccess = ReadBack;

    outb(PIT_MCR, mcr);
    rbs = inb(PIT_CHANNELS_DATA[channel]);

    return rbs;
}

uint8_t I8254::getGate() {
    return inb(PIT_GATE);
}

void I8254::setGate(uint8_t value) {
    return outb(PIT_GATE, value);
}

}
