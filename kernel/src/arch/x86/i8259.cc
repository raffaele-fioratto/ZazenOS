// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/i8259.h>
#include <arch/x86/inline.h>
#include <ostream.h>

namespace X86 {

I8259::I8259() {
    cachedIRQMask = 0xFFFF;
}

I8259::~I8259() {

}

int I8259::remap(uint8_t offset1, uint8_t offset2) {
    uint8_t a1, a2;

    a1 = inb(PIC1_DATA);
    a2 = inb(PIC2_DATA);

    outb(PIC1_COMMAND, ICW1_INIT + ICW1_ICW4);
    io_wait();
    outb(PIC2_COMMAND, ICW1_INIT + ICW1_ICW4);
    io_wait();

    outb(PIC1_DATA, offset1);
    io_wait();
    outb(PIC2_DATA, offset2);
    io_wait();

    outb(PIC1_DATA, 4);
    io_wait();
    outb(PIC2_DATA, 2);
    io_wait();

    outb(PIC1_DATA, ICW4_8086);
    io_wait();
    outb(PIC2_DATA, ICW4_8086);
    io_wait();

    outb(PIC1_DATA, a1);
    outb(PIC2_DATA, a2);

    masterIRQOffset = offset1;
    slaveIRQOffset = offset2;

    return 0;
}

void I8259::disable() {
    outb(PIC1_DATA, 0xFF);
    outb(PIC2_DATA, 0xFF);

    cachedIRQMask = 0xFFFF;
}

void I8259::ackIRQ(uint32_t number) {
    uint16_t mask = 1 << number;
    if (cachedIRQMask & mask) {
        uint16_t isr = getISR();
        getIRR();
        if (!(isr & mask)) {
            KLibcpp::kout << "Spurious i8259 interrupt: IRQ" << number << "." << KLibcpp::endl;
            if (number >= 8) {
                outb(PIC1_COMMAND, PIC_EOI);
            }
            return;
        }
    }

    cachedIRQMask &= ~mask;

    if (number >= slaveIRQOffset) {
        outb(PIC2_DATA, BYTE1(cachedIRQMask));
        outb(PIC2_COMMAND, PIC_EOI);
        outb(PIC1_COMMAND, PIC_EOI);
    } else {
        outb(PIC1_DATA, BYTE0(cachedIRQMask));
        outb(PIC1_COMMAND, PIC_EOI);
    }
}

void I8259::maskIRQ(uint32_t number) {
    cachedIRQMask |= (1 << number);

    if (number < 8) {
        outb(PIC1_DATA, BYTE0(cachedIRQMask));
    } else {
        outb(PIC1_DATA, BYTE1(cachedIRQMask));
    }
}

void I8259::unmaskIRQ(uint32_t number) {
    cachedIRQMask &= ~(1 << number);

    if (number < 8) {
        outb(PIC1_DATA, BYTE0(cachedIRQMask));
    } else {
        outb(PIC2_DATA, BYTE1(cachedIRQMask));
    }
}

void I8259::enable() {
    outb(PIC1_DATA, 0x00);
    outb(PIC2_DATA, 0x00);

    cachedIRQMask = 0x00;
}

uint16_t I8259::getIRR() {
    return getIRQReg(OCW3_READ_IRR);
}

uint16_t I8259::getISR() {
    return getIRQReg(OCW3_READ_ISR);
}

uint8_t I8259::getMasterIRQOffset() {
    return masterIRQOffset;
}

uint8_t I8259::getSlaveIRQOffset() {
    return slaveIRQOffset;
}

uint16_t I8259::getIRQReg(uint8_t ocw3) {
    outb(PIC1_COMMAND, ocw3);
    outb(PIC2_COMMAND, ocw3);
    io_wait();

    return (inb(PIC2_COMMAND) << 8 | inb(PIC1_COMMAND));
}



}
