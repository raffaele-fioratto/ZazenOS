// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/x86.h>
#include <arch/x86/assembly.h>
#include <arch/x86/isr.h>
#include <arch/x86/irq.h>
#include <arch/x86/cpu.h>
#include <arch/x86/i8259.h>
#include <arch/x86/i8254.h>
#include <arch/x86/apic.h>
#include <arch/x86/mp.h>
#include <ostream.h>
#include <errno.h>

extern "C" void gdtInit();
extern "C" void idtInit();
extern "C" void tssInit();

namespace X86 {

I8259 pic;
I8254 pit;
APIC apic;
AbstractInterruptController* intController;

int init() {
    KLibcpp::kout << "=== x86 Boot ===" << KLibcpp::endl;
    bsp.detectMyself();

    gdtInit();
    idtInit();
    tssInit();

    ISRHandler::init();

    pic.init();
    pic.disable();

    intController = &pic;

    pit.init();
    pit.setOperatingMode(I8254::RateGenerator, 0);

    bsp.calibrateTSC();

    return 0;
}

namespace SMP {

int init() {
    KLibcpp::kout << "=== SMP Boot ===" << KLibcpp::endl;
    if (mp.findFloatPtrStruct() < 0) {
        KLibcpp::kout << "MP Tables not found" << KLibcpp::endl;
        return -ENOENT;
    } else {
        mp.parseMPConfigTable();

        apic.init();
        apic.disable();

        pic.disable();

        intController = &apic;

        KLibcpp::kout << "Using IOAPIC for interrupt routing" << KLibcpp::endl;

        bsp.localAPIC().enable();
        apic.unmaskIRQ(0);
    }
    return 0;
}

}

}
