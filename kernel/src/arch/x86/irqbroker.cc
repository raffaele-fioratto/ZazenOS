// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <arch/x86/irq.h>
#include <arch/x86/assembly.h>
#include <errno.h>
#include <ostream.h>

namespace X86 {

IRQBroker::IRQBroker(uint32_t number)
    : AbstractIRQHandler(number) {

}

IRQBroker::~IRQBroker() {

}

void IRQBroker::registerIRQHandler(AbstractIRQHandler* handler) {
    sharedHandlers.push_front(handler);
}

void IRQBroker::deregisterIRQHandler(AbstractIRQHandler* handler) {
    sharedHandlers.remove(handler);
}

int IRQBroker::handle(ISRHandler::Registers& regs) {
    int ret = IRQReturn::Invalid;

    if (sharedHandlers.empty()) {
        KLibcpp::kout << "Received IRQ " << regs.int_no - BASE_IRQ_VECTOR << " but there are no registered handlers to handle it!" << KLibcpp::endl;
        return -ENOENT;
    }

    KLibcpp::List<AbstractIRQHandler*>::Iterator it = sharedHandlers.begin();
    KLibcpp::List<AbstractIRQHandler*>::Iterator end = sharedHandlers.end();
    for (; it != end && ret != IRQReturn::Handled; it++) {
        ret = (*it)->handle(regs);
    }

    if (ret == IRQReturn::Pass) {
        KLibcpp::kout << "Received IRQ " << regs.int_no - BASE_IRQ_VECTOR << " but no registered handlers handled it!" << KLibcpp::endl;
        return -ENOENT;
    }

    return 0;
}

}
