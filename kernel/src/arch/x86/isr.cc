// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/isr.h>
#include <arch/x86/irq.h>
#include <arch/x86/assembly.h>
#include <arch/x86/fault.h>
#include <errno.h>
#include <string.h>

namespace X86 {

X86::AbstractISRHandler* X86::ISRHandler::handlers[ISR_NUM_HANDLERS];
extern X86::AbstractInterruptController* intController;

extern KernelPanicFault kp;

void X86::ISRHandler::init() {
    ::memset(handlers, 0, sizeof(handlers));
    handlers[X86Exceptions::KernelPanic] = &kp;
}

void X86::ISRHandler::handle(X86::ISRHandler::Registers& regs) {
    regs.int_no &= 0xFF;
    if (handlers[regs.int_no]) {
        handlers[regs.int_no]->handle(regs);
    } else {
        handlers[X86::X86Exceptions::KernelPanic]->handle(regs);
    }
}

void X86::ISRHandler::handleIRQ(Registers& regs) {
    intController->maskIRQ(regs.err_code);
    handle(regs);
    intController->ackIRQ(regs.err_code);
}

int X86::ISRHandler::registerHandler(X86::AbstractISRHandler* handler) {
    uint8_t number = handler->number();
    if (handlers[number] == NULL) {
        handlers[number] = handler;
    } else {
        return -EEXIST;
    }

    return 0;
}

int X86::ISRHandler::deregisterHandler(X86::AbstractISRHandler* handler) {
    uint8_t number = handler->number();
    if (handlers[number] != NULL) {
        handlers[number] = NULL;
    } else {
        return -EINVAL;
    }

    return 0;
}

int X86::ISRHandler::registerIRQHandler(X86::AbstractISRHandler* handler) {
    int ret = registerHandler(handler);
    if (ret == 0) {
        intController->unmaskIRQ(handler->number() - BASE_IRQ_VECTOR);
    }

    return ret;
}

int X86::ISRHandler::deregisterIRQHandler(X86::AbstractISRHandler* handler) {
    int ret = deregisterHandler(handler);
    if (ret == 0) {
        intController->maskIRQ(handler->number() - BASE_IRQ_VECTOR);
    }

    return ret;
}

}
