// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/lapic.h>
#include <arch/x86/assembly.h>
#include <arch/x86/inline.h>
#include <arch/x86/i8254.h>
#include <arch/x86/mmu.h>
#include <string.h>
#include <ostream.h>

extern "C" uintptr_t kapictab;

namespace X86 {

extern I8254 pit;

LAPIC::LAPIC() {
}

LAPIC::~LAPIC() {
}

/*
int LAPIC::handle(ISRHandler::Registers& regs) {
    KLibcpp::kout << "LAPIC Spurious interrupt!" << KLibcpp::endl;
    return 0;
}
*/

void LAPIC::init() {
    uint32_t edx;
    uint32_t eax;
    read_MSR(APIC_BASE_MSR, &eax, &edx);

    if (!(eax & APIC_BASE_MSR_ENABLE)) {
        enableAPICMSR(eax);
    }

    lapicBaseAddr = eax & 0xFFFFF000;

    MMU::mapTable(&kapictab, (void*) lapicBaseAddr, MMU::PdeFlags::PDE_READ_WRITE);
    ::memset(&kapictab, 0, MMU::PAGE_SIZE);
    MMU::mapPageFast((void*) lapicBaseAddr, (void*) lapicBaseAddr, MMU::PteFlags::PTE_READ_WRITE);

    APICWrite(APIC_DFR, 0x0FFFFFFF);
    uint32_t ldr = APICRead(APIC_LDR) & 0x00FFFFFF;
    ldr |= 1;
    APICWrite(APIC_LDR, ldr);
    APICWrite(APIC_LVT_TMR, APIC_DISABLE);
    APICWrite(APIC_LVT_PERF, APIC_NMI);
    APICWrite(APIC_LVT_LINT1, APIC_DISABLE);
    APICWrite(APIC_TASKPRIOR, 0);

    /* Calibrate LAPIC Timer using PIT Channel 2 */
    enable();

    APICWrite(APIC_LVT_TMR, BASE_IRQ_VECTOR);
    APICWrite(APIC_TMRDIV, 0x0B);

    int pitGate = pit.getGate();
    pitGate &= 0xFD;
    pitGate |= 1;

    pit.setGate(pitGate);
    pit.setOperatingMode(I8254::OperatingMode::HWRetriggerOneShot, 1193, I8254::Channel::Channel2);

    pitGate &= 0xFE;
    pit.setGate(pitGate);

    pitGate |= 1;
    pit.setGate(pitGate);
    APICWrite(APIC_TMRINITCNT, 0xFFFFFFFF);

    while (pit.getGate() & 0x20);

    APICWrite(APIC_LVT_TMR, APIC_DISABLE);

    int32_t ticks = -((int32_t) APICRead(APIC_TMRCURRCNT));
    ticks *= 1000;

    KLibcpp::kout << "CPU bus freq: " << ticks / 1000 / 1000 << "MHz" << KLibcpp::endl;

    disable();
}

void LAPIC::disable() const {
    APICWrite(APIC_SPURIOUS, 0xFF);
}

void LAPIC::enable() const {
    APICWrite(APIC_SPURIOUS, 0xFF | APIC_SW_ENABLE);
}

void LAPIC::sendEOI() const {
    APICWrite(APIC_EOI, 0);
}

void LAPIC::enableAPICMSR(uintptr_t baseAddr) {
    uint32_t edx = 0;
    uint32_t eax = (baseAddr & 0xFFFFF100) | APIC_BASE_MSR_ENABLE;

    write_MSR(APIC_BASE_MSR, eax, edx);
}

void LAPIC::APICWrite(uint32_t offset, uint32_t value) const {
    uint32_t* addr = (uint32_t*) (lapicBaseAddr + offset);
    *addr = value;
}

uint32_t LAPIC::APICRead(uint32_t offset) const {
    return *((uint32_t*) (lapicBaseAddr + offset));
}

}
