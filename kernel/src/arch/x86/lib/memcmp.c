#include <string.h>
#include <stdint.h>


int memcmp (const void *m1, const void *m2, size_t n)
{
    unsigned short flags;
    if (n < 4)
    {
little_n:
        n -= (4 - ((uintptr_t) m1 & 3));
		__asm__ volatile("repe cmpsb\n\t"
                         "lahf\n\t"
                        : "=&D" (m1), "=&S" (m2), "=&a" (flags)
                        : "0" (m1), "1" (m2), "c" (n)
                        : "memory");
		if (!(flags & 0x4000))
			return ((flags & 0x0100 ) ? 1 : -1);
	    else
	        return 0;
    }

	if((uintptr_t) m1 & 3)
	{
		n -= (4 - ((intptr_t) m1 & 3));
		__asm__ volatile("repe cmpsb\n\t"
				  "lahf\n\t"
				: "=&D" (m1), "=&S" (m2), "=&a" (flags)
				: "0" (m1), "1" (m2), "c" (4 - ((uintptr_t) m1 & 3))
				: "memory");
		if (!(flags & 0x4000))
			return ((flags & 0x0100 ) ? 1 : -1);
	}

	if (n < 4)
        goto little_n;

	__asm__ volatile("repe cmpsl\n\t"
			  "lahf\n\t"
			: "=&D" (m1), "=&S" (m2), "=&a" (flags)
			: "0" (m1), "1" (m2), "c" (n >> 2)
			: "memory");
	if (!(flags & 0x4000))
		return ((flags & 0x0100) ? 1 : -1);

	n &= 3;
	if(n)
	{
		__asm__ volatile("repe cmpsb\n\t"
				  "lahf\n\t"
				: "=&D" (m1), "=&S" (m2), "=&a" (flags)
				: "0" (m1), "1" (m2), "c" (n)
				: "memory");
		if (!(flags & 0x4000))
			return ((flags & 0x0100) ? 1 : -1);
	}
	return 0;
}


