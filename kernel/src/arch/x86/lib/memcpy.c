#include <string.h>
#include <stdint.h>

void* memcpy_descending( void* to, const void* from, size_t n );

void *memcpy( void *to, const void *from, size_t n )
{
    if ( n < 4 )
    {
little_n:
    __asm__ volatile( "rep movsb"
                    : "=&D" ( to ), "=&S" ( from )
       				: "0" ( to ), "1" ( from ), "c" ( n )
      				: "memory" );
        return to;
    }

	if( ( uintptr_t ) to & 3 )
	{
		n -= ( 4 - ( ( uintptr_t ) to & 3 ) );
		__asm__ volatile( "rep movsb"
				: "=&D" ( to ), "=&S" ( from )
				: "0" ( to ), "1" ( from ), "c" ( 4 - ( ( uintptr_t ) to & 3 ) )
				: "memory" );

	}

	if (n < 4)
        goto little_n;

	__asm__ volatile( "rep movsl"
			: "=&D" ( to ), "=&S" ( from )
			: "0" ( to ), "1" ( from ), "c" ( n >> 2 )
			: "memory" );
	n &= 3;
	if( n )
	{
		__asm__ volatile( "rep movsb"
				: "=&D" ( to ), "=&S" ( from )
				: "0" ( to ), "1" ( from ), "c" ( n )
				: "memory" );
	}
	return to;
}

void* memcpy_descending( void* to, const void* from, size_t n )
{
    __asm__ volatile( "std" );
    if ( n < 4 )
    {
        __asm__ volatile( "rep movsb\n\t"
                          "cld"
                        : "=&D" ( to ), "=&S" ( from )
                        : "0" ( to ), "1" ( from ), "c" ( n )
                        : "memory" );
        return to;
    }

    if( ( uintptr_t ) to & 3 )
	{
		n -= ( ( uintptr_t ) to & 3 );
		__asm__ volatile( "rep movsb"
				: "=&D" ( to ), "=&S" ( from )
				: "0" ( to ), "1" ( from ), "c" ( ( ( uintptr_t ) to & 3 ) )
				: "memory" );

	}
	__asm__ volatile( "rep movsl"
			: "=&D" ( to ), "=&S" ( from )
			: "0" ( to ), "1" ( from ), "c" ( n >> 2 )
			: "memory" );
	n &= 3;
	if( n )
	{
		__asm__ volatile( "rep movsb"
				: "=&D" ( to ), "=&S" ( from )
				: "0" ( to ), "1" ( from ), "c" ( n )
				: "memory" );
	}

	__asm__ volatile( "cld" );
	return to;
}

