#include <string.h>
#include <stdint.h>


void *memset( void *to, int value, size_t n )
{
    if ( n < 4 )
    {
little_n:
        __asm__ volatile( "rep stosb"
				: "=&D" ( to )
				: "0" ( to ), "a" ( value ), "c" ( n )
				: "memory" );
		return to;
    }

	value &= 0xFF;
	if( value )
	{
		__asm__ volatile( "xor %%edx, %%edx\n\t"
				  "movb %1, %%dl\n\t"
				  "movb %1, %%dh\n\t"
				  "mov %%edx, %%eax\n\t"
				  "shl $16, %%edx\n\t"
				  "or %%eax, %%edx"
				: "=d"( value )
				: "a" ( ( unsigned char ) value )
				);
	}


	if( ( uintptr_t ) to & 3 )
	{
		n -= ( 4 - ( ( uintptr_t ) to & 3 ) );
		__asm__ volatile( "rep stosb"
				: "=&D" ( to )
				: "0" ( to ), "a" ( value ), "c" ( 4 - ( ( uintptr_t ) to & 3 ) )
				: "memory" );
	}

	if (n < 4)
        goto little_n;

	__asm__ volatile( "rep stosl"
			: "=&D" ( to )
			: "0" ( to ), "a" ( value ), "c" ( n >> 2 )
			: "memory" );
	n &= 3;

	if( n )
	{
		__asm__ volatile( "rep stosb"
				: "=&D" ( to )
				: "0" ( to ), "a" ( value ), "c" ( n )
				: "memory" );
	}
	return to;
}

void* memset_32 (void* to, unsigned int value, size_t dwords) {
	__asm__ volatile( "rep stosl"
					 : "=&D" ( to )
					 : "0" ( to ), "a" ( value ), "c" ( dwords )
					 : "memory" );
	return to;
}


