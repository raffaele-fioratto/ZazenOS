// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/inline.h>
#include <arch/x86/mmu.h>
#include <ostream.h>
#include <errno.h>
#include <mm/physical.h>

namespace X86 { namespace MMU {

mmuDir* mmuSelfProcDir = (mmuDir*) 0xFFFFF000;
mmuTable* mmuSelfProcBaseAddr = (mmuTable*) 0xFFC00000;

int mapPageFast(void* physAddr, void* virtAddr, uint32_t flags) {
    uint32_t pdIndex = dirIndex(reinterpret_cast<mmuAddr>(virtAddr));
    uint32_t ptIndex = tblIndex(reinterpret_cast<mmuAddr>(virtAddr));

    mmuTable* pageTable = mmuSelfProcBaseAddr + pdIndex;

    if (!pageTable->mmuPages[ptIndex]) {
        pageTable->mmuPages[ptIndex] = (((mmuAddr) physAddr) & PAGE_MASK) | (flags & PTE_MASK) | PTE_PRESENT;
        X86::flush_tlb_single ((mmuAddr) virtAddr);
    } else {
        mmuAddr alreadyPhysAddr = pageTable->mmuPages[ptIndex] & PAGE_MASK;
        KLibcpp::kout << "PTE already present: " << virtAddr << " -> " << (void*) alreadyPhysAddr << " request: " << virtAddr << " -> " << physAddr << KLibcpp::endl;

        return -EEXIST;
    }

    return 0;
}

int mapPage(void* physAddr, void* virtAddr, uint32_t flags) {
    uint32_t pdIndex = dirIndex(reinterpret_cast<mmuAddr>(virtAddr));
    uint32_t ptIndex = tblIndex(reinterpret_cast<mmuAddr>(virtAddr));

    if (!(mmuSelfProcDir->mmuTables[pdIndex] & PDE_PRESENT)) {
        MM::Physical::Zone z = MM::Physical::Normal;
        void* physAddrTbl = MM::phys.allocatePage(MM::Physical::Normal);
        if (physAddrTbl == NULL) {
            physAddrTbl = MM::phys.allocatePage(MM::Physical::DMA);
            z = MM::Physical::DMA;
        }

        if (physAddrTbl == NULL) {
            KLibcpp::kout << "Cannot allocate page for PT: out of physical memory" << KLibcpp::endl;
        }

        // TODO: handle flags accordingly
        if (mapTable(physAddrTbl, virtAddr, PDE_PRESENT | flags) < 0) {
            MM::phys.deallocatePage(z, physAddrTbl);
        }
    }

    mmuTable* pageTable = mmuSelfProcBaseAddr + pdIndex;

    if (!pageTable->mmuPages[ptIndex]) {
        pageTable->mmuPages[ptIndex] = (((mmuAddr) physAddr) & PAGE_MASK) | (flags & PTE_MASK) | PTE_PRESENT;
        X86::flush_tlb_single ((mmuAddr) virtAddr);
    } else {
        mmuAddr alreadyPhysAddr = pageTable->mmuPages[ptIndex] & PAGE_MASK;
        KLibcpp::kout << "PTE already present: " << virtAddr << " -> " << (void*) alreadyPhysAddr << " request: " << virtAddr << " -> " << physAddr << KLibcpp::endl;

        return -EEXIST;
    }

    return 0;
}

int mapPageRange(void* physAddr, void* virtAddr, uint32_t length, uint32_t flags) {
    uintptr_t physAddrPtr = reinterpret_cast<uintptr_t>(physAddr);
    uintptr_t virtAddrPtr = reinterpret_cast<uintptr_t>(virtAddr);

    uint32_t pages = ROUND_UP(length, PAGE_SIZE);

    for (uint32_t i = 0; i < pages; physAddrPtr += PAGE_SIZE, virtAddrPtr += PAGE_SIZE, i++) {
        int ret = mapPage((void*) physAddrPtr, (void*) virtAddrPtr, flags);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}

int mapPageRangeFast(void* startPhysAddr, void* endPhysAddr, void* startVirtAddr, void* endVirtAddr, uint32_t flags) {
    uintptr_t startPhysAddrPtr = reinterpret_cast<uintptr_t>(startPhysAddr);
    uintptr_t endPhysAddrPtr = reinterpret_cast<uintptr_t>(endPhysAddr);

    uintptr_t startVirtAddrPtr = reinterpret_cast<uintptr_t>(startVirtAddr);
    uintptr_t endVirtAddrPtr = reinterpret_cast<uintptr_t>(endVirtAddr);

    if ((startPhysAddrPtr - endPhysAddrPtr) != (startVirtAddrPtr - endVirtAddrPtr)) {
        KLibcpp::kout << "Virtual to physical range size mismatch" << KLibcpp::endl;
        return -EINVAL;
    }

    uintptr_t physAddr, virtAddr;
    for (physAddr = startPhysAddrPtr, virtAddr = startVirtAddrPtr; physAddr < endPhysAddrPtr; physAddr += PAGE_SIZE, virtAddr += PAGE_SIZE) {
        int ret = mapPageFast((void*) physAddr, (void*) virtAddr, flags);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}

int mapPageRangeFast(void* physAddr,  void* virtAddr, uint32_t length, uint32_t flags) {
    uintptr_t physAddrPtr = reinterpret_cast<uintptr_t>(physAddr);
    uintptr_t virtAddrPtr = reinterpret_cast<uintptr_t>(virtAddr);

    uint32_t pages = ROUND_UP(length, PAGE_SIZE);

    for (uint32_t i = 0; i < pages; physAddrPtr += PAGE_SIZE, virtAddrPtr += PAGE_SIZE, i++) {
        int ret = mapPageFast((void*) physAddrPtr, (void*) virtAddrPtr, flags);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}

int mapTable(void* physAddr, void* virtAddr, uint32_t flags) {
    uint32_t pdIndex = dirIndex((mmuAddr) virtAddr);

    if (!mmuSelfProcDir->mmuTables[pdIndex]) {
        mmuSelfProcDir->mmuTables[pdIndex] = (((mmuAddr) physAddr) & TABLE_MASK) | (flags & PDE_MASK) | PDE_PRESENT;
        flush_tlb_single((mmuAddr) mmuSelfProcDir);
    } else {
        mmuAddr alreadyPhysAddr = mmuSelfProcDir->mmuTables[pdIndex] & TABLE_MASK;
        KLibcpp::kout << "PDE already present: " << virtAddr << " -> " <<alreadyPhysAddr << " request: " << virtAddr << " -> " << physAddr << KLibcpp::endl;
        return -EEXIST;
    }

    return 0;
}

int unmapPage(void* virtAddr) {
    uint32_t pdIndex = dirIndex(reinterpret_cast<mmuAddr>(virtAddr));
    uint32_t ptIndex = tblIndex(reinterpret_cast<mmuAddr>(virtAddr));

    if (!(mmuSelfProcDir->mmuTables[pdIndex] & PDE_PRESENT)) {
        KLibcpp::kout << "Request unmapping of unmapped virtual address " << virtAddr << " (PDE not present)" << KLibcpp::endl;
        return -ENOENT;
    }

    mmuTable* pageTable = mmuSelfProcBaseAddr + pdIndex;

    if (!(pageTable->mmuPages[ptIndex] & PTE_PRESENT)) {
        KLibcpp::kout << "Request unmapping of unmapped virtual address " << virtAddr << " (PTE not present)" << KLibcpp::endl;
        return -ENOENT;
    }

    pageTable->mmuPages[ptIndex] = 0;
    X86::flush_tlb_single((mmuAddr) virtAddr);

    return 0;
}

int unmapPageRange(void* startVirtAddr, void* endVirtAddr) {

    uintptr_t startVirtAddrPtr = reinterpret_cast<uintptr_t>(startVirtAddr);
    uintptr_t endVirtAddrPtr = reinterpret_cast<uintptr_t>(endVirtAddr);


    uintptr_t virtAddr;
    for (virtAddr = startVirtAddrPtr; virtAddr < endVirtAddrPtr; virtAddr += PAGE_SIZE) {
        int ret = unmapPage((void*) virtAddr);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}

int unmapPageRange(void* virtAddr, uint32_t length) {
    uintptr_t virtAddrPtr = reinterpret_cast<uintptr_t>(virtAddr);

    uint32_t pages = ROUND_UP(length, PAGE_SIZE);

    for (uint32_t i = 0; i < pages; virtAddrPtr += PAGE_SIZE, i++) {
        int ret = unmapPage((void*) virtAddrPtr);
        if (ret < 0) {
            return ret;
        }
    }

    return 0;
}

mmuAddr getPhysicalAddress(mmuAddr virtAddr) {
    uint32_t pdIndex = dirIndex(virtAddr);
    uint32_t ptIndex = tblIndex(virtAddr);

    if (!(mmuSelfProcDir->mmuTables[pdIndex] & PDE_PRESENT)) {
#ifdef DEBUG
        KLibcpp::kout << "Requested physical address of unmapped virtual address " << (void*) virtAddr << " (PDE not present)" << KLibcpp::endl;
#endif
        return 0;
    }

    mmuTable* pageTable = mmuSelfProcBaseAddr + pdIndex;

    if (!(pageTable->mmuPages[ptIndex] & PTE_PRESENT)) {
#ifdef DEBUG
        KLibcpp::kout << "Requested physical address of unmapped virtual address " << (void*) virtAddr << " (PTE not present)" << KLibcpp::endl;
#endif
        return 0;
    }

    return (pageTable->mmuPages[ptIndex] & PAGE_MASK) | (reinterpret_cast<mmuAddr>(virtAddr) & PTE_MASK);
}

void* getPhysicalAddress(void* virtAddr) {
    return reinterpret_cast<void*>(getPhysicalAddress(reinterpret_cast<void*>(virtAddr)));
}

} }
