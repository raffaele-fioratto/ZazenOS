// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/mp.h>
#include <string.h>
#include <errno.h>
#include <ostream.h>

namespace X86 { namespace SMP {

MP mp;

const size_t MP::MP_CONF_TABLE_ENTRY_SIZE[MP_CONF_TABLE_ENTRIES_NUM] = {
    sizeof(mp_processor_entry),
    sizeof(mp_bus_entry),
    sizeof(mp_ioapic_entry),
    sizeof(mp_io_int_assign_entry),
    sizeof(mp_io_int_assign_entry)
};
MM::AddressInterval MP::EBDA(KERNEL_BASE_VIRT_ADDR + 0x0009FC00, 0x400);
MM::AddressInterval MP::BIOS_ROM(KERNEL_BASE_VIRT_ADDR + 0xF0000, 0xFFFF);

MP::TablesIterator::TablesIterator(uintptr_t startAddr, uint32_t tablesCount) {
    addr = startAddr;
    count = tablesCount;
}

MP::TablesIterator::TablesIterator(const TablesIterator& mit) {
    addr = mit.addr;
    count = mit.count;
}

MP::TablesIterator& MP::TablesIterator::operator++() {
    mp_conf_table_entry *thisEntry = operator*();
    addr += MP::MP_CONF_TABLE_ENTRY_SIZE[thisEntry->type];
    return *this;
}

MP::TablesIterator MP::TablesIterator::operator++(int) {
    TablesIterator it(*this);
    operator++();

    return it;
}

bool MP::TablesIterator::operator==(const TablesIterator& rhs) {
    return addr == rhs.addr && count == rhs.count;
}

bool MP::TablesIterator::operator!=(const TablesIterator& rhs) {
    return addr != rhs.addr || count != rhs.count;
}

mp_conf_table_entry* MP::TablesIterator::operator*() {
    return reinterpret_cast<mp_conf_table_entry*>(addr);
}

MP::MP() { }
MP::~MP() { }

int MP::findFloatPtrStruct() {
    MM::AddressInterval::Iterator<uint8_t> it = EBDA.begin<uint8_t>();
    MM::AddressInterval::Iterator<uint8_t> end = EBDA.end<uint8_t>();

    for(; it != end; it += MP_SIGNATURE_STRING_ALIGN) {
        if (!::memcmp(&it, MP_SIGNATURE_STRING, MP_SIGNATURE_STRING_LEN) && isFloatPtrStructChecksumValid(&it)) {
            mpFloatPtrStruct = reinterpret_cast<mp_float_ptr_struct*>(&it);
            mpConfigTableHeader = reinterpret_cast<mp_config_table_header*>(mpFloatPtrStruct->phys_addr_ptr + KERNEL_BASE_VIRT_ADDR);
            KLibcpp::kout << "MP Floating Pointer Structure found at address: " << mpFloatPtrStruct << KLibcpp::endl;
            KLibcpp::kout << "MP Configuration table address: " << (void*) mpFloatPtrStruct->phys_addr_ptr << KLibcpp::endl;
            isValid = true;
            return 0;
        }
    }

    it = BIOS_ROM.begin<uint8_t>();
    end = BIOS_ROM.end<uint8_t>();
    for(; it != end; it += MP_SIGNATURE_STRING_ALIGN) {
        if (!::memcmp(&it, MP_SIGNATURE_STRING, MP_SIGNATURE_STRING_LEN) && isFloatPtrStructChecksumValid(&it)) {
            mpFloatPtrStruct = reinterpret_cast<mp_float_ptr_struct*>(&it);
            mpConfigTableHeader = reinterpret_cast<mp_config_table_header*>(mpFloatPtrStruct->phys_addr_ptr + KERNEL_BASE_VIRT_ADDR);
            KLibcpp::kout << "MP Floating Pointer Structure found at address: " << mpFloatPtrStruct << KLibcpp::endl;
            KLibcpp::kout << "MP Configuration table address: " << (void*) mpFloatPtrStruct->phys_addr_ptr << KLibcpp::endl;
            isValid = true;
            return 0;
        }
    }

    return -ENOENT;
}

MP::TablesIterator MP::getTablesIterator() {
    if (isValid) {
        return TablesIterator(reinterpret_cast<uintptr_t>(mpConfigTableHeader + 1), mpConfigTableHeader->entry_count);
    }

    return TablesIterator(0, 0);
}

MP::TablesIterator MP::getLastTable() {
    if (isValid) {
        return TablesIterator(reinterpret_cast<uintptr_t>(mpConfigTableHeader) + mpConfigTableHeader->base_table_len, mpConfigTableHeader->entry_count);
    }

    return TablesIterator(0, 0);
}

int MP::parseMPConfigTable() {
    if (!isValid) {
        return -ENOENT;
    }

    if (::memcmp(&mpConfigTableHeader->signature, MP_CONF_HDR_SIGNATURE_STRING, MP_CONF_HDR_SIGNATURE_STRING_LEN) || !isMPConfTableHdrChecksumValid()) {
        isValid = false;
        KLibcpp::kout << "Bogus MP Config Table Header found" << KLibcpp::endl;
        return -ENOENT;
    }

    return 0;
}

bool MP::isFloatPtrStructChecksumValid(uint8_t* addr) {
    uint8_t checksum = 0;
    size_t i;
    for (i = 0; i < sizeof(mp_float_ptr_struct); i++) checksum += addr[i];

    return checksum == 0;
}

bool MP::isMPConfTableHdrChecksumValid() {
    uint8_t checksum = 0;
    size_t i;
    uint8_t* addr = reinterpret_cast<uint8_t*>(mpConfigTableHeader);
    for (i = 0; i < mpConfigTableHeader->base_table_len; i++) checksum += addr[i];

    return checksum == 0;
}

} }
