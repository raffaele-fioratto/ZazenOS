// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/fault.h>
#include <ostream.h>
#include <sys/ksymtab.h>

#include <distorm.h>

using namespace KLibcpp;

namespace X86 {

KernelPanicFault kp;

const char* KernelPanicFault::EXC_MNEMONICS[] = {
    "#DE  (Division by Zero)",           // 0
    "#DB  (Debug)",                      // 1
    "#NMI (Non-Maskable Interrupt)",     // 2
    "#BP  (Breakpoint)",                 // 3
    "#OF  (Overflow)",                   // 4
    "#BR  (Bound Range Exceeded)",       // 5
    "#UD  (Undefined Opcode)",           // 6
    "#NM  (Device Not Available)",       // 7
    "#DF  (Double Fault)",               // 8
    "#KP  (Kernel Panic)",               // 9
    "#TS  (Invalid TSS)",                // 10
    "#NP  (Segment Not Present)",        // 11
    "#SS  (Stack Segment Fault)",        // 12
    "#GP  (General Protection Fault)",   // 13
    "#PF  (Page Fault)",                 // 14
    "#UNK (Reserved Exception Number)",  // 15
    "#MF  (x87 Float Exception)",        // 16
    "#AC  (Alignment Check)",            // 17
    "#MC  (Machine Check)",              // 18
    "#XM  (SIMD Float Exception)",       // 19
    "#VE  (Virtualization Exception)",   // 20
    "#UNK (Reserved Exception Number)",  // 21
    "#UNK (Reserved Exception Number)",  // 22
    "#UNK (Reserved Exception Number)",  // 23
    "#UNK (Reserved Exception Number)",  // 24
    "#UNK (Reserved Exception Number)",  // 25
    "#UNK (Reserved Exception Number)",  // 26
    "#UNK (Reserved Exception Number)",  // 27
    "#UNK (Reserved Exception Number)",  // 28
    "#UNK (Reserved Exception Number)",  // 29
    "#SX  (Security Exception)",         // 30
    "#UNK (Reserved Exception Number)",  // 31
};

KernelPanicFault::KernelPanicFault()
    : AbstractISRHandler(X86Exceptions::KernelPanic) {

}

int KernelPanicFault::handle(ISRHandler::Registers& regs) {
    KSymbolTable::KSymbol symbol;
    uint32_t eip, ebp;

    kout << hex << uppercase;
    kout.setFill('0');
    kout << "Fault:  " << EXC_MNEMONICS[regs.int_no] << endl;

    kout.setWidth(4);

    kout << "EIP:    " << regs.cs;

    kout.setWidth(8);

    kout << ":[" << regs.eip << "]" << endl;
    kout << "EFLAGS: " << regs.eflags << endl;

    symbol = ksymtab.resolve(regs.eip);

    if (symbol.isValid()) {
        unsigned int decodedInstructionsCount = 0;
        _DecodedInst decodedInstructions[1];
        _DecodeResult res = distorm_decode(symbol.getRelPos(regs.eip), (unsigned char*) regs.eip, (int) symbol.getSize(), Decode32Bits, decodedInstructions, 1, &decodedInstructionsCount);

        kout.setWidth(0);
        kout << "EIP is at " << symbol.getName() << "+" << symbol.getRelPos(regs.eip) << "/" << symbol.getSize();
        kout.setWidth(8);

        if (res != DECRES_INPUTERR) {
            kout << ":\t" << (char*) decodedInstructions[0].mnemonic.p
                            << (decodedInstructions[0].operands.length != 0 ? " " : "")
                            << (char*) decodedInstructions[0].operands.p << endl;
        }


    } else {
        kout << "EIP is at ??? (symbols not loaded)" << endl;
    }

    kout << "eax: " << regs.eax << "\tebx: " << regs.ebx << "\tecx: " << regs.ecx << "\tedx: " << regs.edx << endl;
    kout << "esi: " << regs.esi << "\tedi: " << regs.edi << "\tebp: " << regs.ebp << "\tesp: " << regs.esp << endl;

    kout.setWidth(4);

    kout << "ds:  " << regs.ds << endl;

    kout.setWidth(8);

    kout << "Call Trace:" << endl;

    ebp = regs.ebp;
    while (ebp)
    {
        __asm__ volatile("movl 4(%0), %%eax" : "=a" (eip) : "0" (ebp));
        eip -= 5;

        symbol = ksymtab.resolve(eip);

        kout << "[" << eip << "] ";
        if (symbol.isValid()) {
            kout.setWidth(0);
            kout << symbol.getName() << "+" << symbol.getRelPos(eip) << "/" << symbol.getSize()  << endl;
            kout.setWidth(8);
        } else {
            kout << "???" << endl;
        }

        __asm__ volatile("movl (%0), %%eax" : "=a" (ebp) : "0" (ebp));
    }

    __asm__ volatile("cli; hlt;");

    return 0;
}

}
