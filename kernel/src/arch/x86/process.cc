// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/process.h>
#include <task/AbstractScheduler.h>

namespace X86 {

X86::Process* Process::duplicate() {
    X86::Process* proc = new Process();

    proc->generalRegs = this->generalRegs;
    proc->state = X86::Process::Ready;
    proc->pid = Task::AbstractScheduler::instance()->getNextPid();

    return proc;
}

}