// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <arch/x86/smbios.h>
#include <errno.h>
#include <ostream.h>

namespace X86 {

SMBIOS smbios;

SMBIOS::TablesIterator::TablesIterator(uintptr_t startAddr, uint32_t tablesCount) {
    addr = startAddr;
    count = tablesCount;
}

SMBIOS::TablesIterator::TablesIterator(const SMBIOS::TablesIterator& mit) {
    addr = mit.addr;
    count = mit.count;
}

SMBIOS::TablesIterator SMBIOS::TablesIterator::operator++() {
    smbios_hdr* hdr = reinterpret_cast<smbios_hdr*>(addr);
    addr += hdr->length;

    while (*((uint16_t*) addr)) addr++;
    addr += 2;

    return *this;
}

SMBIOS::TablesIterator SMBIOS::TablesIterator::operator++(int) {
    TablesIterator mit(*this);

    operator++();

    return mit;
}

bool SMBIOS::TablesIterator::operator==(const SMBIOS::TablesIterator& rhs) {
    return addr == rhs.addr && count == rhs.count;
}

bool SMBIOS::TablesIterator::operator!=(const SMBIOS::TablesIterator& rhs) {
    return addr != rhs.addr || count != rhs.count;
}

smbios_hdr* SMBIOS::TablesIterator::operator*() {
    smbios_hdr* tbl = reinterpret_cast<smbios_hdr*>(addr);
    return tbl;
}

SMBIOS::SMBIOS() {
    smbiosEPS = NULL;
    isValid = false;
}

SMBIOS::~SMBIOS() {

}

int SMBIOS::findEntryPoint() {
    uint8_t* addr;
    for (addr = (uint8_t*) SMBIOS_START_ADDR; (uintptr_t) addr < SMBIOS_END_ADDR; addr += SMBIOS_ANCHOR_STRING_ALIGN) {
        if (!::memcmp(addr, SMBIOS_ANCHOR_STRING, SMBIOS_ANCHOR_STRING_LEN) && isChecksumValid(addr)) {
            smbiosEPS = (smbios_entry_point*) addr;
            KLibcpp::kout << "SMBIOS v" << smbiosEPS->major << "." << smbiosEPS->minor << " present at " << smbiosEPS << KLibcpp::endl;
            isValid = true;
            return 0;
        }
    }
    return -ENOENT;
}

SMBIOS::TablesIterator SMBIOS::getTablesIterator() {
    if (isValid) {
        return TablesIterator(KERNEL_BASE_VIRT_ADDR + smbiosEPS->table_address, smbiosEPS->num_structs);
    }

    return TablesIterator(0, 0);
}

SMBIOS::TablesIterator SMBIOS::getLastTable() {
    if (isValid) {
        return TablesIterator(KERNEL_BASE_VIRT_ADDR + smbiosEPS->table_address + smbiosEPS->table_length, smbiosEPS->num_structs);
    }

    return TablesIterator(0, 0);
}

bool SMBIOS::isChecksumValid(uint8_t* addr) {
    uint8_t checksum = 0;
    size_t i;
    for (i = 0; i < sizeof(smbios_entry_point); i++) checksum += addr[i];

    return checksum == 0;
}

}
