#include <dev/chardev.h>
#include <errno.h>

namespace Dev {

CharDevice::CharDevice(const char* name)
    : Device(name) {

}

CharDevice::~CharDevice() {

}

int CharDevice::write (void* buffer, size_t bytes, off_t *offset) {
    K_UNUSED(buffer);
    K_UNUSED(bytes);
    K_UNUSED(offset);
    return -EINVAL;
}

int CharDevice::read (void* buffer, size_t bytes, off_t *offset) {
    K_UNUSED(buffer);
    K_UNUSED(bytes);
    K_UNUSED(offset);
    return -EINVAL;
}

int CharDevice::flush() {
    return -EINVAL;
}

}
