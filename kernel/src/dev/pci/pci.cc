// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <dev/pci/pci.h>
#include <arch/x86/inline.h>

namespace Dev { namespace PCI {

uint16_t configReadWord(uint8_t bus, uint8_t slot, uint8_t function, uint8_t offset) {
    uint32_t address;
    uint32_t lbus       = bus;
    uint32_t lslot      = slot;
    uint32_t lfunction  = function;
    uint16_t tmp = 0;

    address = ((lbus << 16) | (lslot << 11) |
              (lfunction << 8) | (offset & 0xfc) | 0x80000000);

    X86::outl(CONFIG_ADDRESS, address);

    tmp = (uint16_t)((X86::inl(CONFIG_DATA) >> ((offset & 2) * 8)) & 0xFFFF);
    return tmp;
}

uint32_t configReadDword(uint8_t bus, uint8_t slot, uint8_t function, uint8_t offset) {
    uint32_t address;
    uint32_t lbus       = bus;
    uint32_t lslot      = slot;
    uint32_t lfunction  = function;

    address = ((lbus << 16) | (lslot << 11) |
              (lfunction << 8) | (offset & 0xfc) | 0x80000000);

    X86::outl(CONFIG_ADDRESS, address);

    return X86::inl(CONFIG_DATA);
}

} }
