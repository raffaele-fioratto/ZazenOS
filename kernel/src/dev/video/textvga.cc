// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <dev/video/textvga.h>
#include <arch/x86/inline.h>
#include <errno.h>
#include <string.h>

namespace Dev { namespace Video {

uint16_t TextVGA::earlyBuffer[TextVGA::TEXT_VGA_EARLY_ROWS * TextVGA::TEXT_VGA_EARLY_COLS];

void TextVGA::Framebuffer::clear() {
    size_t i;

    for (i = 0; i < fbBufSize / sizeof(uint16_t); i++) {
        fbBuffer[i] = ' ' | fbAttribute << 8;
    }
}

void TextVGA::Framebuffer::scroll() {
    int linesToScroll;

    if (isAtEof()) {
        linesToScroll = fbY - fbRows + 1;
    } else {
        linesToScroll = 1;
    }

    ::memcpy(fbBuffer, fbBuffer + linesToScroll * fbColumns, fbBufSize - (linesToScroll * fbColumns * sizeof(uint16_t)));

    uint16_t* bufEndPtr = fbBuffer + (fbBufSize / sizeof(uint16_t));
    uint16_t* bufPtr = bufEndPtr - (linesToScroll * fbColumns);
    for(; bufPtr < bufEndPtr; bufPtr++) {
        *bufPtr = ' ' | fbAttribute << 8;
    }

    fbY -= linesToScroll;
}

void TextVGA::Framebuffer::updateCursor() {
    uint32_t pos = fbY * fbColumns + fbX;

    X86::outb(0x3D4, 0x0F);
    X86::outb(0x3D5, BYTE0(pos));

    X86::outb(0x3D4, 0x0E);
    X86::outb(0x3D5, BYTE1(pos));
}

TextVGA::TextVGA()
    : CharDevice("VGA Text Console") {

    size_t earlyBufSize = TextVGA::TEXT_VGA_EARLY_ROWS * TextVGA::TEXT_VGA_EARLY_COLS * sizeof(uint16_t);

    activeFb = 0;

    framebuffers[activeFb].fbBuffer = earlyBuffer;
    framebuffers[activeFb].fbBufSize = earlyBufSize;
    framebuffers[activeFb].fbX = 0;
    framebuffers[activeFb].fbY = 0;
    framebuffers[activeFb].fbRows = TEXT_VGA_EARLY_ROWS;
    framebuffers[activeFb].fbColumns = TEXT_VGA_EARLY_COLS;
    framebuffers[activeFb].fbAttribute = CGAColorPalette::Black << 4 | CGAColorPalette::White;

    framebuffers[activeFb].clear();
}

TextVGA::~TextVGA() {

}

int TextVGA::write (void* buffer, size_t bytes, off_t *offset) {
    K_UNUSED(offset);
    char* buf = static_cast<char*>(buffer);
    int ret = bytes > 1 ? puts(buf, bytes) : putch(*buf);
    return ret;
}

int TextVGA::flush() {
    ::memcpy((uint16_t*) TEXT_VGA_BASE_ADDR, framebuffers[activeFb].fbBuffer, framebuffers[activeFb].fbBufSize);

    framebuffers[activeFb].updateCursor();

    return 0;
}

int TextVGA::puts(char* str, size_t bytes) {
    size_t i = 0;
    while (i < bytes) {
        putch(str[i]);
        i++;
    }
    return bytes;
}

int TextVGA::putch(int ch) {
    if ( ch == 0x08 && framebuffers[activeFb].fbX > 0) {
        framebuffers[activeFb].backspace();
    }
    else if ( ch == 0x09 ) {
        framebuffers[activeFb].tab();
    }
    else if ( ch == 0x0D ) {
        framebuffers[activeFb].carriageReturn();
    }
    else if ( ch == 0x0A ) {
        framebuffers[activeFb].newline();
    }
    else if ( ch >= ' ' ) {
        framebuffers[activeFb].write(ch);
    }

    if ( framebuffers[activeFb].isAtEol() ) {
        framebuffers[activeFb].newline();
    }

    if ( framebuffers[activeFb].isAtEof() ) {
        framebuffers[activeFb].scroll();
    }

    return 1;
}

}}
