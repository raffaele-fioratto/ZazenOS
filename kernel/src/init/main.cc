// Copyright (C) 2016 Fioratto Raffaele
// ZazenOS C main function

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"
#include <global.h>
#include <dev/video/textvga.h>
#include <ostream.h>
#include <sys/multiboot.h>
#include <sys/ksymtab.h>
#include <arch/x86/x86.h>
#include <arch/x86/smbios.h>
#include <arch/x86/mmu.h>
#include <mm/virtual.h>
#include <mm/physical.h>
#include <mm/memrbtree.h>
#include <arch/x86/process.h>
#include <task/RoundRobin.h>
#include <mm/memlist.h>
#include <list.h>
#include <mm/heap/slab.h>
#include <arch/x86/irq.h>
#include <map.h>


extern "C" int kmain (void* mbd, uint32_t magic);

using namespace KLibcpp;

Dev::Video::TextVGA video;
X86::Process kernelProcess;

/*class IRQ0 : public X86::AbstractIRQHandler {
public:
    IRQ0() : X86::AbstractIRQHandler(0) {  }

    int handle(X86::ISRHandler::Registers& regs) { K_UNUSED(regs); kout << "Timer tick "; kout.flush(); return 0; }
};*/

class IRQ1 : public X86::AbstractIRQHandler {
public:
    IRQ1() : X86::AbstractIRQHandler(1) { }

protected:
    int handle(X86::ISRHandler::Registers& regs) {
        K_UNUSED(regs);
        kout << "Keyboard: " << X86::inb(0x60) << ", ";
        kout.flush();
        return 0;
    }
};


//IRQ0 timer;
IRQ1 keyb;
Task::RoundRobin* rr_sched;

extern "C" int kmain (void* mbd, uint32_t magic) {
    kout.registerOutputBuffer(&video);

    if (magic != Multiboot::MAGIC) {
        kout << SOURCE_POS << "Invalid Multiboot magic number! Bogus bootloader?" << endl;
        return -1;
    }

    mboot.setInfo(mbd);
    ksymtab.setInfo(mboot.getElfSymbols());

    if (mboot.getTotalMemory() * 1024 < MIN_RAM_SIZE) {
        kout << "ZazenOS requires at least " << MIN_RAM_SIZE / (1024 * 1024)
             << "MB of RAM to run properly! Check your system" << endl;
        return -2;
    }

    kout << ZAZENOS_UNAME_STRING << endl;
    kout << "Kernel cmdline = \"" << mboot.getCommandLine() << "\"" << endl;

    X86::smbios.findEntryPoint();

    if (X86::init() < 0) {
        kout << "Unsupported BSP detected! Halting." << endl;
        return -3;
    }

    kernelProcess.pid = 1;
    kernelProcess.state = X86::Process::Exec;
    kernelProcess.cr3 = X86::get_cr3();

    //X86::ISRHandler::registerIRQHandler(&timer);

    X86::SMBIOS::TablesIterator resumeIt;

    X86::smbios_bios_info* biosInfo = X86::smbios.getTable<X86::smbios_bios_info>(0, resumeIt);
    kout << "BIOS: " << X86::smbios.readStringFromTable(biosInfo, biosInfo->vendor_str_idx) << " "
         << X86::smbios.readStringFromTable(biosInfo, biosInfo->version_str_idx) << " "
         << X86::smbios.readStringFromTable(biosInfo, biosInfo->rel_date_str_idx) << endl;

    kout << "Memory Map provided by bootloader:" << endl;
    Multiboot::MMapIterator mmapIt = mboot.getMMapIterator();
    Multiboot::MMapIterator mmapEnd = mboot.getLastMMapEntry();
    uintptr_t highestFreeAddr = 0;
    for (; mmapIt != mmapEnd; mmapIt++) {
        if ((*mmapIt).isAvailable() && (*mmapIt).getEndAddr() > highestFreeAddr) {
            highestFreeAddr = (*mmapIt).getEndAddr();
        }
        kout << *mmapIt << endl;
    }

    kout << "Highest free addr: " << (void*) highestFreeAddr << endl;

    MM::Physical::init(highestFreeAddr);
    MM::Virtual::init();
    MM::SlabManager::init();

    X86::SMP::init();

    X86::ISRHandler::registerIRQHandler(&keyb);

    rr_sched = new Task::RoundRobin(&kernelProcess);

    X86::ISRHandler::registerIRQHandler(rr_sched);

    X86::sti();

    return 0;
}
