#include <global.h>
#include <mm/heap/slab.h>

extern "C" void __cxa_pure_virtual();
extern "C" int __cxa_atexit(void (*destructor) (void *), void *arg, void *__dso_handle);

extern "C" void __cxa_pure_virtual() {

}

extern "C" int __cxa_atexit(void (*destructor) (void *), void *arg, void *__dso_handle) {
    K_UNUSED(destructor);
    K_UNUSED(arg);
    K_UNUSED(__dso_handle);
    return 0;
}

void* operator new(size_t size) {
    return MM::kmalloc(size);
}

void* operator new[](size_t size) {
    return MM::kmalloc(size);
}

void operator delete(void *p) {
    K_UNUSED(p);
}

void operator delete[](void *p) {
    K_UNUSED(p);
}
