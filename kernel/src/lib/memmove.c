#include <string.h>


void* memcpy_descending( void* to, const void* from, size_t n );

void* memmove( void* to, const void* from, size_t n )
{
    char* to_8 = (char*) (to);
    const char* from_8 = (const char*) (from);
    if ( from_8 < to_8 && to_8 < from_8 + n )
    {
        to_8 += n;
        from_8 += n;
        memcpy_descending( to_8, from_8, n );
    } else
        memcpy( to, from, n );
    return to;
}

