// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <ostream.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <mm/heap/slab.h>

namespace KLibcpp {

OStream kout;

StringBuffer::StringBuffer(size_t size) {
    maxSize = size;
    index = 0;
    stringBuffer = reinterpret_cast<char*>(MM::kmalloc(size));
}

StringBuffer::~StringBuffer() {
    MM::kfree(stringBuffer, maxSize);
}

int StringBuffer::putn(void* buffer, size_t size) {
    if (index == maxSize) {
        return 0;
    }

    if (size + index < maxSize) {
        ::memcpy(stringBuffer + index, buffer, size);
        index += size;
        return size;
    } else if (size + index == maxSize) {
        ::memcpy(stringBuffer + index, buffer, size - 1);
        stringBuffer[maxSize - 1] = '\0';
        index += size;
        return size;
    } else {
        int bytes = maxSize - index - 1;
        ::memcpy(stringBuffer+ index, buffer, bytes);
        stringBuffer[maxSize - 1] = '\0';
        index = maxSize;
        return bytes;
    }
}

const char* StringBuffer::toString() const {
    return const_cast<char*>(stringBuffer);
}

OStream::OStream() {
    numRegisteredOutputBuffers = 0;

    curOpts.numBase = 10;
    curOpts.fmtFlags = FmtFlags::dec;
    curOpts.fillWidth = 0;
    curOpts.fillChar = ' ';

    optStackTop = 0;
    ::memset(optsStack, 0, sizeof(Options) * OSTREAM_MAX_SAVED_OPTIONS);
}

OStream::~OStream() {

}

int OStream::saveOptions() {
    if (optStackTop < OSTREAM_MAX_SAVED_OPTIONS) {
        optsStack[optStackTop++] = curOpts;
        return 0;
    }

    return -ENOSPC;
}

int OStream::restoreOptions() {
    if (optStackTop > 0) {
        curOpts = optsStack[--optStackTop];
        return 0;
    }

    return -ENOENT;
}

OStream& OStream::operator <<(bool b) {
    if (curOpts.fmtFlags & FmtFlags::boolalpha) {
        write(b ? "true" : "false");
    } else {
        write(b ? "1" : "0");
    }
    return *this;
}

OStream& OStream::operator <<(const char* str) {
    write(str);
    return *this;
}

OStream& OStream::operator <<(char* buf) {
    write(buf);
    return *this;
}

OStream& OStream::operator <<(long long val) {
    char buf[22];
    size_t bytes = itoa<long long>(val, buf, curOpts.numBase);

    if (bytes < curOpts.fillWidth) {
        fill(curOpts.fillWidth - bytes, curOpts.fillChar);
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(unsigned long long val) {
    char buf[21];
    size_t bytes = uitoa<unsigned long long>(val, buf, curOpts.numBase);

    if (bytes < curOpts.fillWidth) {
        fill(curOpts.fillWidth - bytes, curOpts.fillChar);
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(short val) {
    char buf[7];
    size_t bytes = itoa<short>(val, buf, curOpts.numBase);

    if (bytes < curOpts.fillWidth) {
        fill(curOpts.fillWidth - bytes, curOpts.fillChar);
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(unsigned short val) {
    char buf[6];
    size_t bytes = uitoa<unsigned short>(val, buf, curOpts.numBase);

    if (bytes < curOpts.fillWidth) {
        fill(curOpts.fillWidth - bytes, curOpts.fillChar);
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(long val) {
    char buf[12];
    size_t bytes = itoa<long>(val, buf, curOpts.numBase);

    if (bytes < curOpts.fillWidth) {
        fill(curOpts.fillWidth - bytes, curOpts.fillChar);
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(unsigned long val) {
    char buf[11];
    size_t bytes = uitoa<unsigned long>(val, buf, curOpts.numBase);

    if (bytes < curOpts.fillWidth) {
        fill(curOpts.fillWidth - bytes, curOpts.fillChar);
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(int val) {
    char buf[12];
    size_t bytes = itoa<int>(val, buf, curOpts.numBase);

    if (bytes < curOpts.fillWidth) {
        fill(curOpts.fillWidth - bytes, curOpts.fillChar);
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(unsigned int val) {
    char buf[11];
    size_t bytes = uitoa<unsigned int>(val, buf, curOpts.numBase);

    if (bytes < curOpts.fillWidth) {
        fill(curOpts.fillWidth - bytes, curOpts.fillChar);
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(void* ptr) {
    char buf[11];
    uintptr_t ptr_addr = reinterpret_cast<uintptr_t>(ptr);

    size_t bytes = uitoa<uintptr_t>(ptr_addr, buf, 16);
    write("0x");

    if (bytes < 8) {
        fill(8 - bytes, '0');
    }

    write(buf);
    return *this;
}

OStream& OStream::operator <<(OStream& (*pf)(OStream&)) {
    return pf(*this);
}

int OStream::registerOutputBuffer(OStreamBuffer* buf) {
    if (numRegisteredOutputBuffers < OSTREAM_MAX_OUT_BUFS) {
        outputBuffers[numRegisteredOutputBuffers++] = buf;
    } else {
        return -EEXIST;
    }

    return 0;
}

int OStream::deregisterOutputBuffer(OStreamBuffer* buf) {
    uint32_t i;
    for (i = 0; i < numRegisteredOutputBuffers && outputBuffers[i] != buf; i++);

    if (i == numRegisteredOutputBuffers)
        return -ENOENT;

    numRegisteredOutputBuffers--;
    outputBuffers[i] = outputBuffers[numRegisteredOutputBuffers];
    outputBuffers[numRegisteredOutputBuffers] = NULL;

    return 0;
}

OStream& OStream::flush() {
    uint32_t i;
    for(i = 0; i < numRegisteredOutputBuffers; i++) {
        if (outputBuffers[i] != NULL)
            outputBuffers[i]->sync();
    }

    return *this;
}

OStream& OStream::put(char ch) {
    uint32_t i;
    for(i = 0; i < numRegisteredOutputBuffers; i++) {
        if (outputBuffers[i] != NULL)
            outputBuffers[i]->putn(&ch, 1);
    }

    return *this;
}

OStream& OStream::write(const char* str) {
    uint32_t i;
    for(i = 0; i < numRegisteredOutputBuffers; i++) {
        if (outputBuffers[i] != NULL)
            outputBuffers[i]->putn((char*) str, ::strlen(str));
    }

    return *this;
}

char OStream::getFill() {
    return curOpts.fillChar;
}

OStream& OStream::setFill(char fillch) {
    curOpts.fillChar = fillch;
    return *this;
}

size_t OStream::getWidth() {
    return curOpts.fillWidth;
}

OStream& OStream::setWidth(size_t width) {
    curOpts.fillWidth = width;
    return *this;
}

OStream& OStream::setFmtFlags(unsigned int flags) {
    curOpts.fmtFlags |= flags;

    if (curOpts.fmtFlags & FmtFlags::hex) {
        curOpts.numBase = 16;
        curOpts.fmtFlags &= ~(FmtFlags::dec | FmtFlags::oct);
    } else if (curOpts.fmtFlags & FmtFlags::dec) {
        curOpts.numBase = 10;
        curOpts.fmtFlags &= ~(FmtFlags::hex | FmtFlags::oct);
    } else if (curOpts.fmtFlags & FmtFlags::oct) {
        curOpts.numBase = 8;
        curOpts.fmtFlags &= ~(FmtFlags::hex | FmtFlags::dec);
    }

    return *this;
}

OStream& OStream::unsetFmtFlags(unsigned int flags) {
    curOpts.fmtFlags &= ~flags;

    if (!(curOpts.fmtFlags & (FmtFlags::hex | FmtFlags::dec | FmtFlags::oct)))
        setFmtFlags(FmtFlags::dec);

    return *this;
}

/**
 * C++ version 0.4 char* style "itoa":
 * Written by Lukás Chmela
 * Released under GPLv3.
 * Modified by Raffaele Fioratto to support templates

 */
template <class T>
size_t OStream::itoa(T value, char* result, int base) {
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return 0; }

    const char* alphabet = ((curOpts.fmtFlags & FmtFlags::uppercase)
            ? "ZYXWVUTSRQPONMLKJIHGFEDCBA9876543210123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            : "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz");
    size_t bytes;
    char* ptr = result, *ptr1 = result, tmp_char;
    T tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = alphabet[35 + (tmp_value - value * base)];
    } while (value);

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    bytes = ptr - result;

    *ptr-- = '\0';

    while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }

    return bytes;
}

template <class T>
size_t OStream::uitoa(T value, char* result, int base) {
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return 0; }

    const char* alphabet = ((curOpts.fmtFlags & FmtFlags::uppercase)
            ? "ZYXWVUTSRQPONMLKJIHGFEDCBA9876543210123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            : "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz");
    size_t bytes;
    char* ptr = result, *ptr1 = result, tmp_char;
    T tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = alphabet[35 + (tmp_value - value * base)];
    } while (value);
    bytes = ptr - result;

    *ptr-- = '\0';

    while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }

    return bytes;
}

void OStream::fill(size_t num, char fillch) {
    size_t i;
    for(i = 0; i < num; i++) {
        put(fillch);
    }
}

OStream& endl(OStream& os) {
    os.put('\n');
    os.flush();
    return os;
}

OStream& hex(OStream& os) {
    os.setFmtFlags(FmtFlags::hex);
    return os;
}

OStream& dec(OStream& os) {
    os.setFmtFlags(FmtFlags::dec);
    return os;
}

OStream& oct(OStream& os) {
    os.setFmtFlags(FmtFlags::oct);
    return os;
}

OStream& boolalpha(OStream& os) {
    os.setFmtFlags(FmtFlags::boolalpha);
    return os;
}

OStream& noboolalpha(OStream& os) {
    os.unsetFmtFlags(FmtFlags::boolalpha);
    return os;
}

OStream& uppercase(OStream& os) {
    os.setFmtFlags(FmtFlags::uppercase);
    return os;
}

OStream& nouppercase(OStream& os) {
    os.unsetFmtFlags(FmtFlags::uppercase);
    return os;
}

}
