#include <string.h>
#include <stdlib.h>

void reverse( char *str )
{
	int l = strlen( str );
	int i = 0;
	int j = l - 1;
	l /= 2;
	while( i < l )
	{
		char tmp = str[i];
		str[i] = str[j];
		str[j] = tmp;
		i++;
		j--;
	}
}

char *itoa( int n, char *str, int base )
{
	int i = 0;
	int sign = n;
	if ( n < 0 )
		n = -n;
	do
	{
		str[i] = ( n % base );
		if ( str[i] > 9 )
			str[i] += ( 'A' - 10 );
		else
			str[i] += ( '0' );
		i++;
	} while( ( n /= base ) );
	if ( sign < 0 )
		str[i++] = '-';
	str[i] = 0;
	reverse( str );
	return str;
}

int sgn(int x) {
	return (0 < x) - (x < 0);
}

int abs(int x) {
	return x * sgn(x);
}

