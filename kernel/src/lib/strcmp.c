#include <string.h>
#include <stdlib.h>

int strcmp(const char *str1, const char *str2) {
    if(strlen(str1) != strlen(str2)) {
        return (strlen(str1) > strlen(str2) ? 1 : -1);
    }
    return memcmp(str1, str2, strlen(str1));
}

int strncmp(const char *str1, const char *str2, size_t n) {
    if(strlen(str1) != strlen(str2)) {
        return (strlen(str1) > strlen(str2) ? 1 : -1);
    }
    return memcmp(str1, str2, min(strlen(str1), n));
}

