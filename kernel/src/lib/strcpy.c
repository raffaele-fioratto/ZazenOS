#include <string.h>
#include <stdlib.h>

char* strcpy(char *to, const char *from) {
	return (char*) memcpy(to, from, strlen(from));
}

char* strncpy(char *to, const char *from, size_t n) {
    return (char*) memcpy(to, from, min(n, strlen(from)));
}

