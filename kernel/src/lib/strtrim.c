#include <string.h>

char* strtrim( char* str )
{
	int i;

	int len = strlen( str );
	char* ret = str;
	for( i = 0; i < len; i++ )
		if ( *ret == ' ' || *ret == '\t' || *ret == '\n' )
		{
			ret++;
			len--;
		} else
			break;

	for ( ; i < len; i++ )
		if ( *ret == ' ' || *ret == '\t' || *ret == '\n' )
		{
			*ret = 0;
			break;
		}
	return ret;
}

