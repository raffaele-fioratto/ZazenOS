// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <mm/heap/slab.h>
#include <arch/x86/mmu.h>
#include <string.h>
#include <global.h>
#include <ostream.h>
#include <errno.h>

namespace MM {

SlabCache* kmallocCaches[KMALLOC_NUM_CACHES];
SlabCache* dmaKmallocCaches[KMALLOC_NUM_CACHES];

uint32_t kmallocSizes[KMALLOC_NUM_CACHES] =    { 8, 16, 32, 64, 96, 128, 192, 256, 512, 1024, 2048, 4096, 8192 };
uint32_t kmallocNumPages[KMALLOC_NUM_CACHES] = { 1,  1,  1,  1,  1,   1,   1,   1,   2,    4,    8,    8,    8 };
const char* kmallocNames[KMALLOC_NUM_CACHES] = {
    "kmalloc-8",
    "kmalloc-16",
    "kmalloc-32",
    "kmalloc-64",
    "kmalloc-96",
    "kmalloc-128",
    "kmalloc-192",
    "kmalloc-256",
    "kmalloc-512",
    "kmalloc-1024",
    "kmalloc-2048",
    "kmalloc-4096",
    "kmalloc-8192"
};

const char* dmaKmallocNames[KMALLOC_NUM_CACHES] = {
    "dma-kmalloc-8",
    "dma-kmalloc-16",
    "dma-kmalloc-32",
    "dma-kmalloc-64",
    "dma-kmalloc-96",
    "dma-kmalloc-128",
    "dma-kmalloc-192",
    "dma-kmalloc-256",
    "dma-kmalloc-512",
    "dma-kmalloc-1024",
    "dma-kmalloc-2048",
    "dma-kmalloc-4096",
    "dma-kmalloc-8192"
};

SlabCache* kmallocObjsArrayCaches[KMALLOC_NUM_CACHES];
uint32_t kmallocObjsArraySizes[KMALLOC_NUM_CACHES] =    { 2048, 1024, 512, 256, 168, 128, 84, 64, 64, 64, 64, 32, 16 };
uint32_t kmallocObjsArrayNumPages[KMALLOC_NUM_CACHES] = {    8,    4,   2,   1,   1,   1,  1,  1,  1,  1,  1,  1,  1 };
const char* kmallocObjsArrayNames[KMALLOC_NUM_CACHES] = {
    "kmalloc-array-8",
    "kmalloc-array-16",
    "kmalloc-array-32",
    "kmalloc-array-64",
    "kmalloc-array-96",
    "kmalloc-array-128",
    "kmalloc-array-192",
    "kmalloc-array-256",
    "kmalloc-array-512",
    "kmalloc-array-1024",
    "kmalloc-array-2048",
    "kmalloc-array-4096",
    "kmalloc-array-8192"
};

SlabCache slabCacheCache;
SlabCache slabNodeCache;
SlabCache freeVirtualSpaceNodeCache;
SlabManager slab;

SlabCache::SlabCache() { }
SlabCache::~SlabCache() {
    while (!free.isEmpty()) {
        SlabNode* node = free.pop_front();
        if (isSlabOff()) {
            objsArrayCache->freeObject(node->getValue().freeObjsPtr);
            AddressInterval in(node->getValue().slabAddr);
            deallocAndUnmapVirtSpace(in);
            slabNodeCache.deleteObject(node);
        } else {
            AddressInterval in(node, pages * X86::MMU::PAGE_SIZE);
            deallocAndUnmapVirtSpace(in);
        }
    }

    while (!partial.isEmpty()) {
        SlabNode* node = partial.pop_front();
        if (isSlabOff()) {
            objsArrayCache->freeObject(node->getValue().freeObjsPtr);
            AddressInterval in(node->getValue().slabAddr);
            deallocAndUnmapVirtSpace(in);
            slabNodeCache.deleteObject(node);
        } else {
            AddressInterval in(node, pages * X86::MMU::PAGE_SIZE);
            deallocAndUnmapVirtSpace(in);
        }
    }

    while (!full.isEmpty()) {
        SlabNode* node = full.pop_front();
        if (isSlabOff()) {
            objsArrayCache->freeObject(node->getValue().freeObjsPtr);
            AddressInterval in(node->getValue().slabAddr);
            deallocAndUnmapVirtSpace(in);
            slabNodeCache.deleteObject(node);
        } else {
            AddressInterval in(node, pages * X86::MMU::PAGE_SIZE);
            deallocAndUnmapVirtSpace(in);
        }
    }
}

// TODO: implement proper rollback in case of oom instead of simply exit
int SlabCache::allocAndMapVirtSpace(AddressInterval &out) {
    uint32_t size = pages * X86::MMU::PAGE_SIZE;
    void* virtAddr = slab.allocVirtSpace(size);

    if (flags & Flags::PhysImplMultiplePages) {
        void* physAddr = phys.allocatePages(physZone, pages);
        if (physAddr == NULL) {
            return -ENOMEM;
        }
        X86::MMU::mapPageRange(physAddr, virtAddr, pages, X86::MMU::PdeFlags::PDE_READ_WRITE);
    } else {
        uint32_t i;
        uintptr_t virtAddrPtr = reinterpret_cast<uintptr_t>(virtAddr);
        for (i = 0; i < pages; i++, virtAddrPtr += X86::MMU::PAGE_SIZE) {
            void* physAddr = phys.allocatePage(physZone);
            if (physAddr == NULL) {
                return -ENOMEM;
            }
            X86::MMU::mapPage(physAddr, (void*) virtAddrPtr, X86::MMU::PdeFlags::PDE_READ_WRITE);
        }
    }

    out.startAddr = reinterpret_cast<uintptr_t>(virtAddr);
    out.length = size;

    return 0;
}

int SlabCache::deallocAndUnmapVirtSpace(AddressInterval in) {
    slab.freeVirtSpace(in);

    if (flags & Flags::PhysImplMultiplePages) {
        void* physAddr = X86::MMU::getPhysicalAddress((void*) in.startAddr);
        MM::phys.deallocatePages(physZone, physAddr, pages);
        X86::MMU::unmapPageRange((void*) in.startAddr, in.length);
    } else {
        for(uintptr_t i = 0, addr = in.startAddr; i < pages; i++, addr += X86::MMU::PAGE_SIZE) {
            void* physAddr = X86::MMU::getPhysicalAddress((void*) addr);
            phys.deallocatePage(physZone, physAddr);
            X86::MMU::unmapPage((void*) addr);
        }
    }

    return 0;
}

int SlabCache::allocSlab(AddressInterval objsAddr) {
    SlabNode* newSlab;
    if (!(flags & Flags::SlabOff)) {
        newSlab = new ((void*) objsAddr.startAddr) SlabNode;
        newSlab->getValue().inuse = 0;
        newSlab->getValue().freeIndex = 0;
        newSlab->getValue().slabAddr.startAddr = objsAddr.startAddr + sizeof(SlabNode) + objsPerSlab * sizeof(uintptr_t);
        newSlab->getValue().slabAddr.length = objsAddr.length - (newSlab->getValue().slabAddr.startAddr - objsAddr.startAddr);
        newSlab->getValue().freeObjsPtr = reinterpret_cast<uintptr_t*>(objsAddr.startAddr + sizeof(SlabNode));
    } else {
        newSlab = slabNodeCache.newObject<SlabNode>();
        newSlab->getValue().inuse = 0;
        newSlab->getValue().freeIndex = 0;
        newSlab->getValue().slabAddr = objsAddr;
        newSlab->getValue().freeObjsPtr = reinterpret_cast<uintptr_t*>(objsArrayCache->allocObject());
    }

    for(uintptr_t addr = newSlab->getValue().slabAddr.startAddr, i = 0;
        i < objsPerSlab; addr += objSize, i++) {
        newSlab->getValue().freeObjsPtr[i] = addr;
    }

    free.push_front(newSlab);

    return 0;
}

void* SlabCache::allocObject() {
    lock(spinlock) {
        SlabNode* nodeFrom;

        if (!partial.isEmpty()) {
            nodeFrom = partial.pop_front();
        } else if (!free.isEmpty()) {
            nodeFrom = free.pop_front();
        } else {
            AddressInterval out;
            if (allocAndMapVirtSpace(out) < 0) {
                return NULL;
            }

            if (allocSlab(out) < 0) {
                return NULL;
            }

            nodeFrom = free.pop_front();
        }

        nodeFrom->getValue().inuse++;
        void *ret = reinterpret_cast<void*>(nodeFrom->getValue().freeObjsPtr[nodeFrom->getValue().freeIndex++]);
        if (nodeFrom->getValue().inuse == objsPerSlab) {
            full.push_front(nodeFrom);
        } else {
            partial.push_front(nodeFrom);
        }

        return ret;
    }
}

int SlabCache::freeObject(void* addr) {
    lock(spinlock) {
        SlabNode* nodeTo = NULL;
        AddressInterval objInterval(addr, objSize);
        if (!partial.isEmpty()) {
            SlabList::Iterator it = partial.begin();
            SlabList::Iterator end = partial.end();
            for (; nodeTo == NULL && it != end; it++) {
                if ((*it)->getValue().slabAddr.contains(objInterval)) {
                    nodeTo = *it;
                }
            }
        }

        if (nodeTo == NULL) {
            if(!full.isEmpty()) {
                SlabList::Iterator it = full.begin();
                SlabList::Iterator end = full.end();
                for (; nodeTo == NULL && it != end; it++) {
                    if ((*it)->getValue().slabAddr.contains(objInterval)) {
                        nodeTo = *it;
                    }
                }
            }
        }

        if (nodeTo == NULL) {
            return -ENOENT;
        }

        nodeTo->getValue().freeObjsPtr[--nodeTo->getValue().freeIndex] = reinterpret_cast<uintptr_t>(addr);
        if (nodeTo->getValue().inuse == 1) {
            partial.remove(nodeTo);
            free.push_front(nodeTo);
        } else if (nodeTo->getValue().inuse == objsPerSlab) {
            full.remove(nodeTo);
            partial.push_front(nodeTo);
        }

        nodeTo->getValue().inuse--;

        return 0;
    }
}

int SlabCache::shrink() {
    lock(spinlock) {
        while (!free.isEmpty()) {
            SlabNode* node = free.pop_front();
            if (isSlabOff()) {
                objsArrayCache->freeObject(node->getValue().freeObjsPtr);
                AddressInterval in(node->getValue().slabAddr);
                deallocAndUnmapVirtSpace(in);
                slabNodeCache.deleteObject(node);
            } else {
                AddressInterval in(node, pages * X86::MMU::PAGE_SIZE);
                deallocAndUnmapVirtSpace(in);
            }
        }

        return 0;
    }
}

void SlabManager::init() {
    AddressInterval slabAddr;
    slabNodeCache.objSize = sizeof(Slab);
    slabNodeCache.pages = 1;
    slabNodeCache.objsPerSlab = (X86::MMU::PAGE_SIZE - sizeof(SlabNode)) / (sizeof(uintptr_t) + sizeof(SlabNode));
    slabNodeCache.physZone = Physical::Zone::Normal;
    slabNodeCache.flags = phys.supportsMultiplePages(slabNodeCache.physZone) ? SlabCache::Flags::PhysImplMultiplePages : 0;
    ::strncpy(slabNodeCache.name, "slabNode", SLAB_CACHE_NAMELEN);
    void* physPage = phys.allocatePage(Physical::Zone::Normal);
    X86::MMU::mapPage(physPage, (void*) 0xD0000000, X86::MMU::PdeFlags::PDE_READ_WRITE);
    slabAddr.startAddr = 0xD0000000;
    slabAddr.length = X86::MMU::PAGE_SIZE;
    slabNodeCache.allocSlab(slabAddr);

    slabCacheCache.objSize = sizeof(SlabCache);
    slabCacheCache.pages = 1;
    slabCacheCache.objsPerSlab = (X86::MMU::PAGE_SIZE - sizeof(SlabNode)) / (sizeof(uintptr_t) + sizeof(SlabCache));
    slabCacheCache.physZone = Physical::Zone::Normal;
    slabCacheCache.flags = phys.supportsMultiplePages(slabCacheCache.physZone) ? SlabCache::Flags::PhysImplMultiplePages : 0;
    ::strncpy(slabCacheCache.name, "slabCache", SLAB_CACHE_NAMELEN);
    physPage = phys.allocatePage(Physical::Zone::Normal);
    X86::MMU::mapPage(physPage, (void*) 0xD0001000, X86::MMU::PdeFlags::PDE_READ_WRITE);
    slabAddr.startAddr = 0xD0001000;
    slabCacheCache.allocSlab(slabAddr);

    freeVirtualSpaceNodeCache.objSize = sizeof(FreeVirtSpaceNode);
    freeVirtualSpaceNodeCache.pages = 1;
    freeVirtualSpaceNodeCache.objsPerSlab = (X86::MMU::PAGE_SIZE - sizeof(SlabNode)) / (sizeof(uintptr_t) + sizeof(FreeVirtSpaceNode));
    freeVirtualSpaceNodeCache.physZone = Physical::Zone::Normal;
    freeVirtualSpaceNodeCache.flags = phys.supportsMultiplePages(freeVirtualSpaceNodeCache.physZone) ? SlabCache::Flags::PhysImplMultiplePages : 0;
    ::strncpy(freeVirtualSpaceNodeCache.name, "freeVirtSpaceNode", SLAB_CACHE_NAMELEN);
    physPage = phys.allocatePage(Physical::Zone::Normal);
    X86::MMU::mapPage(physPage, (void*) 0xD0002000, X86::MMU::PdeFlags::PDE_READ_WRITE);
    slabAddr.startAddr = 0xD0002000;
    freeVirtualSpaceNodeCache.allocSlab(slabAddr);

    FreeVirtSpaceNode *virtNode = freeVirtualSpaceNodeCache.newObject<FreeVirtSpaceNode>();
    virtNode->getValue().startAddr = 0xD0003000;
    virtNode->getValue().length = 256 * 1024 * 1024 - (3 * 4096);

    slab.freeVirtualSpace.insert(virtNode->getValue().length, virtNode);

    for(uint32_t i = 0; i < KMALLOC_NUM_CACHES; i++) {
        kmallocObjsArrayCaches[i] = slabCacheCache.newObject<SlabCache>();
        kmallocObjsArrayCaches[i]->objSize = kmallocObjsArraySizes[i];
        kmallocObjsArrayCaches[i]->pages = kmallocObjsArrayNumPages[i];
        kmallocObjsArrayCaches[i]->objsPerSlab = X86::MMU::PAGE_SIZE * kmallocObjsArrayCaches[i]->pages / kmallocObjsArrayCaches[i]->objSize;
        kmallocObjsArrayCaches[i]->physZone = Physical::Zone::Normal;

        ::strncpy(kmallocObjsArrayCaches[i]->name, kmallocObjsArrayNames[i], SLAB_CACHE_NAMELEN);

        kmallocCaches[i] = slabCacheCache.newObject<SlabCache>();
        kmallocCaches[i]->objSize = kmallocSizes[i];
        kmallocCaches[i]->pages = kmallocNumPages[i];
        kmallocCaches[i]->flags = SlabCache::Flags::SlabOff;
        kmallocCaches[i]->objsPerSlab = X86::MMU::PAGE_SIZE * kmallocCaches[i]->pages / kmallocCaches[i]->objSize;
        kmallocCaches[i]->physZone = Physical::Zone::Normal;
        kmallocCaches[i]->objsArrayCache = kmallocObjsArrayCaches[i];

        ::strncpy(kmallocCaches[i]->name, kmallocNames[i], SLAB_CACHE_NAMELEN);

        dmaKmallocCaches[i] = slabCacheCache.newObject<SlabCache>();
        dmaKmallocCaches[i]->objSize = kmallocSizes[i];
        dmaKmallocCaches[i]->pages = kmallocNumPages[i];
        dmaKmallocCaches[i]->flags = SlabCache::Flags::SlabOff;
        dmaKmallocCaches[i]->objsPerSlab = X86::MMU::PAGE_SIZE * kmallocCaches[i]->pages / kmallocCaches[i]->objSize;
        dmaKmallocCaches[i]->physZone = Physical::Zone::DMA;
        dmaKmallocCaches[i]->objsArrayCache = kmallocObjsArrayCaches[i];

        ::strncpy(dmaKmallocCaches[i]->name, dmaKmallocNames[i], SLAB_CACHE_NAMELEN);
    }
}

SlabCache* SlabManager::cacheCreate(const char* name, uint32_t objectSize, Physical::Zone zone, uint32_t pagesPerSlab, uint32_t flags) {
    SlabCache* cache = slabCacheCache.newObject<SlabCache>();
    cache->objSize = objectSize;
    cache->pages = pagesPerSlab;

    if (cache->objSize > 512) {
        cache->flags = SlabCache::Flags::SlabOff;
    }

    cache->flags |= flags;
    cache->objsArrayCache = NULL;

    if (cache->flags & SlabCache::Flags::SlabOff) {
        cache->objsPerSlab = X86::MMU::PAGE_SIZE * cache->pages / cache->objSize;
        uint32_t objArrayPtrSize = cache->objsPerSlab * sizeof(uintptr_t);
        for(int i = KMALLOC_NUM_CACHES - 1; i >= 0; i--) {
            if (kmallocObjsArraySizes[i] >= objArrayPtrSize) {
                cache->objsArrayCache = kmallocObjsArrayCaches[i];
                break;
            }
        }

        if (cache->objsArrayCache == NULL) {
            slabCacheCache.deleteObject(cache);
            return NULL;
        }
    } else {
        cache->objsPerSlab = (X86::MMU::PAGE_SIZE * cache->pages - sizeof(SlabNode)) / (sizeof(uintptr_t) + cache->objSize);
    }

    cache->physZone = zone;

    ::strncpy(cache->name, name, SLAB_CACHE_NAMELEN);

    return cache;
}

void SlabManager::cacheDestroy(SlabCache* cache) {
    cache->~SlabCache();
    slabCacheCache.freeObject(cache);
}

void* SlabManager::allocVirtSpace(uint32_t size) {
    SlabManager::FreeVirtSpaceNode* freeNode = slab.freeVirtualSpace.findMinimumGreater(size);
    if (freeNode == NULL) {
        return NULL;
    }
    freeVirtualSpace.remove(freeNode);
    uintptr_t startAddr = freeNode->getValue().startAddr;

    if (freeNode->getKey() == size) {
        freeVirtualSpaceNodeCache.freeObject(freeNode);
    } else {
        freeNode->getValue().startAddr += size;
        freeNode->getValue().length -= size;

        freeVirtualSpace.insert(freeNode->getValue().length, freeNode);
    }

    return reinterpret_cast<void*>(startAddr);
}

// TODO: scan tree for merging?
int SlabManager::freeVirtSpace(AddressInterval interval) {
    FreeVirtSpaceNode* freeNode = freeVirtualSpaceNodeCache.newObject<FreeVirtSpaceNode>();
    freeNode->getValue() = interval;

    slab.freeVirtualSpace.insert(interval.length, freeNode);

    return 0;
}

void* kmalloc(size_t size) {
    for(uint32_t index = 0; index < KMALLOC_NUM_CACHES; index++) {
        if (kmallocSizes[index] >= size) {
            return kmallocCaches[index]->allocObject();
        }
    }

    return NULL;
}

int kfree(void* addr, size_t size) {
    for(uint32_t index = 0; index < KMALLOC_NUM_CACHES; index++) {
        if (kmallocSizes[index] >= size) {
            return kmallocCaches[index]->freeObject(addr);
        }
    }
    return -ENOENT;
}


// TODO: implement me!
int kfree(void* addr) {
    return -ENOENT;
}

}
