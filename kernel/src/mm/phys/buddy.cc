// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <mm/phys/buddy.h>
#include <arch/x86/mmu.h>
#include <string.h>
#include <global.h>
#include <ostream.h>
#include <arch/x86/inline.h>

namespace MM {

extern AddressInterval physMMInterval;

PhysicalBuddy::PhysicalBuddy() {
    pageSizeOrder = X86::bsf(X86::MMU::PAGE_SIZE);
    minBlockSize = X86::MMU::PAGE_SIZE;
    maxBlockSize = minBlockSize << BUDDY_MAX_ORDER;
}

PhysicalBuddy::~PhysicalBuddy() {

}

void* PhysicalBuddy::allocatePage() {
    return allocatePages(1);
}

void* PhysicalBuddy::allocatePages(uint32_t num) {
    int bsrNum = X86::bsr(num);
    int buddyOrder = bsrNum + (bsrNum != X86::bsf(num) ? 1 : 0);
    if (buddyOrder > BUDDY_MAX_ORDER) {
        return NULL;
    }

    if (!zones[buddyOrder].isEmpty()) {
        BuddyZoneNode* freeNode = zones[buddyOrder].rightmostNode();
        zones[buddyOrder].remove(freeNode);
        void* ret = reinterpret_cast<void*>(freeNode->getValue() * (minBlockSize << buddyOrder));
        void* retEnd = reinterpret_cast<void*>((freeNode->getValue() + 1) * (minBlockSize << buddyOrder));

        phys.setPhysicalPageRangeFlags(ret, retEnd, PhysicalPageFlags::InUse);

        return ret;
    }

    int freeIndex = split(buddyOrder);

    void* ret = reinterpret_cast<void*>(freeIndex * (minBlockSize << buddyOrder));
    void* retEnd = reinterpret_cast<void*>((freeIndex + 1) * (minBlockSize << buddyOrder));

    phys.setPhysicalPageRangeFlags(ret, retEnd, PhysicalPageFlags::InUse);

    return ret;
}

void PhysicalBuddy::deallocatePage(void* addr) {
    deallocatePages(addr, 1);
}

void PhysicalBuddy::deallocatePages(void* addr, uint32_t num) {
    int bsrNum = X86::bsr(num);
    int buddyOrder = bsrNum + (bsrNum != X86::bsf(num) ? 1 : 0);
    if (buddyOrder > BUDDY_MAX_ORDER) {
        return;
    }

    int freeIndex = reinterpret_cast<uintptr_t>(addr) / (minBlockSize << buddyOrder);
    BuddyZoneNode* buddyFreeNode = zones[buddyOrder].find(freeIndex & 1 ? freeIndex - 1 : freeIndex);
    if (NULL == buddyFreeNode) {
        BuddyZoneNode* freeNode = allocNode();
        freeNode->setValue(freeIndex);
        zones[buddyOrder].insert(freeIndex, freeNode);

        void* addrEnd = reinterpret_cast<void*>((freeIndex + 1) * (minBlockSize << buddyOrder));
        phys.unsetPhysicalPageRangeFlags(addr, addrEnd, PhysicalPageFlags::InUse);
        return;
    }

    int i = buddyOrder;
    do {
        zones[i].remove(buddyFreeNode);
        deallocNode(buddyFreeNode);
        freeIndex >>= 1;
        i++;
    } while(i < (BUDDY_MAX_ORDER - 1) && NULL == (buddyFreeNode = zones[i].find(freeIndex & 1 ? freeIndex - 1 : freeIndex)));

    buddyFreeNode = allocNode();
    buddyFreeNode->setValue(freeIndex);
    zones[i].insert(freeIndex, buddyFreeNode);

    void* addrEnd = reinterpret_cast<void*>((freeIndex + 1) * (minBlockSize << buddyOrder));
    phys.unsetPhysicalPageRangeFlags(addr, addrEnd, PhysicalPageFlags::InUse);
}

void PhysicalBuddy::printStatistics() {
    uint32_t i = 0;
    while (i < BUDDY_NUM) {
        KLibcpp::kout << "Free " << (minBlockSize << i) / 1024 << "KB blocks: ";
        BuddyZone::MemRBTreeIterator it = zones[i].begin();
        BuddyZone::MemRBTreeIterator end = zones[i].end();
        for(; it != end; it++) {
            uintptr_t startAddr = ((*it)->getValue() * minBlockSize << i);
            uintptr_t endAddr = startAddr + (minBlockSize << i);
            KLibcpp::kout << "[mem " << (void*) startAddr << "-" << (void*) endAddr << "] ";
        }
        KLibcpp::kout << KLibcpp::endl;
        i++;
    }
}

int PhysicalBuddy::init(uintptr_t startAddr, uintptr_t regionLength) {
    size_t regionPages = ROUND_UP(regionLength, X86::MMU::PAGE_SIZE);
    size_t sizeToAlloc = X86::MMU::pageAlign((regionPages * sizeof(BuddyZoneNode) + regionPages * sizeof(BuddyZoneNode*)) >> 1);
    size_t pagesToAlloc = sizeToAlloc / X86::MMU::PAGE_SIZE;
    uintptr_t endAddr = startAddr + regionLength;

    size_t i = 0;
    uintptr_t virtStartAddr = physMMInterval.startAddr + physMMInterval.length;
    uintptr_t physAddr = startAddr;
    uintptr_t virtAddr = virtStartAddr;

    while (i < pagesToAlloc && physAddr < regionLength) {
        if (phys.isPhysicalPageFree(physAddr)) {
            X86::MMU::mapPageFast((void*) physAddr, (void*) virtAddr, X86::MMU::PteFlags::PTE_PRESENT | X86::MMU::PteFlags::PTE_READ_WRITE);
            phys.setPhysicalPageFlags(physAddr, MM::PhysicalPageFlags::InUse);
            virtAddr += X86::MMU::PAGE_SIZE;
            i++;
        }
        physAddr += X86::MMU::PAGE_SIZE;
    }

    if (i < pagesToAlloc) {
        KLibcpp::kout << SOURCE_POS << "Physical region isn't big enough to hold metadata needed to manage it" << KLibcpp::endl;
        return -1;
    }

    physMMInterval.length += X86::MMU::pageAlign(sizeToAlloc);

    freeNodes = reinterpret_cast<BuddyZoneNode*>(virtStartAddr);
    freeNodesStack = reinterpret_cast<BuddyZoneNode**>(freeNodes + (regionPages >> 1));
    freeNodesIndex = 0;
    freeNodesMaxIndex = regionPages >> 1;
    ::memset(freeNodes, 0, sizeToAlloc);

    for(i = 0, virtAddr = virtStartAddr; i < freeNodesMaxIndex; i++, virtAddr += sizeof(BuddyZoneNode)) {
        freeNodesStack[i] = reinterpret_cast<BuddyZoneNode*>(virtAddr);
    }

    physAddr = startAddr;
    while(physAddr < endAddr) {
        if (phys.isPhysicalPageFree(physAddr)) {
            AddressInterval freeRegion;
            freeRegion.startAddr = physAddr;
            freeRegion.length = X86::MMU::PAGE_SIZE;
            physAddr += X86::MMU::PAGE_SIZE;
            while(phys.isPhysicalPageFree(physAddr)) {
                freeRegion.length += X86::MMU::PAGE_SIZE;
                physAddr += X86::MMU::PAGE_SIZE;
            }
            freeMemoryRegion(freeRegion);
        }
        physAddr += X86::MMU::PAGE_SIZE;
    }

    return 0;
}

BuddyZoneNode* PhysicalBuddy::allocNode() {
    if (freeNodesIndex < freeNodesMaxIndex) {
        return freeNodesStack[freeNodesIndex++];
    }

    KLibcpp::kout << SOURCE_POS << "freeNodesIndex went out of bounds!" << KLibcpp::endl;

    return NULL;
}

void PhysicalBuddy::deallocNode(BuddyZoneNode* node) {
    if (freeNodesIndex > 0) {
        freeNodesStack[--freeNodesIndex] = node;
        return;
    }

    KLibcpp::kout << SOURCE_POS << "freeNodesIndex underrun, possible double free?" << KLibcpp::endl;
}

void PhysicalBuddy::freeMemoryRegion(AddressInterval interval) {
    int32_t buddyOrder;
    uint32_t buddyIndex;
    BuddyZoneNode* freeNode;
    uint32_t freeBlockSize;
    uintptr_t addr = interval.startAddr + (interval.startAddr % X86::MMU::PAGE_SIZE ? X86::MMU::PAGE_SIZE - (interval.startAddr % X86::MMU::PAGE_SIZE) : 0);
    uintptr_t length = interval.length - (addr != interval.startAddr ? addr - interval.startAddr : 0);

    while (length > 0) {
        int bsrAddr = X86::bsr(addr);
        if (bsrAddr != X86::bsf(addr)) {
            uintptr_t partialLength = ::min(length, (1 << (bsrAddr + 1)) - addr);
            length -= partialLength;
            while (partialLength > 0) {
                buddyOrder = X86::bsf(addr) - pageSizeOrder;
                if (buddyOrder > BUDDY_MAX_ORDER) {
                    buddyOrder = BUDDY_MAX_ORDER;
                }

                freeBlockSize = (minBlockSize << buddyOrder);

                if (freeBlockSize > partialLength) {
                    buddyOrder = X86::bsr(partialLength) - pageSizeOrder;
                    if (buddyOrder > BUDDY_MAX_ORDER) {
                        buddyOrder = BUDDY_MAX_ORDER;
                    }
                    freeBlockSize = (minBlockSize << buddyOrder);
                }

                buddyIndex = addr / freeBlockSize;

                freeNode = allocNode();
                freeNode->setValue(buddyIndex);
                zones[buddyOrder].insert(buddyIndex, freeNode);
                partialLength -= freeBlockSize;
                addr += freeBlockSize;
            }
            continue;
        }

        buddyOrder = bsrAddr - pageSizeOrder;
        if (buddyOrder > BUDDY_MAX_ORDER) {
            buddyOrder = BUDDY_MAX_ORDER;
        }

        freeBlockSize = (minBlockSize << buddyOrder);

        if (freeBlockSize > length) {
            buddyOrder = X86::bsr(length) - pageSizeOrder;
            if (buddyOrder > BUDDY_MAX_ORDER) {
                buddyOrder = BUDDY_MAX_ORDER;
            }
            freeBlockSize = (minBlockSize << buddyOrder);
        }

        buddyIndex = addr / freeBlockSize;

        freeNode = allocNode();
        freeNode->setValue(buddyIndex);
        zones[buddyOrder].insert(buddyIndex, freeNode);
        length -= freeBlockSize;
        addr += freeBlockSize;
    }
}

int PhysicalBuddy::split(int from) {
    int i = from + 1;
    while (i < BUDDY_MAX_ORDER && zones[i].isEmpty()) i++;

    if (i >= BUDDY_MAX_ORDER) {
        KLibcpp::kout << SOURCE_POS << "Out of Memory!" << KLibcpp::endl;
        return -1;
    }

    BuddyZoneNode* freeNode = zones[i].rightmostNode();
    zones[i].remove(freeNode);
    int freeIndex = freeNode->getValue();
    deallocNode(freeNode);
    i--;

    while (i >= from) {
        freeIndex <<= 1;
        freeNode = allocNode();
        freeNode->setValue(freeIndex);
        zones[i].insert(freeIndex, freeNode);
        i--;
    }

    return freeIndex - 1;
}

}
