// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <mm/mm.h>
#include <mm/phys/stack.h>
#include <arch/x86/mmu.h>
#include <sys/multiboot.h>
#include <stdlib.h>
#include <ostream.h>

extern uintptr_t __kernel_brk;

namespace MM {

int PhysicalStack::numInitializedInstances = 0;

PhysicalStack::PhysicalStack() {
    firstFreePageAddr = 0;
    virtMapAddr = numInitializedInstances * X86::MMU::PAGE_SIZE + KERNEL_BASE_VIRT_ADDR + reinterpret_cast<uintptr_t>(&__kernel_brk);

    numInitializedInstances++;
}

PhysicalStack::~PhysicalStack() {

}

int PhysicalStack::init(uintptr_t startAddr, uintptr_t regionLength) {
    physStartAddr = startAddr;
    physRegionLen = regionLength;

    Multiboot::MMapIterator mmapIt = mboot.getMMapIterator();
    Multiboot::MMapIterator mmapEnd = mboot.getLastMMapEntry();
    Multiboot::MMapEntry ent;
    uint32_t numPages;
    uint32_t i;
    uintptr_t freeAddr;
    uintptr_t intEndAddr;
    AddressInterval int1(startAddr, regionLength);
    AddressInterval out;

    for (; mmapIt != mmapEnd; mmapIt++) {
        ent = *mmapIt;
        if (!ent.isAvailable())
            continue;

        AddressInterval int2(ent.getStartAddr(), ent.getLength());

        if (int1.intersect(int2, out)) {
            intEndAddr = out.startAddr + out.length;
            numPages = out.length / X86::MMU::PAGE_SIZE;
            freeAddr = intEndAddr - X86::MMU::PAGE_SIZE;
            KLibcpp::kout << "Free phys range: [" << (void*) out.startAddr << " - " << (void*) intEndAddr << "]" << KLibcpp::endl;
            for (i = 0; i < numPages; i++, freeAddr -= X86::MMU::PAGE_SIZE) {
                if (phys.isPhysicalPageFree(freeAddr)) {
                    deallocatePage(reinterpret_cast<void*>(freeAddr));
                }
            }
        }
    }

    return 0;
}

void* PhysicalStack::allocatePage() {
    if (!(firstFreePageAddr & X86::MMU::PAGE_MASK)) {
        return NULL;
    }

    void* freePage = reinterpret_cast<void*>(firstFreePageAddr);
    X86::MMU::mapPageFast(freePage, reinterpret_cast<void*>(virtMapAddr), X86::MMU::PteFlags::PTE_PRESENT | X86::MMU::PteFlags::PTE_READ_WRITE);

    firstFreePageAddr = *(reinterpret_cast<uintptr_t*>(virtMapAddr));
    *(reinterpret_cast<uintptr_t*>(virtMapAddr)) = 0;

    X86::MMU::unmapPage(reinterpret_cast<void*>(virtMapAddr));
    phys.setPhysicalPageFlags(freePage, PhysicalPageFlags::InUse);

    return freePage;
}

void* PhysicalStack::allocatePages(uint32_t num) {
    K_UNUSED(num);
    return NULL;
}

void PhysicalStack::deallocatePage(void* addr) {
    X86::MMU::mapPageFast(reinterpret_cast<void*>(addr), reinterpret_cast<void*>(virtMapAddr), X86::MMU::PteFlags::PTE_PRESENT | X86::MMU::PteFlags::PTE_READ_WRITE);

    *(reinterpret_cast<uintptr_t*>(virtMapAddr)) = firstFreePageAddr;

    X86::MMU::unmapPage(reinterpret_cast<void*>(virtMapAddr));

    firstFreePageAddr = reinterpret_cast<uintptr_t>(addr);
    firstFreePageAddr &= X86::MMU::PAGE_MASK;
    phys.unsetPhysicalPageFlags(addr, PhysicalPageFlags::InUse);
}

void PhysicalStack::deallocatePages(void* addr, uint32_t num) {
    K_UNUSED(addr);
    K_UNUSED(num);
}

void PhysicalStack::printStatistics() {

}

}
