// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <mm/physical.h>
#include <mm/phys/stack.h>
#include <mm/phys/buddy.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ostream.h>
#include <arch/x86/mmu.h>
#include <sys/multiboot.h>
#include <sys/ksymtab.h>

extern "C" uintptr_t __kernel_start;
extern "C" uintptr_t __kstack_physical_end;
extern "C" uintptr_t ktable;
extern "C" uintptr_t kstacktab;
extern "C" uintptr_t kapictab;
extern "C" uintptr_t kdir;

namespace MM {

Physical phys;
PhysicalStack stackNormalZone;
PhysicalBuddy buddyDMAZone;
AddressInterval physMMInterval;

Physical::Physical() {
    ::memset(physImpls, 0, sizeof(PhysicalImplementer*) * NumZones);
}

Physical::~Physical() {

}

int Physical::initPageStructs(uintptr_t physFreeEndAddr) {
    uint32_t pages = physFreeEndAddr / X86::MMU::PAGE_SIZE;
    uint32_t sizeToAlloc = pages * sizeof(PhysicalPage);
    uint32_t mmuPagesToAlloc = ROUND_UP(sizeToAlloc, X86::MMU::PAGE_SIZE);
    uint32_t mmuTablesToAlloc = ROUND_UP(sizeToAlloc, X86::MMU::TABLE_SIZE);
    uintptr_t firstPhysFreeAddr = PHYSICAL_BORDER_ADDR;
    uintptr_t addrToMap = physMMInterval.startAddr;
    Multiboot::MMapIterator mmapIt = mboot.getMMapIterator();
    Multiboot::MMapIterator mmapEnd = mboot.getLastMMapEntry();

    /* Map tables into kernel address space */
    for (uint32_t i = 0; i < mmuTablesToAlloc; addrToMap += X86::MMU::TABLE_SIZE, firstPhysFreeAddr += X86::MMU::PAGE_SIZE, i++) {
        X86::MMU::mapTable((void*) firstPhysFreeAddr, (void*) addrToMap, X86::MMU::PdeFlags::PDE_PRESENT | X86::MMU::PdeFlags::PDE_READ_WRITE);
    }

    /* Map the actual pages that will contain the PhysicalPage array */
    addrToMap = physMMInterval.startAddr;
    for (uint32_t i = 0; i < mmuPagesToAlloc; i++, addrToMap += X86::MMU::PAGE_SIZE, firstPhysFreeAddr += X86::MMU::PAGE_SIZE) {
        X86::MMU::mapPageFast((void*) firstPhysFreeAddr, (void*) addrToMap, X86::MMU::PteFlags::PTE_PRESENT | X86::MMU::PteFlags::PTE_READ_WRITE);
    }

    physPages = reinterpret_cast<PhysicalPage*>(physMMInterval.startAddr);
    ::memset(physPages, 0, sizeToAlloc);
    physLastFreeAddr = physFreeEndAddr;

    /* Mark some pages that are already being in use */
    /* First physical page in RAM: it contains IVT */
    setPhysicalPageFlags(0x0UL, PhysicalPageFlags::InUse);

    /* MemMap tells us some reserved regions in RAM, mark them also in our structure */
    for (; mmapIt != mmapEnd; mmapIt++) {
        if (!(*mmapIt).isAvailable()) {
            setPhysicalPageRangeFlags((*mmapIt).getStartAddr(), (*mmapIt).getEndAddr(), PhysicalPageFlags::InUse);
        }
    }

    /* Mark pages used for mapping the kernel virtual address space in the MMU */
    setPhysicalPageFlags(&ktable, PhysicalPageFlags::InUse);
    setPhysicalPageFlags(&kstacktab, PhysicalPageFlags::InUse);
    setPhysicalPageFlags(&kapictab, PhysicalPageFlags::InUse);
    setPhysicalPageFlags(&kdir, PhysicalPageFlags::InUse);

    /* Mark pages that contain multiboot structures and strings */
    mboot.markPhysicalPages();

    /* Mark kernel ELF symbols tables */
    ksymtab.markPhysicalPages();

    /* Mark the region occupied by kernel */
    setPhysicalPageRangeFlags(&__kernel_start, &__kstack_physical_end, PhysicalPageFlags::InUse);

    /* Mark conventional video memory hole */
    setPhysicalPageRangeFlags(0xA0000, 0xF0000, PhysicalPageFlags::InUse);

    /* Mark pages that contain the pageStruct array as well */
    setPhysicalPageRangeFlags(PHYSICAL_BORDER_ADDR, firstPhysFreeAddr, PhysicalPageFlags::InUse);

    physMMInterval.length = X86::MMU::pageAlign(sizeToAlloc);

    return 0;
}

void Physical::setPhysicalPageFlags(uintptr_t addr, uint32_t flags) {
    if (addr > physLastFreeAddr) {
        return;
    }

    uint32_t idx = (addr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    physPages[idx].flags |= flags;
}

void Physical::setPhysicalPageFlags(void* addr, uint32_t flags) {
    uintptr_t addrPtr = reinterpret_cast<uintptr_t>(addr);
    if (addrPtr > physLastFreeAddr) {
        return;
    }

    uint32_t idx = (addrPtr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    physPages[idx].flags |= flags;
}

void Physical::unsetPhysicalPageFlags(uintptr_t addr, uint32_t flags) {
    if (addr > physLastFreeAddr) {
        return;
    }

    uint32_t idx = (addr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    physPages[idx].flags &= ~flags;
}

void Physical::unsetPhysicalPageFlags(void* addr, uint32_t flags) {
    uintptr_t addrPtr = reinterpret_cast<uintptr_t>(addr);
    if (addrPtr > physLastFreeAddr) {
        return;
    }

    uint32_t idx = (addrPtr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    physPages[idx].flags &= ~flags;
}

void Physical::setPhysicalPageRangeFlags(uintptr_t startAddr, uintptr_t endAddr, uint32_t flags) {
    if (startAddr > endAddr || startAddr > physLastFreeAddr || endAddr > physLastFreeAddr) {
        return;
    }

    uint32_t startIdx = (startAddr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    uint32_t endIdx = (endAddr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    for (uint32_t idx = startIdx; idx < endIdx; idx++) {
        physPages[idx].flags |= flags;
    }
}

void Physical::setPhysicalPageRangeFlags(void* startAddr, void* endAddr, uint32_t flags) {
    uintptr_t startAddrPtr = reinterpret_cast<uintptr_t>(startAddr);
    uintptr_t endAddrPtr = reinterpret_cast<uintptr_t>(endAddr);

    if (startAddrPtr > endAddrPtr || startAddrPtr > physLastFreeAddr || endAddrPtr > physLastFreeAddr) {
        return;
    }

    uint32_t startIdx = (startAddrPtr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    uint32_t endIdx = (endAddrPtr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    for (uint32_t idx = startIdx; idx < endIdx; idx++) {
        physPages[idx].flags |= flags;
    }
}

void Physical::unsetPhysicalPageRangeFlags(uintptr_t startAddr, uintptr_t endAddr, uint32_t flags) {
    if (startAddr > endAddr || startAddr > physLastFreeAddr || endAddr > physLastFreeAddr) {
        return;
    }

    uint32_t startIdx = (startAddr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    uint32_t endIdx = (endAddr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    for (uint32_t idx = startIdx; idx < endIdx; idx++) {
        physPages[idx].flags &= ~flags;
    }
}

void Physical::unsetPhysicalPageRangeFlags(void* startAddr, void* endAddr, uint32_t flags) {
    uintptr_t startAddrPtr = reinterpret_cast<uintptr_t>(startAddr);
    uintptr_t endAddrPtr = reinterpret_cast<uintptr_t>(endAddr);
    if (startAddrPtr > endAddrPtr || startAddrPtr > physLastFreeAddr || endAddrPtr > physLastFreeAddr) {
        return;
    }

    uint32_t startIdx = (startAddrPtr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    uint32_t endIdx = (endAddrPtr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    for (uint32_t idx = startIdx; idx < endIdx; idx++) {
        physPages[idx].flags &= ~flags;
    }
}

uint32_t Physical::getPhysicalPageFlags(uintptr_t addr) {
    if (addr > physLastFreeAddr) {
        return -1;
    }

    addr &= X86::MMU::PAGE_MASK;
    uint32_t idx = addr / X86::MMU::PAGE_SIZE;

    return physPages[idx].flags;
}

uint32_t Physical::getPhysicalPageFlags(void* addr) {
    uintptr_t addrPtr = reinterpret_cast<uintptr_t>(addr);
    if (addrPtr > physLastFreeAddr) {
        return -1;
    }

    uint32_t idx = (addrPtr & X86::MMU::PAGE_MASK) / X86::MMU::PAGE_SIZE;

    return physPages[idx].flags;
}

bool Physical::isPhysicalPageFree(uintptr_t addr) {
    return !(getPhysicalPageFlags(addr) & PhysicalPageFlags::InUse);
}

bool Physical::isPhysicalPageFree(void* addr) {
    return !(getPhysicalPageFlags(addr) & PhysicalPageFlags::InUse);
}

void* Physical::allocatePage(Zone zone) {
    if (zone > Normal || zone < DMA) {
        return NULL;
    }

    if (physImpls[zone] != NULL) {
        return physImpls[zone]->allocatePage();
    }

    return NULL;
}

void* Physical::allocatePages(Zone zone, uint32_t num) {
    if (zone > Normal || zone < DMA) {
        return NULL;
    }

    if (physImpls[zone] != NULL) {
        return physImpls[zone]->allocatePages(num);
    }

    return NULL;
}

void Physical::deallocatePage(Zone zone, void* addr) {
    if (zone > Normal || zone < DMA) {
        return;
    }

    if (physImpls[zone] != NULL) {
        physImpls[zone]->deallocatePage(addr);
    }
}

void Physical::deallocatePages(Zone zone, void* addr, uint32_t num) {
    if (zone > Normal || zone < DMA) {
        return;
    }

    if (physImpls[zone] != NULL) {
        physImpls[zone]->deallocatePages(addr, num);
    }
}

int Physical::registerImplementer(Zone zone, PhysicalImplementer* impl) {
    if (zone > Normal || zone < DMA) {
        return -ENOENT;
    }

    if (physImpls[zone] == NULL) {
        physImpls[zone] = impl;
        return 0;
    }

    return -EEXIST;
}

int Physical::deregisterImplementer(Zone zone, PhysicalImplementer* impl) {
    if (zone > Normal || zone < DMA) {
        return -ENOENT;
    }

    if (physImpls[zone] != NULL && physImpls[zone] == impl) {
        physImpls[zone] = NULL;
        return 0;
    }

    return -ENOENT;
}

void Physical::printStatistics(Zone zone) {
    if (zone > Normal || zone < DMA) {
        return;
    }

    if (physImpls[zone] != NULL) {
        physImpls[zone]->printStatistics();
    }
}

bool Physical::supportsMultiplePages(Zone zone) {
    if (zone > Normal || zone < DMA) {
        return false;
    }

    if (physImpls[zone] != NULL) {
        return physImpls[zone]->supportsMultiplePages();
    }

    return false;
}

int Physical::init(uintptr_t physFreeEndAddr) {
    physMMInterval.startAddr = PHYSPAGES_VIRT_START_ADDR;
    physMMInterval.length = 0;

    if (phys.initPageStructs(physFreeEndAddr) < 0) {
        return -1;
    }

    if (stackNormalZone.init(PHYSICAL_BORDER_ADDR) < 0) {
        return -1;
    }

    phys.registerImplementer(Physical::Zone::Normal, &stackNormalZone);

    if (buddyDMAZone.init(0, PHYSICAL_BORDER_ADDR) < 0) {
        return -1;
    }

    phys.registerImplementer(Physical::Zone::DMA, &buddyDMAZone);

    return 0;
}

}
