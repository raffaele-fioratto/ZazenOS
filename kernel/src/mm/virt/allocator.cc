// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <mm/virt/allocator.h>
#include <mm/virt/chunk.h>

#include <arch/x86/mmu.h>

#include <algorithm.h>

VirtAllocator::VirtAllocator(uintptr_t baseAddr, size_t regionSize) {
    baseAddr_ = X86::MMU::pageAlign(baseAddr);
    regionSize_ = round2(regionSize, true);
    freeBins_[regionSize_].push_front(new VirtChunk(VirtChunk::Free, baseAddr_, regionSize_));
}

void* VirtAllocator::alloc(size_t size) {
    size_t sizeRounded = round2(size, false);
    freeBinsIterator it = freeBins_.find(sizeRounded);

    VirtChunk* chunk;

    int splits = 0;
    while ((it == freeBins_.end() || (*it).empty()) && sizeRounded <= regionSize_) {
        splits++;
        sizeRounded <<= 1;
        it = freeBins_.find(sizeRounded);
    }

    if (sizeRounded > regionSize_) {
        return NULL;
    }

    while(splits > 0) {
        VirtChunk* chunkToSplit = freeBins_[sizeRounded].front();
        freeBins_[sizeRounded].pop_front();

        VirtChunk* newChunk = chunkToSplit->split();

        splits--;
        sizeRounded >>= 1;

        freeBins_[sizeRounded].push_front(newChunk);
        freeBins_[sizeRounded].push_front(chunkToSplit);
    }

    chunk = freeBins_[sizeRounded].front();
    freeBins_[sizeRounded].pop_front();

    chunk->setState(VirtChunk::InUse);
    allocatedChunks_[chunk->address()] = chunk;

    return (void*) chunk->address();
}

void VirtAllocator::dealloc(void* addr) {
    uintptr_t addrInt = (uintptr_t) addr;
    allocatedChunksIterator it = allocatedChunks_.find(addrInt);

    if (it == allocatedChunks_.end()) {
        return;
    }

    VirtChunk* chunk = *it;
    allocatedChunks_.erase(addrInt);

    while(chunk->isBuddyFree()) {
        KLibcpp::List<VirtChunk*>::Iterator bit = KLibcpp::find(freeBins_[chunk->buddy()->size()].begin(), freeBins_[chunk->buddy()->size()].end(), chunk->buddy());
        freeBins_[chunk->buddy()->size()].erase(bit);

        if (chunk->buddy()->address() < chunk->address())
            chunk = chunk->buddy();

        chunk->merge(chunk->buddy());
    }

    chunk->setState(VirtChunk::Free);

    freeBins_[chunk->size()].push_front(chunk);
}

size_t VirtAllocator::round2(size_t size, bool off) {
    if (__builtin_popcount(size) == 1) {
        return size;
    }

    int leading_zeros = __builtin_clz(size);
    if (off) {
        leading_zeros++;
    }
    return (size_t) (1 << (sizeof(size_t) * 8 - leading_zeros));
}