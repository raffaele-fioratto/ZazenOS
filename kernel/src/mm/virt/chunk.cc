// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mm/virt/chunk.h"

VirtChunk::VirtChunk(VirtChunk::State state, uintptr_t address, size_t size) :
        state_(state), address_(address), size_(size) {

}

VirtChunk::~VirtChunk() {
    buddies_.clear();
}

const VirtChunk::State& VirtChunk::state() const {
    return state_;
}

const uintptr_t& VirtChunk::address() const {
    return address_;
}

const size_t& VirtChunk::size() const {
    return size_;
}

void VirtChunk::setState(const State& state) {
    state_ = state;
}

VirtChunk* VirtChunk::split() {
    size_ /= 2;
    VirtChunk* buddy = new VirtChunk(state_, address_ + (uintptr_t) size_, size_);
    buddies_.push_front(buddy);
    buddy->buddies_.push_front(this);
    return buddy;
}

void VirtChunk::merge(VirtChunk* other) {
    size_ += other->size_;
    buddies_.pop_front();

    delete other;
}

bool VirtChunk::isBuddyFree() const {
    if (buddies_.empty())
        return false;

    return buddies_.front()->state() == State::Free;
}

VirtChunk* VirtChunk::buddy() const {
    if (buddies_.empty())
        return NULL;

    return buddies_.front();
}