// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <mm/virtual.h>
#include <arch/x86/process.h>

extern "C" uintptr_t __kstack;
extern "C" uintptr_t __kstack_end;
extern "C" uintptr_t __kcode;
extern "C" uintptr_t __kcode_end;
extern "C" uintptr_t __kdata;
extern "C" uintptr_t __kdata_end;

extern X86::Process kernelProcess;

namespace MM {

Virtual virt;

MappedRegion kernelCode, kernelData, kernelStack;
MappedRegionNode kCodeNode, kDataNode, kStackNode;

Virtual::Virtual() {

}

Virtual::~Virtual() {

}

int Virtual::init() {
    kernelCode.addressInterval.startAddr = reinterpret_cast<uintptr_t>(&__kcode);
    kernelCode.addressInterval.length = reinterpret_cast<uintptr_t>(&__kcode_end) - kernelCode.addressInterval.startAddr;
    kernelCode.perms = MappedRegion::Read | MappedRegion::Exec | MappedRegion::Private;
    kernelCode.pathName = "ZazenOS.elf";
    kCodeNode.setValue(&kernelCode);

    kernelData.addressInterval.startAddr = reinterpret_cast<uintptr_t>(&__kdata);
    kernelData.addressInterval.length = reinterpret_cast<uintptr_t>(&__kdata_end) - kernelData.addressInterval.startAddr;
    kernelData.perms = MappedRegion::Read | MappedRegion::Write | MappedRegion::Private;
    kernelData.pathName = "ZazenOS.elf";
    kDataNode.setValue(&kernelData);

    kernelStack.addressInterval.startAddr = reinterpret_cast<uintptr_t>(&__kstack);
    kernelStack.addressInterval.length = reinterpret_cast<uintptr_t>(&__kstack_end) - kernelStack.addressInterval.startAddr;
    kernelStack.perms = MappedRegion::Read | MappedRegion::Write | MappedRegion::Private;
    kernelStack.pathName = "ZazenOS.elf";
    kStackNode.setValue(&kernelStack);

    //kernelProcess.maps.insert(kernelCode.addressInterval, &kCodeNode);
    //kernelProcess.maps.insert(kernelData.addressInterval, &kDataNode);
    //kernelProcess.maps.insert(kernelStack.addressInterval, &kStackNode);

    return 0;
}

}
