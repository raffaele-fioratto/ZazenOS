// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <sys/ksymtab.h>
#include <arch/x86/mmu.h>
#include <mm/physical.h>
#include <string.h>
#include <ostream.h>

KSymbolTable ksymtab;

KSymbolTable::KSymbolTable() {
    symsInfo = NULL;
    isInfoValid = false;
    tableSize = 0;
}

KSymbolTable::~KSymbolTable() {

}

void KSymbolTable::setInfo(elfSyms* info) {
    if (info == NULL) {
        KLibcpp::kout << "No ELF symbols loaded" << KLibcpp::endl;
        return;
    }

    symsInfo = info;
    X86::MMU::mapPageRangeFast((void*) symsInfo->addr, (void*) ((uintptr_t) symsInfo->addr + KERNEL_BASE_VIRT_ADDR), symsInfo->size * symsInfo->num, X86::MMU::PteFlags::PTE_PRESENT);

    tableSize = symsInfo->size * symsInfo->num;
    Elf32_Shdr* shdr = reinterpret_cast<Elf32_Shdr*>(symsInfo->addr);
    shdrsStrTab = reinterpret_cast<const char*>(shdr[symsInfo->shndx].sh_addr);

    SectionHeaderIterator shdrIt = SectionHeaderIterator(symsInfo->addr, symsInfo->num, shdrsStrTab);
    SectionHeaderIterator shdrEnd = SectionHeaderIterator(symsInfo->addr + sizeof(Elf32_Shdr) * symsInfo->num, symsInfo->num, shdrsStrTab);

    for (; shdrIt != shdrEnd; shdrIt++) {
        SectionHeader hdr = *shdrIt;
        if (hdr.getType() == SHT_STRTAB) {
            if (!::strcmp(hdr.getName(), ".strtab")) {
                symsStrTable = reinterpret_cast<const char*>(hdr.getAddress());
            }
            tableSize += hdr.getSize();
        } else if (hdr.getType() == SHT_SYMTAB) {
            if  (!::strcmp(hdr.getName(), ".symtab")) {
                kernelSymsArray = reinterpret_cast<Elf32_Sym*>(hdr.getAddress());
                kernelNumSymbols = hdr.getSize() / sizeof(Elf32_Sym);
            }
            tableSize += hdr.getSize();
        }
    }

    X86::MMU::unmapPageRange((void*) ((uintptr_t) symsInfo->addr + KERNEL_BASE_VIRT_ADDR), symsInfo->size * symsInfo->num);
    X86::MMU::mapPageRangeFast((void*) symsInfo->addr, (void*) ((uintptr_t) symsInfo->addr + KERNEL_BASE_VIRT_ADDR), tableSize, X86::MMU::PteFlags::PTE_PRESENT);

    isInfoValid = true;
}

void KSymbolTable::markPhysicalPages() {
    MM::phys.setPhysicalPageRangeFlags(symsInfo->addr, X86::MMU::pageAlign(symsInfo->addr + tableSize), MM::PhysicalPageFlags::InUse);
}

KSymbolTable::KSymbolIterator KSymbolTable::getSymbolsIterator() {
    if (isInfoValid) {
        return KSymbolIterator(reinterpret_cast<uintptr_t>(kernelSymsArray), kernelNumSymbols, symsStrTable);
    }

    return KSymbolIterator(0, 0, NULL);
}

KSymbolTable::KSymbolIterator KSymbolTable::getSymbolsIteratorEnd() {
    if (isInfoValid) {
        return KSymbolIterator(reinterpret_cast<uintptr_t>(kernelSymsArray) + kernelNumSymbols * sizeof(Elf32_Sym), kernelNumSymbols, symsStrTable);
    }

    return KSymbolIterator(0, 0, NULL);
}

// TODO: implement demangling functionality to resolve C++ functions
KSymbolTable::KSymbol KSymbolTable::resolve(const char* name) {
    KSymbolIterator kSymIt = getSymbolsIterator();
    KSymbolIterator kSymEnd = getSymbolsIteratorEnd();

    for(; kSymIt != kSymEnd; kSymIt++) {
        KSymbol sym = *kSymIt;
        if (!::strcmp(sym.getName(), name)) {
            return sym;
        }
    }

    return KSymbol();
}

KSymbolTable::KSymbol KSymbolTable::resolve(uintptr_t addr) {
    KSymbolIterator kSymIt = getSymbolsIterator();
    KSymbolIterator kSymEnd = getSymbolsIteratorEnd();

    for(; kSymIt != kSymEnd; kSymIt++) {
        KSymbol sym = *kSymIt;
        if (sym.getValue() == addr ||
            (sym.getValue() < addr && addr < (sym.getValue() + sym.getSize()))) {
            return sym;
        }
    }

    return KSymbol();
}

KSymbolTable::SectionHeaderIterator KSymbolTable::getSectionHeaderIterator() {
    if (isInfoValid) {
        return SectionHeaderIterator(symsInfo->addr, symsInfo->num, shdrsStrTab);
    }

    return SectionHeaderIterator(0, 0, NULL);
}

KSymbolTable::SectionHeaderIterator KSymbolTable::getSectionHeaderIteratorEnd() {
    if (isInfoValid) {
        return SectionHeaderIterator(symsInfo->addr + sizeof(Elf32_Shdr) * symsInfo->num, symsInfo->num, shdrsStrTab);
    }

    return SectionHeaderIterator(0, 0, NULL);
}

KSymbolTable::SectionHeader::SectionHeader(Elf32_Shdr* header, const char* stringTbl)
    : shdr(header), shdrStrTbl(stringTbl) {
}

KSymbolTable::SectionHeader::~SectionHeader() {

}

Elf32_Word KSymbolTable::SectionHeader::getSize() {
    return shdr->sh_size;
}

Elf32_Word KSymbolTable::SectionHeader::getAddress() {
    return shdr->sh_addr;
}

Elf32_Word KSymbolTable::SectionHeader::getType() {
    return shdr->sh_type;
}

const char* KSymbolTable::SectionHeader::getName() {
    return shdrStrTbl + shdr->sh_name;
}

KSymbolTable::SectionHeaderIterator::SectionHeaderIterator(uintptr_t addr, uint32_t count, const char* stringTbl)
    : shdrAddr(addr), shdrsCount(count), shdrStrTbl(stringTbl) {
}

KSymbolTable::SectionHeaderIterator::SectionHeaderIterator(const SectionHeaderIterator& other) {
    shdrAddr = other.shdrAddr;
    shdrsCount = other.shdrsCount;
    shdrStrTbl = other.shdrStrTbl;
}

KSymbolTable::SectionHeaderIterator KSymbolTable::SectionHeaderIterator::SectionHeaderIterator::operator++() {
    shdrAddr += sizeof(Elf32_Shdr);

    return *this;
}

KSymbolTable::SectionHeaderIterator KSymbolTable::SectionHeaderIterator::SectionHeaderIterator::operator++(int) {
    SectionHeaderIterator temp(*this);

    operator++();

    return temp;
}

bool KSymbolTable::SectionHeaderIterator::operator==(const SectionHeaderIterator& rhs) {
    return (shdrAddr == rhs.shdrAddr && shdrsCount == rhs.shdrsCount && shdrStrTbl == rhs.shdrStrTbl);
}

bool KSymbolTable::SectionHeaderIterator::operator!=(const SectionHeaderIterator& rhs) {
    return (shdrAddr != rhs.shdrAddr || shdrsCount != rhs.shdrsCount || shdrStrTbl != rhs.shdrStrTbl);
}

KSymbolTable::SectionHeader KSymbolTable::SectionHeaderIterator::operator*() {
    return SectionHeader(reinterpret_cast<Elf32_Shdr*>(shdrAddr), shdrStrTbl);
}

KSymbolTable::KSymbol::KSymbol(Elf32_Sym* symbol, const char* stringTbl)
    : sym(symbol), symsStrTbl(stringTbl) {

}

KSymbolTable::KSymbol::~KSymbol() {

}

const char* KSymbolTable::KSymbol::getName() {
    return symsStrTbl + sym->st_name;
}

Elf32_Word KSymbolTable::KSymbol::getValue() {
    return sym->st_value;
}

Elf32_Word KSymbolTable::KSymbol::getSize() {
    return sym->st_size;
}

uint32_t KSymbolTable::KSymbol::getType() {
    return ELF32_ST_TYPE(sym->st_info);
}

uint32_t KSymbolTable::KSymbol::getBind() {
    return ELF32_ST_BIND(sym->st_info);
}

uint32_t KSymbolTable::KSymbol::getRelPos(uint32_t addr) {
    return addr - getValue();
}

KSymbolTable::KSymbolIterator::KSymbolIterator(uintptr_t addr, uint32_t count, const char* stringTbl)
    : symAddr(addr), symsCount(count), symsStrTbl(stringTbl) {

}

KSymbolTable::KSymbolIterator::KSymbolIterator(const KSymbolIterator& other) {
    symAddr = other.symAddr;
    symsCount = other.symsCount;
    symsStrTbl = other.symsStrTbl;
}

KSymbolTable::KSymbolIterator KSymbolTable::KSymbolIterator::operator++() {
    symAddr += sizeof(Elf32_Sym);

    return *this;
}

KSymbolTable::KSymbolIterator KSymbolTable::KSymbolIterator::operator++(int) {
    KSymbolIterator temp(*this);

    operator++();

    return temp;
}

bool KSymbolTable::KSymbolIterator::operator==(const KSymbolIterator& rhs) {
    return (symAddr == rhs.symAddr && symsCount == rhs.symsCount && symsStrTbl == rhs.symsStrTbl);
}

bool KSymbolTable::KSymbolIterator::operator!=(const KSymbolIterator& rhs) {
    return (symAddr != rhs.symAddr || symsCount != rhs.symsCount || symsStrTbl != rhs.symsStrTbl);
}

KSymbolTable::KSymbol KSymbolTable::KSymbolIterator::operator*() {
    return KSymbol(reinterpret_cast<Elf32_Sym*>(symAddr), symsStrTbl);
}

