// Copyright (C) 2016 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <sys/multiboot.h>
#include <arch/x86/mmu.h>
#include <ostream.h>
#include <mm/physical.h>

Multiboot mboot;

Multiboot::Multiboot() {
    isInfoValid = false;
}

void Multiboot::setInfo(void* mbd) {
    info = reinterpret_cast<mbootInfo*>(mbd);
    isInfoValid = true;

    if (hasFlags(InfoFlags::CmdLine)) {
        X86::MMU::mapPageFast(info->cmdline, (void*) ((uintptr_t) info->cmdline + KERNEL_BASE_VIRT_ADDR), X86::MMU::PteFlags::PTE_PRESENT);
    }
}

elfSyms* Multiboot::getElfSymbols() {
    if (hasFlags(InfoFlags::Elf)) {
        return &(info->syms.elf);
    }

    return NULL;
}

bool Multiboot::hasFlags(uint32_t flags) {
    if (isInfoValid) {
        return info->flags & flags;
    }

    return false;
}

uint32_t Multiboot::getLowerMemory() {
    if (hasFlags(InfoFlags::Memory)) {
        return info->mem_lower;
    }

    return 0;
}
uint32_t Multiboot::getUpperMemory() {
    if (hasFlags(InfoFlags::Memory)) {
        return info->mem_upper;
    }

    return 0;
}

uint32_t Multiboot::getTotalMemory() {
    if (hasFlags(InfoFlags::Memory)) {
        return getUpperMemory() + 1024;
    }

    return 0;
}

char* Multiboot::getCommandLine() {
    if (hasFlags(InfoFlags::CmdLine)) {
        return info->cmdline;
    }

    return NULL;
}

void Multiboot::markPhysicalPages() {
    MM::phys.setPhysicalPageFlags(info, MM::PhysicalPageFlags::InUse);
    MM::phys.setPhysicalPageFlags(info->cmdline, MM::PhysicalPageFlags::InUse);
}

Multiboot::ModuleIterator Multiboot::getModulesIterator() {
    if (isInfoValid && hasFlags(InfoFlags::Mods)) {
        return ModuleIterator(info->mods_addr, info->mods_count);
    }

    return ModuleIterator(0, 0);
}

Multiboot::ModuleIterator Multiboot::getLastModule() {
    if (hasFlags(InfoFlags::Mods)) {
        return ModuleIterator(info->mods_addr + info->mods_count * sizeof(mbootInfo), info->mods_count);
    }

    return ModuleIterator(0, 0);
}

Multiboot::MMapIterator Multiboot::getMMapIterator() {
    if (hasFlags(InfoFlags::MemMap)) {
        return MMapIterator(info->mmap_addr, info->mmap_length);
    }

    return MMapIterator(0, 0);
}

Multiboot::MMapIterator Multiboot::getLastMMapEntry() {
    if (hasFlags(InfoFlags::MemMap)) {
        return MMapIterator(info->mmap_addr + info->mmap_length, info->mmap_length);
    }

    return MMapIterator(0, 0);
}

Multiboot::ModuleIterator::ModuleIterator(uintptr_t startAddr, uint32_t modCount) {
    this->addr = startAddr;
    this->count = modCount;
}

Multiboot::ModuleIterator::ModuleIterator(const ModuleIterator& mit) {
    this->addr = mit.addr;
    this->count = mit.count;
}

Multiboot::ModuleIterator Multiboot::ModuleIterator::operator++() {
    this->addr += sizeof(mbootMod);

    return *this;
}

Multiboot::ModuleIterator Multiboot::ModuleIterator::operator++(int) {
    ModuleIterator mit(*this);

    operator++();

    return mit;
}

bool Multiboot::ModuleIterator::operator==(const ModuleIterator& rhs) {
    return this->addr == rhs.addr && this->count == rhs.count;
}
bool Multiboot::ModuleIterator::operator!=(const ModuleIterator& rhs) {
    return this->addr != rhs.addr || this->count != rhs.count;
}

Multiboot::Module Multiboot::ModuleIterator::operator*() {
    Module mod(this->addr);
    return mod;
}

Multiboot::Module::Module(uintptr_t addr) {
    module = reinterpret_cast<mbootMod*>(addr);
}

uint32_t Multiboot::Module::getStartAddr() {
    return module->mod_start;
}

Multiboot::MMapIterator::MMapIterator(uintptr_t startAddr, uint32_t mapLength) {
    this->addr = startAddr;
    this->length = mapLength;
}

Multiboot::MMapIterator::MMapIterator(const MMapIterator& mit) {
    this->addr = mit.addr;
    this->length = mit.length;
}

Multiboot::MMapIterator Multiboot::MMapIterator::operator++() {
    this->addr += operator*().getEntrySize() + sizeof(uint32_t);

    return *this;
}

Multiboot::MMapIterator Multiboot::MMapIterator::operator++(int) {
    MMapIterator mit(*this);

    operator++();

    return mit;
}

bool Multiboot::MMapIterator::operator==(const MMapIterator& rhs) {
    return this->addr == rhs.addr && this->length == rhs.length;
}
bool Multiboot::MMapIterator::operator!=(const MMapIterator& rhs) {
    return this->addr != rhs.addr || this->length != rhs.length;
}

const Multiboot::MMapEntry& Multiboot::MMapIterator::operator*() {
    currentEntry.setAddress(this->addr);
    return currentEntry;
}

void Multiboot::MMapEntry::setAddress(uintptr_t addr) {
    entry = reinterpret_cast<mbootMmapEntry*>(addr);
}

uint32_t Multiboot::MMapEntry::getEntrySize() const {
    return entry->size;
}

uint64_t Multiboot::MMapEntry::getStartAddr() const {
    return entry->addr;
}

uint64_t Multiboot::MMapEntry::getLength() const {
    return entry->len;
}

uint64_t Multiboot::MMapEntry::getEndAddr() const {
    return getStartAddr() + getLength();
}
uint32_t Multiboot::MMapEntry::getType() const {
    return entry->type;
}

bool Multiboot::MMapEntry::isAvailable() const {
    return getType() == MMapEntryType::Available;
}

KLibcpp::OStream& operator <<(KLibcpp::OStream& os, const Multiboot::MMapEntry& ent) {
    os.saveOptions();

    os.setFill('0');
    os.setWidth(16);

    os << KLibcpp::hex << "[mem 0x" << ent.getStartAddr() << "-0x" << ent.getEndAddr() << "] " << (ent.isAvailable() ? "available" : "reserved");

    os.restoreOptions();

    return os;
}
