// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <task/AbstractScheduler.h>
#include <arch/x86/inline.h>

namespace Task {

AbstractScheduler* AbstractScheduler::instance_;

AbstractScheduler::AbstractScheduler(X86::Process* currentProcess) :
        X86::AbstractIRQHandler(0), currentProcess_(currentProcess) {
    instance_ = this;
    lastPid_ = currentProcess->pid;
}

AbstractScheduler::~AbstractScheduler() {

}

int AbstractScheduler::handle(X86::ISRHandler::Registers& regs) {
    currentProcess_->generalRegs = regs;

    currentProcess_ = next();

    regs = currentProcess_->generalRegs;
    X86::set_cr3(currentProcess_->cr3);

    return 0;
}

uint32_t AbstractScheduler::getNextPid() {
    return ++lastPid_;
}

}