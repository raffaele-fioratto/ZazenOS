// Copyright (C) 2017 Fioratto Raffaele

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <task/RoundRobin.h>

Task::RoundRobin::RoundRobin(X86::Process* currentProcess)
        : AbstractScheduler(currentProcess) {
    //readyProcesses_.push_back(currentProcess);
}

X86::Process* Task::RoundRobin::next() {
    if (readyProcesses_.empty()) {
        return currentProcess_;
    }

    X86::Process* front = readyProcesses_.front();
    readyProcesses_.pop_front();

    readyProcesses_.push_back(currentProcess_);
    currentProcess_->state = X86::Process::Ready;

    front->state = X86::Process::Exec;

    return front;
}

void Task::RoundRobin::addProcess(X86::Process* process) {
    readyProcesses_.push_front(process);
}
